<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Customer;
use App\Models\SubscriptionTransaction;
use App\Models\Subscription;
use App\Models\Invoices;
use App\Models\Reports;
use App\Models\Senderid;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\SeederId as Sender;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Carbon\Carbon;


class AdminBaseController extends Controller
{
    /**
     * Show admin home.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function index()
    {
        $breadcrumbs = [
                ['link' => "/dashboard", 'name' => __('locale.menu.Dashboard')],
                ['name' => Auth::user()->displayName()],
        ];

        $sms_outgoing = Reports::currentMonth()
                ->selectRaw('Day(created_at) as day, count(send_by) as outgoing,send_by')
                ->where('send_by', "from")
                ->groupBy('day')->pluck('day', 'outgoing')->flip()->sortKeys();

        $sms_incoming = Reports::currentMonth()
                ->selectRaw('Day(created_at) as day, count(send_by) as incoming,send_by')
                ->where('send_by', "to")
                ->groupBy('day')->pluck('day', 'incoming')->flip()->sortKeys();


        $outgoing = (new LarapexChart)->lineChart()
                ->addData(__('locale.labels.outgoing'), $sms_outgoing->values()->toArray())
                ->setXAxis($sms_outgoing->keys()->toArray());


        $incoming = (new LarapexChart)->lineChart()
                ->addData(__('locale.labels.incoming'), $sms_incoming->values()->toArray())
                ->setXAxis($sms_incoming->keys()->toArray());


        $revenue = Invoices::CurrentMonth()
                ->selectRaw('Day(created_at) as day, sum(amount) as revenue')
                ->groupBy('day')
                ->pluck('revenue', 'day');

        $revenue_chart = (new LarapexChart)->lineChart()
                ->addData(__('locale.labels.revenue'), $revenue->values()->toArray())
                ->setXAxis($revenue->keys()->toArray());

        $customers = Customer::thisYear()
                ->selectRaw('DATE_FORMAT(created_at, "%m-%Y") as month, count(uid) as customer')
                ->groupBy('month')
                ->orderBy('month')
                ->pluck('customer', 'month');


        $customer_growth = (new LarapexChart)->barChart()
                ->addData(__('locale.labels.customers_growth'), $customers->values()->toArray())
                ->setXAxis($customers->keys()->toArray());

        $sms_history = (new LarapexChart)->pieChart()
                ->addData([
                        Reports::where('status', 'like', "%Delivered%")->count(),
                        Reports::where('status', 'not like', "%Delivered%")->count(),
                ]);

        $sender_ids = Senderid::get();
        $senderid_count = Senderid::where('status', 'active')->count();
        $current_date = Carbon::now();
        $monthStartDate = $current_date->startOfMonth()->format('Y-m-d H:i:s');
        $monthEndingDate = $current_date->endOfMonth()->format('Y-m-d H:i:s');
        $endingYear = $yearEnd = date('Y-m-d H:i:s', strtotime('Dec 31'));
        $startingYear = $yearEnd = date('Y-m-d H:i:s', strtotime('Jan 01'));

        $thisMonth = 0;
        $thisYear = 0;
        $total = 0;
        $customer_earn = 0;
        $month = SubscriptionTransaction::whereBetween('created_at', [$monthStartDate, $monthEndingDate])->get();
        if($month->count()>0){
            foreach ($month as $mval){
                $amount = $mval->amount;
                $firstchar = $amount[0];
                $finalAmount =  (int)(preg_replace('/[^0-9\d*(\.\d+),]/', '', $amount));
               if($firstchar == '$'){
                  $finalAmount *= 415.30 ;
               }
               $thisMonth +=(int)$finalAmount;
                
                
            }
        }
        $totalAmount = SubscriptionTransaction::get();
        if($totalAmount->count()>0){
            foreach ($totalAmount as $tval){
                $amount = $tval->amount;
                $firstchar = $amount[0];
                $finalAmount =  (int)(preg_replace('/[^0-9\d*(\.\d+),]/', '', $amount));
               if($firstchar == '$'){
                  $finalAmount *= 415.30 ;
               }
               $total +=(int)$finalAmount;
                
                
            }
        }
        $year = SubscriptionTransaction::whereBetween('created_at', [$startingYear, $endingYear])->get();
        if($year->count()>0){
            foreach ($year as $yval){
                $amount = $yval->amount;
                $firstchar = $amount[0];
                $finalAmount =  (int)(preg_replace('/[^0-9\d*(\.\d+),]/', '', $amount));
               if($firstchar == '$'){
                  $finalAmount *= 415.30 ;
               }
               $thisYear +=(int)$finalAmount;
            }
        }

        // $customerRecord = Subscription::get();
        // if($customerRecord->count()>0){
        //     foreach ($customerRecord as $cval){
        //         $customer = json_decode($cval->options, true);
        //         $customer_earn += (int)(isset($customer['credit'])?$customer['credit']:0);
                
        //     }
                    
        // }
        $customer_earn = 0;
         $customerRecord = User::where("is_admin", 0)->get();
          if($customerRecord->count()>0){
            foreach ($customerRecord as $cval){
               
                $customer_earn += (int)(isset($cval->sms_unit)?$cval->sms_unit:0);
                
            }
                    
        }
         // if($customerRecord->count()>0){
        //     foreach ($customerRecord as $cval){
        //         $customer = json_decode($cval->options, true);
        //         $customer_earn += (int)(isset($customer['credit'])?$customer['credit']:0);
                
        //     }
                    
        // }

        return view('admin.dashboard', compact('total', 'breadcrumbs', 'sms_incoming', 'sms_outgoing', 'outgoing', 'incoming', 'revenue_chart', 'customer_growth', 'sms_history','sender_ids', 'customer_earn', 'thisMonth', 'thisYear', 'senderid_count'));
    }

    protected function redirectResponse(Request $request, $message, $type = 'success')
    {
        if ($request->wantsJson()) {
            return response()->json([
                    'status'  => $type,
                    'message' => $message,
            ]);
        }

        return redirect()->back()->with("flash_{$type}", $message);
    }

}
