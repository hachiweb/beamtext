<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopUpTransactions extends Model
{
    use HasFactory;
    const STATUS_NEW = 'new';
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_ENDED = 'ended';
    const STATUS_RENEW = 'renew';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
     
    protected $table = 'top_transactions';
    protected $dates = [
            'created_at',
            'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'uid',
            'user_id',
            'payment_method',
            'status',
            'total_unit',
    ];
}
