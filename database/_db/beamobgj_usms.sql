-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 21, 2022 at 06:54 AM
-- Server version: 10.3.36-MariaDB-cll-lve
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beamobgj_usms`
--

-- --------------------------------------------------------

--
-- Table structure for table `cg_app_config`
--

CREATE TABLE `cg_app_config` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `setting` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_app_config`
--

INSERT INTO `cg_app_config` (`id`, `setting`, `value`, `created_at`, `updated_at`) VALUES
(1, 'app_name', 'BeamText', '2022-01-27 05:01:39', '2022-08-08 16:34:37'),
(2, 'app_title', 'Bulk SMS For Marketing', '2022-01-27 05:01:39', '2022-08-08 16:34:37'),
(3, 'app_keyword', 'sms, bulk sms, sms, sms marketing', '2022-01-27 05:01:39', '2022-08-08 16:34:37'),
(4, 'license', '686d231f-6452-4ff6-a4e0-6f694c8721c8', '2022-01-27 05:01:39', '2022-01-26 23:02:47'),
(5, 'license_type', 'Regular License', '2022-01-27 05:01:40', '2022-01-26 23:02:47'),
(6, 'valid_domain', 'yes', '2022-01-27 05:01:40', '2022-01-26 23:02:47'),
(7, 'from_email', 'info@beamtext.com', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(8, 'from_name', 'Beamtext', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(9, 'company_address', 'NAF Shopping Plaza,\r\nOff International Airport Road,\r\nLagos. Nigeria', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(10, 'software_version', '3.0.1', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(11, 'footer_text', 'Copyright © Beamtext - 2022', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(12, 'app_logo', 'images/logo/0fa9aa3a2baa1a1d8f9e339a7f25b9ea.png', '2022-01-27 05:01:40', '2022-02-18 14:55:41'),
(13, 'app_favicon', 'images/logo/f0cb15e3da91f082f93d19bb26d8b922.png', '2022-01-27 05:01:40', '2022-02-23 11:02:38'),
(14, 'country', 'Nigeria', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(15, 'timezone', 'Africa/Lagos', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(16, 'app_stage', 'live', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(17, 'maintenance_mode', '1', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(18, 'maintenance_mode_message', 'We\'re undergoing a bit of scheduled maintenance.', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(19, 'maintenance_mode_end', 'Jan 5, 2021 15:37:25', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(20, 'php_bin_path', '/usr/local/bin/php', '2022-01-27 05:01:40', '2022-01-27 05:01:40'),
(21, 'driver', 'sendmail', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(22, 'host', 'smtp.mailtrap.io', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(23, 'username', NULL, '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(24, 'password', NULL, '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(25, 'port', '2525', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(26, 'encryption', 'tls', '2022-01-27 05:01:40', '2022-02-18 16:58:04'),
(27, 'date_format', 'jS M y', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(28, 'language', 'en', '2022-01-27 05:01:40', '2022-08-08 16:34:37'),
(29, 'client_registration', '1', '2022-01-27 05:01:40', '2022-02-19 07:55:16'),
(30, 'registration_verification', '0', '2022-01-27 05:01:40', '2022-02-19 07:55:16'),
(31, 'two_factor', '0', '2022-01-27 05:01:40', '2022-02-19 07:55:16'),
(32, 'two_factor_send_by', 'email', '2022-01-27 05:01:40', '2022-02-19 07:55:16'),
(33, 'captcha_in_login', '0', '2022-01-27 05:01:40', '2022-02-19 07:55:16'),
(34, 'captcha_in_client_registration', '0', '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(35, 'captcha_site_key', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(36, 'captcha_secret_key', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(37, 'login_with_facebook', '0', '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(38, 'facebook_client_id', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(39, 'facebook_client_secret', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(40, 'login_with_twitter', '0', '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(41, 'twitter_client_id', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(42, 'twitter_client_secret', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(43, 'login_with_google', '0', '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(44, 'google_client_id', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(45, 'google_client_secret', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(46, 'login_with_github', '0', '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(47, 'github_client_id', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(48, 'github_client_secret', NULL, '2022-01-27 05:01:41', '2022-02-19 07:55:16'),
(49, 'notification_sms_gateway', '62138c29184e2', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(50, 'notification_sender_id', 'Beamtext SMS Portal', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(51, 'notification_phone', '91900000000', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(52, 'notification_from_name', 'Beamtext SMS Portal', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(53, 'notification_email', 'info@beamtext.com', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(54, 'sender_id_notification_email', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(55, 'sender_id_notification_sms', '0', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(56, 'user_registration_notification_email', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(57, 'user_registration_notification_sms', '0', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(58, 'subscription_notification_email', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(59, 'subscription_notification_sms', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(60, 'keyword_notification_email', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(61, 'keyword_notification_sms', '0', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(62, 'phone_number_notification_email', 'true', '2022-01-27 05:01:41', '2022-02-28 14:22:19'),
(63, 'phone_number_notification_sms', '0', '2022-01-27 05:01:42', '2022-02-28 14:22:19'),
(64, 'block_message_notification_email', '0', '2022-01-27 05:01:42', '2022-02-28 14:22:19'),
(65, 'block_message_notification_sms', '0', '2022-01-27 05:01:42', '2022-02-28 14:22:19'),
(66, 'unsubscribe_message', 'Reply Stop to unsubscribe', '2022-01-27 05:01:42', '2022-01-27 05:01:42'),
(67, 'custom_script', NULL, '2022-01-27 05:01:42', '2022-08-08 16:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `cg_blacklists`
--

CREATE TABLE `cg_blacklists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_blacklists`
--

INSERT INTO `cg_blacklists` (`id`, `uid`, `user_id`, `number`, `reason`, `created_at`, `updated_at`) VALUES
(1, '6224ba71d0710', 1, '+2340000000000', 'Invalid no', NULL, NULL),
(2, '6224baa8d036e', 1, '2347057337653', 'Nigerian Police Force emergency hotlines', NULL, NULL),
(3, '6224baa8d0501', 1, ' 2348061581938', 'Nigerian Police Force emergency hotlines', NULL, NULL),
(4, '6224baa8d0722', 1, ' 2348032003913', 'Nigerian Police Force emergency hotlines', NULL, NULL),
(5, '6224bb3f94ebe', 1, '2348160134303', 'Nigerian Army Human Rights', NULL, NULL),
(6, '6224bb3f9508c', 1, '2348161507644', 'Nigerian Army Human Rights', NULL, NULL),
(7, '62d6ecff34238', 16, '08056254492', NULL, NULL, NULL),
(8, '62e4f5bd54cab', 19, '+23494545444', NULL, NULL, NULL),
(9, '62e98e3a7306f', 20, '+2348034104824', NULL, NULL, NULL),
(10, '6318fa4e4635a', 25, '08789875867', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cg_campaigns`
--

CREATE TABLE `cg_campaigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `campaign_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_url` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upload_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cache` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_time` timestamp NULL DEFAULT NULL,
  `schedule_type` enum('onetime','recurring') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_cycle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_amount` int(11) DEFAULT NULL,
  `frequency_unit` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recurring_end` timestamp NULL DEFAULT NULL,
  `run_at` timestamp NULL DEFAULT NULL,
  `delivery_at` timestamp NULL DEFAULT NULL,
  `batch_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_campaigns`
--

INSERT INTO `cg_campaigns` (`id`, `uid`, `user_id`, `campaign_name`, `message`, `media_url`, `language`, `gender`, `sms_type`, `upload_type`, `status`, `reason`, `api_key`, `cache`, `timezone`, `schedule_time`, `schedule_type`, `frequency_cycle`, `frequency_amount`, `frequency_unit`, `recurring_end`, `run_at`, `delivery_at`, `batch_id`, `created_at`, `updated_at`) VALUES
(103, '62de3e44ed52e', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 10:55:00', '2022-07-25 10:55:00'),
(104, '62de3e4d3992c', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 10:55:09', '2022-07-25 10:55:09'),
(105, '62de3f1d95698', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 10:58:37', '2022-07-25 10:58:37'),
(106, '62de3f34847cc', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 10:59:00', '2022-07-25 10:59:00'),
(107, '62de3f623bb02', 1, 'Rohan', 'vdg', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 10:59:46', '2022-07-25 10:59:46'),
(108, '62de3f94b0cca', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:00:36', '2022-07-25 11:00:36'),
(109, '62de3fa33f581', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:00:51', '2022-07-25 11:00:51'),
(110, '62de410b9e3d1', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:06:51', '2022-07-25 11:06:51'),
(111, '62de410fad0e6', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:06:55', '2022-07-25 11:06:55'),
(112, '62de41233c44a', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:07:15', '2022-07-25 11:07:15'),
(113, '62de4133ec29e', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:07:31', '2022-07-25 11:07:31'),
(114, '62de4140f084f', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:07:44', '2022-07-25 11:07:44'),
(115, '62de414621f05', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:07:50', '2022-07-25 11:07:50'),
(116, '62de415fad439', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:08:15', '2022-07-25 11:08:15'),
(117, '62de416a87358', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:08:26', '2022-07-25 11:08:26'),
(118, '62de41b98e5f7', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:09:45', '2022-07-25 11:09:45'),
(119, '62de41c988b93', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:10:01', '2022-07-25 11:10:01'),
(120, '62de41d9ec516', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:10:17', '2022-07-25 11:10:17'),
(121, '62de437a95fb9', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:17:14', '2022-07-25 11:17:14'),
(122, '62de43bff19d5', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:18:23', '2022-07-25 11:18:23'),
(123, '62de45a229f2d', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:26:26', '2022-07-25 11:26:26'),
(124, '62de4820c95bd', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:37:04', '2022-07-25 11:37:04'),
(125, '62de486e3a3c5', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:38:22', '2022-07-25 11:38:22'),
(126, '62de487220e2a', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 11:38:26', '2022-07-25 11:38:26'),
(131, '62de54a11e125', 3, 'Rahul Rawat', '+2347031476524', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 12:30:25', '2022-07-25 12:30:25'),
(136, '62de8b477efeb', 1, 'Rohan', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 16:23:35', '2022-07-25 16:23:35'),
(147, '62df83190d693', 3, 'Rahul Rawat', 'grth', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:00:57', '2022-07-26 10:00:57'),
(148, '62df832e6a642', 3, 'Rahul Rawat', 'grth', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:01:31', '2022-07-26 10:01:32', NULL, '2022-07-26 10:01:18', '2022-07-26 10:01:32'),
(149, '62df88c70014f', 3, 'Rahul Rawat', 'fbhg', NULL, NULL, NULL, 'plain', 'normal', 'delivered', 'Trying to get property \'username_param\' of non-object', NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:26:17', '2022-07-26 10:26:17', NULL, '2022-07-26 10:25:11', '2022-07-26 10:26:17'),
(150, '62df88e45a25b', 3, 'Rahul Rawat', 'dgvrf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":1,\"FailedDeliveredCount\":1,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:27:01', '2022-07-26 10:27:03', NULL, '2022-07-26 10:25:40', '2022-07-26 10:27:03'),
(151, '62df8a3317178', 3, 'Rahul Rawat', 'xzvf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', 'Trying to get property \'username_param\' of non-object', NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:31:32', '2022-07-26 10:31:32', NULL, '2022-07-26 10:31:15', '2022-07-26 10:31:32'),
(152, '62df8e02e019d', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:47:30', '2022-07-26 10:47:30'),
(153, '62df8e393073f', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:48:25', '2022-07-26 10:48:25'),
(154, '62df8e893d670', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:49:45', '2022-07-26 10:49:45'),
(155, '62df8ec4ac522', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:50:44', '2022-07-26 10:50:44'),
(156, '62df8eda84af4', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:51:06', '2022-07-26 10:51:06'),
(157, '62df8fbe2e1da', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:54:54', '2022-07-26 10:54:54'),
(158, '62df90037eb60', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:56:03', '2022-07-26 10:56:03'),
(159, '62df901307739', 3, 'Rahul Rawat', '+2348127139986', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 10:57:05', '2022-07-26 10:57:07', NULL, '2022-07-26 10:56:19', '2022-07-26 10:57:07'),
(160, '62df91a73ce37', 1, 'Rohan', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:03:03', '2022-07-26 11:03:03'),
(161, '62df94d299ae9', 1, 'Rohan', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:16:34', '2022-07-26 11:16:34'),
(162, '62df95c40cc1a', 1, 'Rohan', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:20:36', '2022-07-26 11:20:36'),
(163, '62df95ff8399e', 1, 'Rohan', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:22:03', '2022-07-26 11:22:06', NULL, '2022-07-26 11:21:35', '2022-07-26 11:22:06'),
(164, '62df973abec9b', 1, 'Rahul', 'Hi {first_name} your available balance is {username}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:27:45', '2022-07-26 11:27:48', NULL, '2022-07-26 11:26:50', '2022-07-26 11:27:48'),
(166, '62df97f98a976', 3, 'Rahul Rawat', 'hii yooo', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:30:18', '2022-07-26 11:30:20', NULL, '2022-07-26 11:30:01', '2022-07-26 11:30:20'),
(170, '62df990ac917e', 3, 'Rahul Rawat', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:34:38', '2022-07-26 11:34:40', NULL, '2022-07-26 11:34:34', '2022-07-26 11:34:40'),
(171, '62df993c5e9d6', 3, 'Rahul Rawat', '{first_name}hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:35:48', '2022-07-26 11:35:50', NULL, '2022-07-26 11:35:24', '2022-07-26 11:35:50'),
(172, '62df9b81a4c1e', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:45:05', '2022-07-26 11:45:05'),
(173, '62df9b87932ec', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:45:11', '2022-07-26 11:45:11'),
(174, '62df9b9f0b58a', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:45:35', '2022-07-26 11:45:35'),
(175, '62df9ba49766a', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:45:40', '2022-07-26 11:45:40'),
(176, '62df9bbb08852', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:46:03', '2022-07-26 11:46:03'),
(177, '62df9bf870a2b', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:47:04', '2022-07-26 11:47:04'),
(178, '62df9c1890dc0', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:47:36', '2022-07-26 11:47:36'),
(179, '62df9ec2a1358', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 11:58:58', '2022-07-26 11:58:58'),
(180, '62df9f777fa90', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:01:59', '2022-07-26 12:01:59'),
(181, '62dfa3ecdf06f', 1, 'Rohan', 'fgrtf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:21:50', '2022-07-26 12:21:53', NULL, '2022-07-26 12:21:00', '2022-07-26 12:21:53'),
(182, '62dfa45fe8e71', 1, 'Rohan', 'gggg', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:22:55', '2022-07-26 12:22:55'),
(183, '62dfa5edd8741', 1, 'Rohan', 'gggg', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:29:33', '2022-07-26 12:29:33'),
(184, '62dfa5f159ba9', 1, 'Rohan', 'gggg', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:29:37', '2022-07-26 12:29:37'),
(186, '62dfa637e7bca', 1, 'Rohan', 'gggg', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:30:47', '2022-07-26 12:30:47'),
(188, '62dfac78e38e3', 3, 'Rahul Rawat', 'fg', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 12:57:48', '2022-07-26 12:57:50', NULL, '2022-07-26 12:57:28', '2022-07-26 12:57:50'),
(192, '62dfae1839403', 3, 'Rahul Rawat', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 13:04:37', '2022-07-26 13:04:39', NULL, '2022-07-26 13:04:24', '2022-07-26 13:04:39'),
(193, '62dfae4e1d612', 3, 'Rahul Rawat', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 13:06:34', '2022-07-26 13:06:36', NULL, '2022-07-26 13:05:18', '2022-07-26 13:06:36'),
(195, '62dfc7a36af2a', 3, 'Rahul Rawat', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 14:53:45', '2022-07-26 14:53:47', NULL, '2022-07-26 14:53:23', '2022-07-26 14:53:47'),
(196, '62dfc81821b13', 1, 'Rahul', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 14:56:15', '2022-07-26 14:56:18', NULL, '2022-07-26 14:55:20', '2022-07-26 14:56:18'),
(197, '62dfc89610252', 1, 'hii', 'hiiillo', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":4,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 14:57:49', '2022-07-26 14:57:54', NULL, '2022-07-26 14:57:26', '2022-07-26 14:57:54'),
(198, '62dfc8fd630e2', 3, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":14,\"DeliveredCount\":14,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 15:00:24', '2022-07-26 15:00:38', NULL, '2022-07-26 14:59:09', '2022-07-26 15:00:38'),
(199, '62dfca8454487', 3, 'cvdsgb', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 15:06:49', '2022-07-26 15:06:50', NULL, '2022-07-26 15:05:40', '2022-07-26 15:06:50'),
(200, '62dfcc2143112', 3, 'Rahul Rawat', 'hhii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 15:13:33', '2022-07-26 15:13:36', NULL, '2022-07-26 15:12:33', '2022-07-26 15:13:36'),
(201, '62dfcc77bc46a', 3, 'Rahul Rawat', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":13,\"DeliveredCount\":13,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 15:14:23', '2022-07-26 15:14:36', NULL, '2022-07-26 15:13:59', '2022-07-26 15:14:36'),
(202, '62dfccc697097', 3, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 15:15:40', '2022-07-26 15:15:41', NULL, '2022-07-26 15:15:18', '2022-07-26 15:15:41'),
(203, '62dfe06bcbcdc', 1, 'Rohan', 'hfgfgb', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-26 16:39:15', '2022-07-26 16:39:18', NULL, '2022-07-26 16:39:07', '2022-07-26 16:39:18'),
(204, '62e05e04717ad', 16, 'Kunle Joseph', 'testing message', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 01:35:00', '2022-07-27 01:35:00'),
(205, '62e05e407471c', 16, 'Kunle Joseph', 'testing message', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 01:36:00', '2022-07-27 01:36:00'),
(206, '62e05f44be4c5', 1, 'Kj', 'testing', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 01:40:31', '2022-07-27 01:40:33', NULL, '2022-07-27 01:40:20', '2022-07-27 01:40:33'),
(207, '62e0600f0bd01', 1, 'Kunle Joseph', 'testing again', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 01:43:46', '2022-07-27 01:43:47', NULL, '2022-07-27 01:43:43', '2022-07-27 01:43:47'),
(208, '62e0c5b4dea77', 1, 'Rohan', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 08:57:33', '2022-07-27 08:57:36', NULL, '2022-07-27 08:57:24', '2022-07-27 08:57:36'),
(211, '62e0dd9fe2ba4', 3, 'Rahul Rawat', 'gdff', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 10:40:08', '2022-07-27 10:40:10', NULL, '2022-07-27 10:39:27', '2022-07-27 10:40:10'),
(212, '62e0df7cd70f7', 3, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 10:47:45', '2022-07-27 10:47:47', NULL, '2022-07-27 10:47:24', '2022-07-27 10:47:47'),
(213, '62e26dc36407c', 19, 'Kj', 'This a test message', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-28 15:07:04', '2022-07-28 15:07:06', NULL, '2022-07-28 15:06:43', '2022-07-28 15:07:06'),
(214, '62e4ef0860225', 19, 'Kj', 'Tet message', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-30 12:43:15', '2022-07-30 12:43:17', NULL, '2022-07-30 12:42:48', '2022-07-30 12:43:17'),
(215, '62e4f00f15351', 19, 'kj1', 'Testing SMS', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-30 12:47:14', '2022-07-30 12:47:17', NULL, '2022-07-30 12:47:11', '2022-07-30 12:47:17'),
(216, '62e4faeb3885b', 19, 'Kunle Joseph', 'Happy birthday to you {first_name\r\n\r\nWe wish you well in all your endeavors', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-30 13:34:26', '2022-07-30 13:34:29', NULL, '2022-07-30 13:33:31', '2022-07-30 13:34:29'),
(217, '62e904bf2f444', 3, 'dd', 'vdf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 15:04:31', '2022-08-02 15:04:31'),
(218, '62e904c79dbe6', 3, 'dd', 'vdf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 15:04:39', '2022-08-02 15:04:39'),
(219, '62e904d63a05a', 3, 'dd', 'vdf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 15:04:54', '2022-08-02 15:04:54'),
(220, '62e904de8ee67', 3, 'dd', 'vdf', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 15:05:02', '2022-08-02 15:05:02'),
(222, '62e90541b82b8', 3, 'sdf', 'hiii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 15:07:49', '2022-08-02 15:07:50', NULL, '2022-08-02 15:06:41', '2022-08-02 15:07:50'),
(223, '62e98c4bda448', 20, 'Tnde', 'Happy Birthday {first_name}  \r\nWe wish you the best as you add one more year to your many years', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 00:43:44', '2022-08-03 00:43:45', NULL, '2022-08-03 00:42:51', '2022-08-03 00:43:45'),
(224, '62e9ffa5485a9', 3, 'sds', 'gg', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 08:55:15', '2022-08-03 08:55:16', NULL, '2022-08-03 08:55:01', '2022-08-03 08:55:16'),
(225, '62ea000fb9d34', 3, 'gd', 'sdd', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 08:57:37', '2022-08-03 08:57:38', NULL, '2022-08-03 08:56:47', '2022-08-03 08:57:38'),
(226, '62ea00f3cdcd5', 1, 'Rohan', 'fgd', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 09:00:40', '2022-08-03 09:00:41', NULL, '2022-08-03 09:00:35', '2022-08-03 09:00:41'),
(227, '62ea1bbad88aa', 3, 'hii', NULL, NULL, NULL, NULL, 'plain', 'file', 'processing', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 10:55:41', '2022-08-03 10:55:41', '96ee7890-30ab-455f-bdad-42815901f7b0', '2022-08-03 10:54:50', '2022-08-03 10:55:41'),
(228, '62eb581e40a99', 3, 'Rahul Rawat', 'hii', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":0,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-04 09:25:35', '2022-08-04 09:25:36', NULL, '2022-08-04 09:24:46', '2022-08-04 09:25:36'),
(229, '62eb7d2502cff', 20, 'Kj', 'Testing message', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":3,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-04 12:03:36', '2022-08-04 12:03:39', NULL, '2022-08-04 12:02:45', '2022-08-04 12:03:39'),
(230, '62ee487f90add', 23, 'Rahul Rawat', 'hello test', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 14:55:39', '2022-08-06 14:55:40', NULL, '2022-08-06 14:54:55', '2022-08-06 14:55:40'),
(231, '62ee51c916042', 23, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:34:33', '2022-08-06 15:34:33'),
(232, '62ee531cc8a0a', 23, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:40:12', '2022-08-06 15:40:12'),
(233, '62ee53487eb92', 23, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:40:56', '2022-08-06 15:40:56'),
(234, '62ee545aedc0b', 23, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:45:46', '2022-08-06 15:45:47', NULL, '2022-08-06 15:45:30', '2022-08-06 15:45:47'),
(235, '62ee5498154c5', 23, 'Rahul Rawat', 'hello, user name Test test1', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:46:37', '2022-08-06 15:46:38', NULL, '2022-08-06 15:46:32', '2022-08-06 15:46:38'),
(236, '62ee54e7c47f5', 23, 'Rahul Rawat', 'hello Test test1', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 15:48:08', '2022-08-06 15:48:09', NULL, '2022-08-06 15:47:51', '2022-08-06 15:48:09'),
(237, '62f0d1f91a68b', 23, 'Rahul Rawat', 'hello Test test1', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 14:06:48', '2022-08-08 14:06:49', NULL, '2022-08-08 14:06:01', '2022-08-08 14:06:49'),
(238, '62f0d21c2912d', 23, 'Rahul Rawat', 'hello Test test1', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 14:07:20', '2022-08-08 14:07:21', NULL, '2022-08-08 14:06:36', '2022-08-08 14:07:21'),
(239, '62f0e2532faa4', 23, 'Rahul Rawat', 'hello Test', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 15:16:26', '2022-08-08 15:16:27', NULL, '2022-08-08 15:15:47', '2022-08-08 15:16:27'),
(240, '62f0f0c547ccc', 23, 'Rahul Rawat', 'message from system', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":1,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 16:17:27', '2022-08-08 16:17:28', NULL, '2022-08-08 16:17:25', '2022-08-08 16:17:28'),
(241, '62f0fe39cccf0', 23, 'Rahul Rawat', 'hii test', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:14:49', '2022-08-08 17:14:49'),
(242, '62f1014f616b0', 23, 'Rahul Rawat', 'hii test', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:27:59', '2022-08-08 17:27:59'),
(245, '62f102195a309', 23, 'Rahul Rawat', 'Message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:31:21', '2022-08-08 17:31:21'),
(246, '62f10277a2f20', 23, 'Rahul Rawat', 'Message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:32:55', '2022-08-08 17:32:55'),
(250, '62f1034162fca', 23, 'Rahul Rawat', 'hhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:36:17', '2022-08-08 17:36:17'),
(252, '62f10384310ac', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:37:24', '2022-08-08 17:37:24'),
(253, '62f103a7a3b4c', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:37:59', '2022-08-08 17:37:59'),
(254, '62f103c879ebd', 23, 'Rahul Rawat', 'Message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:38:32', '2022-08-08 17:38:32'),
(255, '62f103e548f19', 23, 'Rahul Rawat', 'Message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:39:01', '2022-08-08 17:39:01'),
(256, '62f103f426fb5', 23, 'Rahul Rawat', 'Message', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:39:16', '2022-08-08 17:39:16'),
(258, '62f105a444305', 23, 'Rahul Rawat', 'mm', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:46:28', '2022-08-08 17:46:28'),
(259, '62f105c4527f6', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-08 17:47:00', '2022-08-08 17:47:00'),
(260, '62f201dd9da6b', 23, 'Rahul Rawat', 'hh hha', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:42:37', '2022-08-09 11:42:37'),
(261, '62f201e13ee3f', 23, 'Rahul Rawat', 'hh hha', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:42:41', '2022-08-09 11:42:41'),
(263, '62f20257ece99', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:44:39', '2022-08-09 11:44:39'),
(264, '62f2026d9985f', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:45:01', '2022-08-09 11:45:01'),
(265, '62f203b0d8091', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:50:24', '2022-08-09 11:50:24'),
(266, '62f2046b7d155', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:53:31', '2022-08-09 11:53:31'),
(267, '62f2047ba3bfa', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 11:53:47', '2022-08-09 11:53:47'),
(269, '62f206226d653', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:00:50', '2022-08-09 12:00:50'),
(270, '62f20d70357b0', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:32:00', '2022-08-09 12:32:00'),
(271, '62f20f63156f0', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:40:19', '2022-08-09 12:40:19'),
(272, '62f20fb8510cd', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:41:44', '2022-08-09 12:41:44'),
(273, '62f20fd7ab65b', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:42:15', '2022-08-09 12:42:15'),
(274, '62f21002b288b', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:42:58', '2022-08-09 12:42:58'),
(275, '62f2114851393', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:48:24', '2022-08-09 12:48:24'),
(276, '62f21158a8e53', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:48:40', '2022-08-09 12:48:40'),
(277, '62f211b22a708', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:50:10', '2022-08-09 12:50:10'),
(278, '62f211deec3b0', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:50:54', '2022-08-09 12:50:54'),
(279, '62f21219a59b9', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:51:53', '2022-08-09 12:51:53'),
(280, '62f21230aa2f1', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 12:52:16', '2022-08-09 12:52:16'),
(281, '62f2143c9a9d2', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:01:00', '2022-08-09 13:01:00'),
(282, '62f2177dc1392', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:14:53', '2022-08-09 13:14:53'),
(283, '62f21781a0639', 23, 'Rahul Rawat', 'Message file', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:14:57', '2022-08-09 13:14:57'),
(284, '62f217adab3e9', 23, 'Rahul Rawat', 'message from api', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:15:41', '2022-08-09 13:15:41'),
(288, '62f219d61475b', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:24:54', '2022-08-09 13:24:54'),
(289, '62f219fdd2002', 23, 'gg', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:25:33', '2022-08-09 13:25:33'),
(290, '62f21a1059814', 23, 'gg', 'hh hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:25:52', '2022-08-09 13:25:52'),
(291, '62f21ce3f4025', 23, 'rahul', 'hhhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:37:56', '2022-08-09 13:37:56'),
(292, '62f21d169c638', 23, 'rahul', 'hhhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 13:38:46', '2022-08-09 13:38:46'),
(295, '62f225b431243', 23, 'Rahul Rawat', 'hh hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:15:32', '2022-08-09 14:15:32'),
(296, '62f226f934af1', 23, 'Rahul Rawat', 'hh hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:20:57', '2022-08-09 14:20:57'),
(297, '62f227436ef63', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:22:11', '2022-08-09 14:22:11'),
(298, '62f2290defe4d', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:29:49', '2022-08-09 14:29:50'),
(299, '62f2291e2993c', 23, 'Rahul Rawat', 'hh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:30:06', '2022-08-09 14:30:06'),
(300, '62f22a07b093d', 23, 'Rahul Rawat', 'message from system', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:33:59', '2022-08-09 14:33:59'),
(302, '62f22a6c2ed6e', 23, 'Rahul Rawat', 'message from system', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-09 14:35:40', '2022-08-09 14:35:40'),
(303, '62f3525d54466', 23, 'Rahul Rawat', 'message from system', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-10 11:38:21', '2022-08-10 11:38:21'),
(304, '62f397ae6c43d', 23, 'Rahul Rawat', 'message from system', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-10 16:34:06', '2022-08-10 16:34:06'),
(305, '62f623f844a11', 23, 'Rahul Rawat', 'cd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 14:57:12', '2022-08-12 14:57:12'),
(306, '62f62420a311c', 23, 'Rahul Rawat', 'cd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 14:57:52', '2022-08-12 14:57:52'),
(307, '62f624487e00f', 23, 'Rahul Rawat', 'cd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 14:58:32', '2022-08-12 14:58:32'),
(308, '62f62469a15b1', 23, 'Rahul Rawat', 'hhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 14:59:05', '2022-08-12 14:59:05'),
(309, '62f6249b6f229', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 14:59:55', '2022-08-12 14:59:55'),
(310, '62f6261444b49', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:06:12', '2022-08-12 15:06:12'),
(311, '62f6265686237', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:07:18', '2022-08-12 15:07:18'),
(312, '62f62678cdd43', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:07:52', '2022-08-12 15:07:52'),
(313, '62f6271ad729b', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:10:34', '2022-08-12 15:10:34'),
(315, '62f627a3de74a', 23, 'Rahul Rawat', 'c', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:12:51', '2022-08-12 15:12:51'),
(316, '62f627f0416dc', 23, 'Rahul Rawat', 'c', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:14:08', '2022-08-12 15:14:08'),
(317, '62f62a825784e', 23, 'Rahul Rawat', 'c', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:25:06', '2022-08-12 15:25:06');
INSERT INTO `cg_campaigns` (`id`, `uid`, `user_id`, `campaign_name`, `message`, `media_url`, `language`, `gender`, `sms_type`, `upload_type`, `status`, `reason`, `api_key`, `cache`, `timezone`, `schedule_time`, `schedule_type`, `frequency_cycle`, `frequency_amount`, `frequency_unit`, `recurring_end`, `run_at`, `delivery_at`, `batch_id`, `created_at`, `updated_at`) VALUES
(318, '62f62edfec2d9', 23, 'Rahul Rawat', 'hhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:43:43', '2022-08-12 15:43:43'),
(319, '62f6311af0bc5', 23, 'Rahul Rawat', 'hhh', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:53:14', '2022-08-12 15:53:15'),
(320, '62f6312d121c1', 23, 'Rahul Rawat', 'rgr', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:53:33', '2022-08-12 15:53:33'),
(321, '62f6318e58703', 23, 'Rahul Rawat', 'rgr', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:55:10', '2022-08-12 15:55:10'),
(322, '62f6319a76505', 23, 'Rahul Rawat', 'dvd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:55:22', '2022-08-12 15:55:22'),
(323, '62f6320d08a17', 23, 'Rahul Rawat', 'dvf', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 15:57:17', '2022-08-12 15:57:17'),
(324, '62f6333ac4570', 23, 'Rahul Rawat', 'dvd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 16:02:18', '2022-08-12 16:02:18'),
(325, '62f6335cc0394', 23, 'Rahul Rawat', 'dvd', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 16:02:52', '2022-08-12 16:02:52'),
(326, '62f633e3cf46c', 23, 'Rahul Rawat', 'dvf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":5,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 16:05:07', '2022-08-12 16:05:09'),
(327, '62f6340517a91', 23, 'Rahul Rawat', 'vfb', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 16:05:41', '2022-08-12 16:05:41'),
(328, '62f6345aa35d3', 23, 'Rahul Rawat', 'dvd', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-12 16:07:06', '2022-08-12 16:07:08'),
(329, '62f74d5c29466', 23, 'Rahul Rawat', 'fgf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-13 12:06:04', '2022-08-13 12:06:05'),
(330, '62f752d9eb421', 23, 'Rahul Rawat', 'ced', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-13 12:29:29', '2022-08-13 12:29:31'),
(331, '62fb3fa5ee4c9', 23, 'Rahul Rawat', 'hhh', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 11:56:37', '2022-08-16 11:56:38'),
(332, '62fb594545d09', 23, 'Rahul Rawat', 'hello', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 13:45:57', '2022-08-16 13:45:58'),
(333, '62fb5e8251ef5', 23, 'Rahul Rawat', 'fgtgfh', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 14:03:21', '2022-08-16 14:06:21', NULL, '2022-08-16 14:08:18', '2022-08-16 14:08:21'),
(334, '62fbddcfd4de2', 20, 'Kunle Joseph', 'Happy Birthday {first_name}  \r\nWe wish you the best as you add one more year to your many years', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 23:06:29', '2022-08-16 23:09:29', NULL, '2022-08-16 23:11:27', '2022-08-16 23:11:29'),
(335, '62fbe4cb020c7', 20, 'Kjj', 'Testing Beam Text for {first_name} Testing Beam Text for {first_name} Testing Beam Text for {first_name} Testing Beam Text for {first_name} Testing Beam Text for {first_name}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 23:36:16', '2022-08-16 23:39:16', NULL, '2022-08-16 23:41:15', '2022-08-16 23:41:16'),
(336, '62ff6807bd546', 23, 'cd', 'csdc', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-19 15:33:00', '2022-08-19 15:36:00', NULL, '2022-08-19 15:37:59', '2022-08-19 15:38:00'),
(337, '62ff68248ec56', 23, 'scef', 'safed cfvbf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-19 15:33:30', '2022-08-19 15:36:30', NULL, '2022-08-19 15:38:28', '2022-08-19 15:38:30'),
(338, '62ff6862b2742', 23, 'asd', 'sfced', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-19 15:34:32', '2022-08-19 15:37:32', NULL, '2022-08-19 15:39:30', '2022-08-19 15:39:32'),
(339, '62ff6986cfdf0', 23, 'Roh', 'fewrg', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-19 15:39:24', '2022-08-19 15:42:24', NULL, '2022-08-19 15:44:22', '2022-08-19 15:44:24'),
(340, '631075d52aa8b', 23, 'Rahul Rawat', '{first_name} dfhbth', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-01 14:00:37', '2022-09-01 14:03:37', NULL, '2022-09-01 14:05:25', '2022-09-01 14:05:37'),
(341, '6310786c28ebd', 23, 'Rahul Rawat', NULL, NULL, NULL, NULL, 'plain', 'file', 'processing', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":2,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-01 14:16:52', '2022-09-01 14:16:52', '972901c0-55fa-49b5-ac49-e42809e3d406', '2022-09-01 14:16:28', '2022-09-01 14:16:52'),
(342, '6310788cbb301', 23, 'Rahul Rawat', '{first_name} test', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-01 14:12:01', '2022-09-01 14:15:01', NULL, '2022-09-01 14:17:00', '2022-09-01 14:17:01'),
(343, '63107c620c14f', 23, 'dv', 'fedg', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-01 14:28:22', '2022-09-01 14:31:22', NULL, '2022-09-01 14:33:22', '2022-09-01 14:33:22'),
(344, '6310807de481f', 23, 'Rahul Rawat', 'dgvrf', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":4,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-01 14:45:55', '2022-09-01 14:48:55', NULL, '2022-09-01 14:50:53', '2022-09-01 14:50:55'),
(351, '6315bbe132e34', 1, 'Rohan', 'swdfe', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 14:00:38', '2022-09-05 14:03:38', NULL, '2022-09-05 14:05:37', '2022-09-05 14:05:38'),
(357, '6315c44a560ad', 23, 'df', NULL, NULL, NULL, NULL, 'plain', 'file', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 14:36:30', '2022-09-05 14:39:30', NULL, '2022-09-05 14:41:30', '2022-09-05 14:41:30'),
(362, '6315c6a94dae0', 23, 'Rahul Rawat', NULL, NULL, NULL, NULL, 'plain', 'file', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 14:46:37', '2022-09-05 14:49:37', NULL, '2022-09-05 14:51:37', '2022-09-05 14:51:37'),
(363, '6315c7b68268a', 23, 'Rahul Rawat', NULL, NULL, NULL, NULL, 'plain', 'file', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 14:51:06', '2022-09-05 14:54:06', NULL, '2022-09-05 14:56:06', '2022-09-05 14:56:06'),
(364, '6315c8778ee3e', 23, 'Rahul Rawat', NULL, NULL, NULL, NULL, 'plain', 'file', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 14:54:20', '2022-09-05 14:57:20', NULL, '2022-09-05 14:59:19', '2022-09-05 14:59:20'),
(388, '6315e7108a186', 23, 'Rahul Rawat', '{last_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:04:53', '2022-09-05 17:07:53', NULL, '2022-09-05 17:09:52', '2022-09-05 17:09:53'),
(389, '6315e798d4fb1', 23, 'Rahul Rawat', 'Test from System phone no is {phone} first name is  {first_name}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:07:09', '2022-09-05 17:10:09', NULL, '2022-09-05 17:12:08', '2022-09-05 17:12:09'),
(390, '6315e8bb04477', 23, 'Rahul Rawat', 'Test from system {first_name} & {phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:11:59', '2022-09-05 17:14:59', NULL, '2022-09-05 17:16:59', '2022-09-05 17:16:59'),
(391, '6315e8eec1d50', 23, 'Rahul Rawat', 'Message from system {first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:12:51', '2022-09-05 17:15:51', NULL, '2022-09-05 17:17:50', '2022-09-05 17:17:51'),
(392, '6315e90ae5c38', 23, 'Rahul Rawat', 'vgfr', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:13:19', '2022-09-05 17:16:19', NULL, '2022-09-05 17:18:18', '2022-09-05 17:18:19'),
(393, '6315e90c61081', 23, 'Rahul Rawat', 'vgfr', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:13:21', '2022-09-05 17:16:21', NULL, '2022-09-05 17:18:20', '2022-09-05 17:18:21'),
(394, '6315e94ec6c26', 23, 'Rahul Rawat', 'Message from system{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":1,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:14:27', '2022-09-05 17:17:27', NULL, '2022-09-05 17:19:26', '2022-09-05 17:19:27'),
(395, '6315e9e233a60', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:21:54', '2022-09-05 17:21:54'),
(396, '6315ea072d29e', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:22:31', '2022-09-05 17:22:31'),
(397, '6315ea3a28182', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:23:22', '2022-09-05 17:23:22'),
(398, '6315ea7b35428', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:24:27', '2022-09-05 17:24:27'),
(399, '6315eaa4ac5f4', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'queued', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:25:08', '2022-09-05 17:25:08'),
(400, '6315eab0530d0', 23, 'dvf', 'vf{first_name}{phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:20:21', '2022-09-05 17:23:21', NULL, '2022-09-05 17:25:20', '2022-09-05 17:25:21'),
(401, '6315eae20a5c6', 23, 'Rahul Rawat', 'Message from system {first_name} {phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:21:11', '2022-09-05 17:24:11', NULL, '2022-09-05 17:26:10', '2022-09-05 17:26:11'),
(402, '6315ec885eaf9', 23, 'Rahul Rawat', 'message from system {first_name} & {phone}', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":2,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 17:28:13', '2022-09-05 17:31:13', NULL, '2022-09-05 17:33:12', '2022-09-05 17:33:13'),
(403, '6318f5a38e0d5', 25, 'Kj', 'Dear {first_name},\r\nYour phone number {phone} has been on our system for over a year. So you are welcome', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 00:43:52', '2022-09-08 00:46:52', NULL, '2022-09-08 00:48:51', '2022-09-08 00:48:52'),
(406, '6318f9e9cae60', 25, 'Kj', 'Dear {first_name},\r\nYour phone number {phone} has been on our system for over a year. So you are welcome', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 01:02:06', '2022-09-08 01:05:06', NULL, '2022-09-08 01:07:05', '2022-09-08 01:07:06'),
(407, '631903f43ba6e', 25, 'kj', 'BEAM TEXT Dear {first_name},\r\nYour phone number {phone} has been on our system for over a year. So you are welcome.', NULL, NULL, NULL, 'plain', 'normal', 'delivered', NULL, NULL, '{\"ContactCount\":3,\"DeliveredCount\":0,\"FailedDeliveredCount\":0,\"NotDeliveredCount\":0}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 01:44:57', '2022-09-08 01:47:57', NULL, '2022-09-08 01:49:56', '2022-09-08 01:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `cg_campaigns_lists`
--

CREATE TABLE `cg_campaigns_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `contact_list_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_campaigns_lists`
--

INSERT INTO `cg_campaigns_lists` (`id`, `campaign_id`, `contact_list_id`, `created_at`, `updated_at`) VALUES
(20, 131, 5, '2022-07-25 12:30:25', '2022-07-25 12:30:25'),
(21, 148, 5, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(22, 149, 5, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(23, 150, 5, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(24, 151, 5, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(25, 159, 5, '2022-07-26 10:56:19', '2022-07-26 10:56:19'),
(26, 163, 3, '2022-07-26 11:21:35', '2022-07-26 11:21:35'),
(27, 164, 3, '2022-07-26 11:26:50', '2022-07-26 11:26:50'),
(28, 166, 5, '2022-07-26 11:30:01', '2022-07-26 11:30:01'),
(29, 170, 5, '2022-07-26 11:34:34', '2022-07-26 11:34:34'),
(30, 171, 5, '2022-07-26 11:35:24', '2022-07-26 11:35:24'),
(31, 179, 3, '2022-07-26 11:58:58', '2022-07-26 11:58:58'),
(32, 180, 3, '2022-07-26 12:01:59', '2022-07-26 12:01:59'),
(33, 181, 3, '2022-07-26 12:21:00', '2022-07-26 12:21:00'),
(34, 182, 3, '2022-07-26 12:22:55', '2022-07-26 12:22:55'),
(35, 183, 3, '2022-07-26 12:29:33', '2022-07-26 12:29:33'),
(36, 184, 3, '2022-07-26 12:29:37', '2022-07-26 12:29:37'),
(38, 186, 3, '2022-07-26 12:30:47', '2022-07-26 12:30:47'),
(40, 188, 5, '2022-07-26 12:57:28', '2022-07-26 12:57:28'),
(41, 192, 5, '2022-07-26 13:04:24', '2022-07-26 13:04:24'),
(42, 193, 5, '2022-07-26 13:05:18', '2022-07-26 13:05:18'),
(43, 195, 5, '2022-07-26 14:53:23', '2022-07-26 14:53:23'),
(44, 196, 3, '2022-07-26 14:55:20', '2022-07-26 14:55:20'),
(45, 197, 1, '2022-07-26 14:57:26', '2022-07-26 14:57:26'),
(46, 197, 3, '2022-07-26 14:57:26', '2022-07-26 14:57:26'),
(47, 198, 5, '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(48, 198, 11, '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(49, 198, 12, '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(50, 198, 13, '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(51, 199, 11, '2022-07-26 15:05:40', '2022-07-26 15:05:40'),
(52, 200, 5, '2022-07-26 15:12:33', '2022-07-26 15:12:33'),
(53, 201, 13, '2022-07-26 15:13:59', '2022-07-26 15:13:59'),
(54, 202, 11, '2022-07-26 15:15:18', '2022-07-26 15:15:18'),
(55, 203, 3, '2022-07-26 16:39:07', '2022-07-26 16:39:07'),
(56, 206, 3, '2022-07-27 01:40:20', '2022-07-27 01:40:20'),
(57, 208, 3, '2022-07-27 08:57:24', '2022-07-27 08:57:24'),
(60, 211, 5, '2022-07-27 10:39:27', '2022-07-27 10:39:27'),
(61, 212, 5, '2022-07-27 10:47:24', '2022-07-27 10:47:24'),
(62, 213, 14, '2022-07-28 15:06:43', '2022-07-28 15:06:43'),
(63, 214, 14, '2022-07-30 12:42:48', '2022-07-30 12:42:48'),
(64, 215, 14, '2022-07-30 12:47:11', '2022-07-30 12:47:11'),
(65, 216, 14, '2022-07-30 13:33:31', '2022-07-30 13:33:31'),
(67, 222, 11, '2022-08-02 15:06:41', '2022-08-02 15:06:41'),
(68, 223, 16, '2022-08-03 00:42:51', '2022-08-03 00:42:51'),
(69, 224, 5, '2022-08-03 08:55:01', '2022-08-03 08:55:01'),
(70, 226, 3, '2022-08-03 09:00:35', '2022-08-03 09:00:35'),
(71, 228, 5, '2022-08-04 09:24:46', '2022-08-04 09:24:46'),
(72, 229, 16, '2022-08-04 12:02:45', '2022-08-04 12:02:45'),
(99, 334, 16, '2022-08-16 23:11:28', '2022-08-16 23:11:28'),
(100, 335, 16, '2022-08-16 23:41:15', '2022-08-16 23:41:15'),
(110, 351, 3, '2022-09-05 14:05:37', '2022-09-05 14:05:37'),
(134, 388, 19, '2022-09-05 17:09:52', '2022-09-05 17:09:52'),
(135, 389, 19, '2022-09-05 17:12:08', '2022-09-05 17:12:08'),
(136, 390, 19, '2022-09-05 17:16:59', '2022-09-05 17:16:59'),
(137, 391, 19, '2022-09-05 17:17:50', '2022-09-05 17:17:50'),
(138, 392, 19, '2022-09-05 17:18:18', '2022-09-05 17:18:18'),
(139, 393, 19, '2022-09-05 17:18:20', '2022-09-05 17:18:20'),
(140, 395, 19, '2022-09-05 17:21:54', '2022-09-05 17:21:54'),
(141, 396, 19, '2022-09-05 17:22:31', '2022-09-05 17:22:31'),
(142, 397, 19, '2022-09-05 17:23:22', '2022-09-05 17:23:22'),
(143, 398, 19, '2022-09-05 17:24:27', '2022-09-05 17:24:27'),
(144, 399, 19, '2022-09-05 17:25:08', '2022-09-05 17:25:08'),
(145, 400, 19, '2022-09-05 17:25:20', '2022-09-05 17:25:20'),
(146, 401, 19, '2022-09-05 17:26:10', '2022-09-05 17:26:10'),
(147, 402, 19, '2022-09-05 17:33:12', '2022-09-05 17:33:12'),
(148, 403, 20, '2022-09-08 00:48:51', '2022-09-08 00:48:51'),
(149, 406, 20, '2022-09-08 01:07:05', '2022-09-08 01:07:05'),
(150, 407, 20, '2022-09-08 01:49:56', '2022-09-08 01:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `cg_campaigns_recipients`
--

CREATE TABLE `cg_campaigns_recipients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `recipient` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_campaigns_recipients`
--

INSERT INTO `cg_campaigns_recipients` (`id`, `campaign_id`, `recipient`, `created_at`, `updated_at`) VALUES
(14, 131, 'fhbg', '2022-07-25 12:30:25', '2022-07-25 12:30:25'),
(15, 148, '+2347031476524', '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(16, 149, '+2347031476524', '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(17, 150, 'bgn', '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(18, 151, 'cbg', '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(19, 159, 'fdhh', '2022-07-26 10:56:19', '2022-07-26 10:56:19'),
(20, 163, '+2348127139986', '2022-07-26 11:21:35', '2022-07-26 11:21:35'),
(21, 164, '+2348127139986', '2022-07-26 11:26:50', '2022-07-26 11:26:50'),
(22, 166, '+2348127139986', '2022-07-26 11:30:01', '2022-07-26 11:30:01'),
(23, 170, 'hghg', '2022-07-26 11:34:34', '2022-07-26 11:34:34'),
(24, 171, ':+2348127139986', '2022-07-26 11:35:24', '2022-07-26 11:35:24'),
(25, 179, '+2348127139986', '2022-07-26 11:58:58', '2022-07-26 11:58:58'),
(26, 180, '+2348127139986', '2022-07-26 12:01:59', '2022-07-26 12:01:59'),
(27, 181, '+2348127139986', '2022-07-26 12:21:00', '2022-07-26 12:21:00'),
(28, 182, '+2348127139986', '2022-07-26 12:22:55', '2022-07-26 12:22:55'),
(29, 183, '+2348127139986', '2022-07-26 12:29:33', '2022-07-26 12:29:33'),
(30, 184, '+2348127139986', '2022-07-26 12:29:37', '2022-07-26 12:29:37'),
(32, 186, '+2348127139986', '2022-07-26 12:30:47', '2022-07-26 12:30:47'),
(34, 188, '+2348127139986', '2022-07-26 12:57:28', '2022-07-26 12:57:28'),
(35, 192, 'hiii', '2022-07-26 13:04:24', '2022-07-26 13:04:24'),
(36, 193, '+2348127139986', '2022-07-26 13:05:18', '2022-07-26 13:05:18'),
(37, 195, '+2348127139986', '2022-07-26 14:53:23', '2022-07-26 14:53:23'),
(38, 196, '+2348127139986', '2022-07-26 14:55:20', '2022-07-26 14:55:20'),
(39, 197, '+2348127139986', '2022-07-26 14:57:26', '2022-07-26 14:57:26'),
(40, 198, '+2348127139986', '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(41, 199, 'hello', '2022-07-26 15:05:40', '2022-07-26 15:05:40'),
(42, 200, '+2348127139986', '2022-07-26 15:12:33', '2022-07-26 15:12:33'),
(43, 201, '+2347031476524', '2022-07-26 15:13:59', '2022-07-26 15:13:59'),
(44, 202, '+2347031476524', '2022-07-26 15:15:18', '2022-07-26 15:15:18'),
(45, 203, '+2347031476524', '2022-07-26 16:39:07', '2022-07-26 16:39:07'),
(46, 207, '+2347031476524', '2022-07-27 01:43:43', '2022-07-27 01:43:43'),
(47, 208, '+2347031476524', '2022-07-27 08:57:24', '2022-07-27 08:57:24'),
(50, 211, '+2348127139986', '2022-07-27 10:39:27', '2022-07-27 10:39:27'),
(51, 212, '+2348127139986', '2022-07-27 10:47:24', '2022-07-27 10:47:24'),
(52, 222, '1234567891', '2022-08-02 15:06:41', '2022-08-02 15:06:41'),
(53, 225, '+2348127139986', '2022-08-03 08:56:47', '2022-08-03 08:56:47'),
(54, 230, '+2348127139986', '2022-08-06 14:54:55', '2022-08-06 14:54:55'),
(55, 234, '+2348127139986', '2022-08-06 15:45:30', '2022-08-06 15:45:30'),
(56, 235, '+2348127139986', '2022-08-06 15:46:32', '2022-08-06 15:46:32'),
(57, 236, '+2347031476524', '2022-08-06 15:47:51', '2022-08-06 15:47:51'),
(58, 237, '+2347031476524', '2022-08-08 14:06:01', '2022-08-08 14:06:01'),
(59, 238, '2347031476524', '2022-08-08 14:06:36', '2022-08-08 14:06:36'),
(60, 239, '2347031476524', '2022-08-08 15:15:47', '2022-08-08 15:15:47'),
(61, 240, '+2347031476524', '2022-08-08 16:17:25', '2022-08-08 16:17:25'),
(62, 241, '+2347031476524', '2022-08-08 17:14:49', '2022-08-08 17:14:49'),
(63, 242, '+2347031476524', '2022-08-08 17:27:59', '2022-08-08 17:27:59'),
(66, 245, '+2347031476524', '2022-08-08 17:31:21', '2022-08-08 17:31:21'),
(67, 246, '+2347031476524', '2022-08-08 17:32:55', '2022-08-08 17:32:55'),
(71, 250, '+2347031476524', '2022-08-08 17:36:17', '2022-08-08 17:36:17'),
(73, 252, '+2347031476524', '2022-08-08 17:37:24', '2022-08-08 17:37:24'),
(74, 253, '+2347031476524', '2022-08-08 17:37:59', '2022-08-08 17:37:59'),
(75, 254, '+2347031476524', '2022-08-08 17:38:32', '2022-08-08 17:38:32'),
(76, 255, '+2347031476524', '2022-08-08 17:39:01', '2022-08-08 17:39:01'),
(77, 256, '+2347031476524', '2022-08-08 17:39:16', '2022-08-08 17:39:16'),
(79, 258, '+2347031476524', '2022-08-08 17:46:28', '2022-08-08 17:46:28'),
(80, 259, '+2347031476524', '2022-08-08 17:47:00', '2022-08-08 17:47:00'),
(81, 260, '+2347031476524', '2022-08-09 11:42:37', '2022-08-09 11:42:37'),
(82, 261, '+2347031476524', '2022-08-09 11:42:41', '2022-08-09 11:42:41'),
(84, 263, '+2347031476524', '2022-08-09 11:44:39', '2022-08-09 11:44:39'),
(85, 264, '+2347031476524', '2022-08-09 11:45:01', '2022-08-09 11:45:01'),
(86, 265, '+2347031476524', '2022-08-09 11:50:24', '2022-08-09 11:50:24'),
(87, 266, '+2347031476524', '2022-08-09 11:53:31', '2022-08-09 11:53:31'),
(88, 267, '+2347031476524', '2022-08-09 11:53:47', '2022-08-09 11:53:47'),
(90, 269, '+2347031476524', '2022-08-09 12:00:50', '2022-08-09 12:00:50'),
(91, 270, '+2347031476524', '2022-08-09 12:32:00', '2022-08-09 12:32:00'),
(92, 271, '+2347031476524', '2022-08-09 12:40:19', '2022-08-09 12:40:19'),
(93, 272, '+2347031476524', '2022-08-09 12:41:44', '2022-08-09 12:41:44'),
(94, 273, '+2347031476524', '2022-08-09 12:42:15', '2022-08-09 12:42:15'),
(95, 274, '+2347031476524', '2022-08-09 12:42:58', '2022-08-09 12:42:58'),
(96, 275, '+2347031476524', '2022-08-09 12:48:24', '2022-08-09 12:48:24'),
(97, 276, '+2347031476524', '2022-08-09 12:48:40', '2022-08-09 12:48:40'),
(98, 277, '+2347031476524', '2022-08-09 12:50:10', '2022-08-09 12:50:10'),
(99, 278, '+2347031476524', '2022-08-09 12:50:54', '2022-08-09 12:50:54'),
(100, 279, '+2347031476524', '2022-08-09 12:51:53', '2022-08-09 12:51:53'),
(101, 280, '+2347031476524', '2022-08-09 12:52:16', '2022-08-09 12:52:16'),
(102, 281, '+2347031476524', '2022-08-09 13:01:00', '2022-08-09 13:01:00'),
(103, 282, '+2347031476524', '2022-08-09 13:14:53', '2022-08-09 13:14:53'),
(104, 283, '+2347031476524', '2022-08-09 13:14:57', '2022-08-09 13:14:57'),
(105, 284, '+2347031476524', '2022-08-09 13:15:41', '2022-08-09 13:15:41'),
(109, 288, '+2347031476524', '2022-08-09 13:24:54', '2022-08-09 13:24:54'),
(110, 289, '+2347031476524', '2022-08-09 13:25:33', '2022-08-09 13:25:33'),
(111, 290, '+2347031476524', '2022-08-09 13:25:52', '2022-08-09 13:25:52'),
(112, 291, '+2347031476524', '2022-08-09 13:37:56', '2022-08-09 13:37:56'),
(113, 292, '+2347031476524', '2022-08-09 13:38:46', '2022-08-09 13:38:46'),
(116, 295, '+2347031476524', '2022-08-09 14:15:32', '2022-08-09 14:15:32'),
(117, 296, '+2347031476524', '2022-08-09 14:20:57', '2022-08-09 14:20:57'),
(118, 297, '+2347031476524', '2022-08-09 14:22:11', '2022-08-09 14:22:11'),
(119, 298, '+2347031476524', '2022-08-09 14:29:50', '2022-08-09 14:29:50'),
(120, 299, '+2347031476524', '2022-08-09 14:30:06', '2022-08-09 14:30:06'),
(121, 300, '+2347031476524', '2022-08-09 14:33:59', '2022-08-09 14:33:59'),
(123, 302, '+2347031476524', '2022-08-09 14:35:40', '2022-08-09 14:35:40'),
(124, 303, '+2347031476524', '2022-08-10 11:38:21', '2022-08-10 11:38:21'),
(125, 304, '+2348127139986', '2022-08-10 16:34:06', '2022-08-10 16:34:06'),
(126, 306, '123', '2022-08-12 14:57:52', '2022-08-12 14:57:52'),
(127, 307, '123', '2022-08-12 14:58:32', '2022-08-12 14:58:32'),
(128, 309, '111', '2022-08-12 14:59:55', '2022-08-12 14:59:55'),
(129, 310, '111', '2022-08-12 15:06:12', '2022-08-12 15:06:12'),
(130, 311, '111', '2022-08-12 15:07:18', '2022-08-12 15:07:18'),
(131, 312, '111', '2022-08-12 15:07:52', '2022-08-12 15:07:52'),
(132, 313, '111', '2022-08-12 15:10:34', '2022-08-12 15:10:34'),
(133, 320, '1222', '2022-08-12 15:53:33', '2022-08-12 15:53:33'),
(134, 321, '1222', '2022-08-12 15:55:10', '2022-08-12 15:55:10'),
(135, 343, 'cv', '2022-09-01 14:33:22', '2022-09-01 14:33:22'),
(137, 394, '+2347031476524', '2022-09-05 17:19:26', '2022-09-05 17:19:26');

-- --------------------------------------------------------

--
-- Table structure for table `cg_campaigns_senderids`
--

CREATE TABLE `cg_campaigns_senderids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `originator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_campaigns_senderids`
--

INSERT INTO `cg_campaigns_senderids` (`id`, `campaign_id`, `sender_id`, `originator`, `created_at`, `updated_at`) VALUES
(22, 131, 'BeamText', NULL, '2022-07-25 12:30:25', '2022-07-25 12:30:25'),
(23, 148, 'AB-HACWEB', 'sender_id', '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(24, 149, 'AB-HACWEB', 'sender_id', '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(25, 150, 'AB-HACWEB', 'sender_id', '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(26, 151, 'AB-HACWEB', 'sender_id', '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(27, 159, 'AB-HACWEB', 'sender_id', '2022-07-26 10:56:19', '2022-07-26 10:56:19'),
(28, 163, 'BeamTEXT', NULL, '2022-07-26 11:21:35', '2022-07-26 11:21:35'),
(29, 164, 'BeamTEXT', NULL, '2022-07-26 11:26:50', '2022-07-26 11:26:50'),
(30, 166, 'BeamTEXT', 'sender_id', '2022-07-26 11:30:01', '2022-07-26 11:30:01'),
(31, 170, 'AB-HACWEB', 'sender_id', '2022-07-26 11:34:34', '2022-07-26 11:34:34'),
(32, 171, 'AB-HACWEB', 'sender_id', '2022-07-26 11:35:24', '2022-07-26 11:35:24'),
(33, 179, 'BeamTEXT', NULL, '2022-07-26 11:58:58', '2022-07-26 11:58:58'),
(34, 180, 'BeamTEXT', NULL, '2022-07-26 12:01:59', '2022-07-26 12:01:59'),
(35, 181, 'BeamTEXT', NULL, '2022-07-26 12:21:00', '2022-07-26 12:21:00'),
(36, 182, 'BeamTEXT', NULL, '2022-07-26 12:22:55', '2022-07-26 12:22:55'),
(37, 183, 'BeamTEXT', NULL, '2022-07-26 12:29:33', '2022-07-26 12:29:33'),
(38, 184, 'BeamTEXT', NULL, '2022-07-26 12:29:37', '2022-07-26 12:29:37'),
(40, 186, 'BeamTEXT', NULL, '2022-07-26 12:30:47', '2022-07-26 12:30:47'),
(42, 188, 'AB-HACWEB', 'sender_id', '2022-07-26 12:57:28', '2022-07-26 12:57:28'),
(43, 192, 'AB-HACWEB', 'sender_id', '2022-07-26 13:04:24', '2022-07-26 13:04:24'),
(44, 193, 'BeamTEXT', 'sender_id', '2022-07-26 13:05:18', '2022-07-26 13:05:18'),
(45, 195, 'BeamTEXT', 'sender_id', '2022-07-26 14:53:23', '2022-07-26 14:53:23'),
(47, 197, 'BeamTEXT', NULL, '2022-07-26 14:57:26', '2022-07-26 14:57:26'),
(48, 198, 'BeamTEXT', 'sender_id', '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(49, 199, 'AB-HACWEB', 'sender_id', '2022-07-26 15:05:40', '2022-07-26 15:05:40'),
(50, 200, 'BeamTEXT', 'sender_id', '2022-07-26 15:12:33', '2022-07-26 15:12:33'),
(51, 201, 'BeamTEXT', 'sender_id', '2022-07-26 15:13:59', '2022-07-26 15:13:59'),
(52, 202, 'BeamTEXT', 'sender_id', '2022-07-26 15:15:18', '2022-07-26 15:15:18'),
(53, 203, 'BeamTEXT', NULL, '2022-07-26 16:39:07', '2022-07-26 16:39:07'),
(54, 206, 'BeamTEXT', NULL, '2022-07-27 01:40:20', '2022-07-27 01:40:20'),
(55, 207, 'BeamText', NULL, '2022-07-27 01:43:43', '2022-07-27 01:43:43'),
(56, 208, 'BeamTEXT', NULL, '2022-07-27 08:57:24', '2022-07-27 08:57:24'),
(59, 211, 'AB-HACWEB', 'sender_id', '2022-07-27 10:39:27', '2022-07-27 10:39:27'),
(60, 212, 'AB-HACWEB', 'sender_id', '2022-07-27 10:47:24', '2022-07-27 10:47:24'),
(61, 213, 'BeamTEXT', 'sender_id', '2022-07-28 15:06:43', '2022-07-28 15:06:43'),
(62, 214, 'BeamTEXT', 'sender_id', '2022-07-30 12:42:48', '2022-07-30 12:42:48'),
(63, 215, 'BeamTEXT', 'sender_id', '2022-07-30 12:47:11', '2022-07-30 12:47:11'),
(64, 216, 'BeamTEXT', NULL, '2022-07-30 13:33:31', '2022-07-30 13:33:31'),
(66, 222, 'AB-HACWEB', 'sender_id', '2022-08-02 15:06:41', '2022-08-02 15:06:41'),
(67, 223, 'BeamTEXT', 'sender_id', '2022-08-03 00:42:51', '2022-08-03 00:42:51'),
(68, 224, 'AB-HACWEB', 'sender_id', '2022-08-03 08:55:01', '2022-08-03 08:55:01'),
(69, 225, 'AB-HACWEB', 'sender_id', '2022-08-03 08:56:47', '2022-08-03 08:56:47'),
(70, 226, 'BeamTEXT', NULL, '2022-08-03 09:00:35', '2022-08-03 09:00:35'),
(71, 227, 'AB-HACWEB', NULL, '2022-08-03 10:54:50', '2022-08-03 10:54:50'),
(72, 228, 'BeamText', 'sender_id', '2022-08-04 09:24:46', '2022-08-04 09:24:46'),
(73, 229, 'BeamTEXT', 'sender_id', '2022-08-04 12:02:45', '2022-08-04 12:02:45'),
(74, 230, 'BeamTEXT', 'sender_id', '2022-08-06 14:54:55', '2022-08-06 14:54:55'),
(75, 234, 'BeamTEXT', 'sender_id', '2022-08-06 15:45:30', '2022-08-06 15:45:30'),
(76, 235, 'BeamTEXT', 'sender_id', '2022-08-06 15:46:32', '2022-08-06 15:46:32'),
(77, 236, 'BeamTEXT', 'sender_id', '2022-08-06 15:47:51', '2022-08-06 15:47:51'),
(78, 237, 'BeamTEXT', 'sender_id', '2022-08-08 14:06:01', '2022-08-08 14:06:01'),
(79, 238, 'BeamTEXT', 'sender_id', '2022-08-08 14:06:36', '2022-08-08 14:06:36'),
(80, 239, 'BeamTEXT', 'sender_id', '2022-08-08 15:15:47', '2022-08-08 15:15:47'),
(81, 240, 'BeamTEXT', 'sender_id', '2022-08-08 16:17:25', '2022-08-08 16:17:25'),
(82, 241, 'BeamTEXT', 'sender_id', '2022-08-08 17:14:49', '2022-08-08 17:14:49'),
(83, 242, 'BeamTEXT', 'sender_id', '2022-08-08 17:27:59', '2022-08-08 17:27:59'),
(86, 245, 'BeamTEXT', 'sender_id', '2022-08-08 17:31:21', '2022-08-08 17:31:21'),
(87, 246, 'BeamTEXT', 'sender_id', '2022-08-08 17:32:55', '2022-08-08 17:32:55'),
(91, 250, 'BeamTEXT', 'sender_id', '2022-08-08 17:36:17', '2022-08-08 17:36:17'),
(93, 252, 'BeamTEXT', 'sender_id', '2022-08-08 17:37:24', '2022-08-08 17:37:24'),
(94, 253, 'BeamTEXT', 'sender_id', '2022-08-08 17:37:59', '2022-08-08 17:37:59'),
(95, 254, 'BeamTEXT', 'sender_id', '2022-08-08 17:38:32', '2022-08-08 17:38:32'),
(96, 255, 'BeamTEXT', 'sender_id', '2022-08-08 17:39:01', '2022-08-08 17:39:01'),
(97, 256, 'BeamTEXT', 'sender_id', '2022-08-08 17:39:16', '2022-08-08 17:39:16'),
(99, 258, 'BeamTEXT', 'sender_id', '2022-08-08 17:46:28', '2022-08-08 17:46:28'),
(100, 259, 'BeamTEXT', 'sender_id', '2022-08-08 17:47:00', '2022-08-08 17:47:00'),
(101, 260, 'BeamTEXT', 'sender_id', '2022-08-09 11:42:37', '2022-08-09 11:42:37'),
(102, 261, 'BeamTEXT', 'sender_id', '2022-08-09 11:42:41', '2022-08-09 11:42:41'),
(104, 263, 'BeamTEXT', 'sender_id', '2022-08-09 11:44:39', '2022-08-09 11:44:39'),
(105, 264, 'BeamTEXT', 'sender_id', '2022-08-09 11:45:01', '2022-08-09 11:45:01'),
(106, 265, 'BeamTEXT', 'sender_id', '2022-08-09 11:50:24', '2022-08-09 11:50:24'),
(107, 266, 'BeamTEXT', 'sender_id', '2022-08-09 11:53:31', '2022-08-09 11:53:31'),
(108, 267, 'BeamTEXT', 'sender_id', '2022-08-09 11:53:47', '2022-08-09 11:53:47'),
(110, 269, 'BeamTEXT', 'sender_id', '2022-08-09 12:00:50', '2022-08-09 12:00:50'),
(111, 270, 'BeamTEXT', 'sender_id', '2022-08-09 12:32:00', '2022-08-09 12:32:00'),
(112, 271, 'BeamTEXT', 'sender_id', '2022-08-09 12:40:19', '2022-08-09 12:40:19'),
(113, 272, 'BeamTEXT', 'sender_id', '2022-08-09 12:41:44', '2022-08-09 12:41:44'),
(114, 273, 'BeamTEXT', 'sender_id', '2022-08-09 12:42:15', '2022-08-09 12:42:15'),
(115, 274, 'BeamTEXT', 'sender_id', '2022-08-09 12:42:58', '2022-08-09 12:42:58'),
(116, 275, 'BeamTEXT', 'sender_id', '2022-08-09 12:48:24', '2022-08-09 12:48:24'),
(117, 276, 'BeamTEXT', 'sender_id', '2022-08-09 12:48:40', '2022-08-09 12:48:40'),
(118, 277, 'BeamTEXT', 'sender_id', '2022-08-09 12:50:10', '2022-08-09 12:50:10'),
(119, 278, 'BeamTEXT', 'sender_id', '2022-08-09 12:50:54', '2022-08-09 12:50:54'),
(120, 279, 'BeamTEXT', 'sender_id', '2022-08-09 12:51:53', '2022-08-09 12:51:53'),
(121, 280, 'BeamTEXT', 'sender_id', '2022-08-09 12:52:16', '2022-08-09 12:52:16'),
(122, 281, 'BeamTEXT', 'sender_id', '2022-08-09 13:01:00', '2022-08-09 13:01:00'),
(123, 282, 'BeamTEXT', 'sender_id', '2022-08-09 13:14:53', '2022-08-09 13:14:53'),
(124, 283, 'BeamTEXT', 'sender_id', '2022-08-09 13:14:57', '2022-08-09 13:14:57'),
(125, 284, 'BeamTEXT', 'sender_id', '2022-08-09 13:15:41', '2022-08-09 13:15:41'),
(129, 288, 'BeamTEXT', 'sender_id', '2022-08-09 13:24:54', '2022-08-09 13:24:54'),
(130, 289, 'BeamTEXT', 'sender_id', '2022-08-09 13:25:33', '2022-08-09 13:25:33'),
(131, 290, 'BeamTEXT', 'sender_id', '2022-08-09 13:25:52', '2022-08-09 13:25:52'),
(132, 291, 'BeamTEXT', 'sender_id', '2022-08-09 13:37:56', '2022-08-09 13:37:56'),
(133, 292, 'BeamTEXT', 'sender_id', '2022-08-09 13:38:46', '2022-08-09 13:38:46'),
(136, 295, 'BeamTEXT', 'sender_id', '2022-08-09 14:15:32', '2022-08-09 14:15:32'),
(137, 296, 'BeamTEXT', 'sender_id', '2022-08-09 14:20:57', '2022-08-09 14:20:57'),
(138, 297, 'BeamTEXT', 'sender_id', '2022-08-09 14:22:11', '2022-08-09 14:22:11'),
(139, 298, 'BeamTEXT', 'sender_id', '2022-08-09 14:29:50', '2022-08-09 14:29:50'),
(140, 299, 'BeamTEXT', 'sender_id', '2022-08-09 14:30:06', '2022-08-09 14:30:06'),
(141, 300, 'BeamTEXT', 'sender_id', '2022-08-09 14:33:59', '2022-08-09 14:33:59'),
(143, 302, 'BeamTEXT', 'sender_id', '2022-08-09 14:35:40', '2022-08-09 14:35:40'),
(144, 303, 'BeamTEXT', 'sender_id', '2022-08-10 11:38:21', '2022-08-10 11:38:21'),
(145, 304, 'BeamTEXT', 'sender_id', '2022-08-10 16:34:06', '2022-08-10 16:34:06'),
(146, 305, 'BeamTEXT', 'sender_id', '2022-08-12 14:57:12', '2022-08-12 14:57:12'),
(147, 306, 'BeamTEXT', 'sender_id', '2022-08-12 14:57:52', '2022-08-12 14:57:52'),
(148, 307, 'BeamTEXT', 'sender_id', '2022-08-12 14:58:32', '2022-08-12 14:58:32'),
(149, 308, 'BeamTEXT', 'sender_id', '2022-08-12 14:59:05', '2022-08-12 14:59:05'),
(150, 309, 'BeamTEXT', 'sender_id', '2022-08-12 14:59:55', '2022-08-12 14:59:55'),
(151, 310, 'BeamTEXT', 'sender_id', '2022-08-12 15:06:12', '2022-08-12 15:06:12'),
(152, 311, 'BeamTEXT', 'sender_id', '2022-08-12 15:07:18', '2022-08-12 15:07:18'),
(153, 312, 'BeamTEXT', 'sender_id', '2022-08-12 15:07:52', '2022-08-12 15:07:52'),
(154, 313, 'BeamTEXT', 'sender_id', '2022-08-12 15:10:34', '2022-08-12 15:10:34'),
(156, 315, 'BeamTEXT', 'sender_id', '2022-08-12 15:12:51', '2022-08-12 15:12:51'),
(157, 316, 'BeamTEXT', 'sender_id', '2022-08-12 15:14:08', '2022-08-12 15:14:08'),
(158, 317, 'BeamTEXT', 'sender_id', '2022-08-12 15:25:06', '2022-08-12 15:25:06'),
(159, 318, 'BeamTEXT', 'sender_id', '2022-08-12 15:43:43', '2022-08-12 15:43:43'),
(160, 319, 'BeamTEXT', 'sender_id', '2022-08-12 15:53:15', '2022-08-12 15:53:15'),
(161, 320, 'BeamTEXT', 'sender_id', '2022-08-12 15:53:33', '2022-08-12 15:53:33'),
(162, 321, 'BeamTEXT', 'sender_id', '2022-08-12 15:55:10', '2022-08-12 15:55:10'),
(163, 322, 'BeamTEXT', 'sender_id', '2022-08-12 15:55:22', '2022-08-12 15:55:22'),
(164, 323, 'BeamTEXT', 'sender_id', '2022-08-12 15:57:17', '2022-08-12 15:57:17'),
(165, 324, 'BeamTEXT', 'sender_id', '2022-08-12 16:02:18', '2022-08-12 16:02:18'),
(166, 325, 'BeamTEXT', 'sender_id', '2022-08-12 16:02:52', '2022-08-12 16:02:52'),
(167, 326, 'BeamTEXT', 'sender_id', '2022-08-12 16:05:07', '2022-08-12 16:05:07'),
(168, 327, 'BeamTEXT', 'sender_id', '2022-08-12 16:05:41', '2022-08-12 16:05:41'),
(169, 328, 'BeamTEXT', 'sender_id', '2022-08-12 16:07:06', '2022-08-12 16:07:06'),
(170, 329, 'BeamTEXT', 'sender_id', '2022-08-13 12:06:04', '2022-08-13 12:06:04'),
(171, 330, 'BeamTEXT', 'sender_id', '2022-08-13 12:29:29', '2022-08-13 12:29:29'),
(172, 331, 'BeamTEXT', 'sender_id', '2022-08-16 11:56:37', '2022-08-16 11:56:37'),
(173, 332, 'BeamTEXT', 'sender_id', '2022-08-16 13:45:57', '2022-08-16 13:45:57'),
(174, 333, 'BeamTEXT', 'sender_id', '2022-08-16 14:08:18', '2022-08-16 14:08:18'),
(175, 334, 'BeamTEXT', 'sender_id', '2022-08-16 23:11:28', '2022-08-16 23:11:28'),
(176, 335, 'BeamTEXT', 'sender_id', '2022-08-16 23:41:15', '2022-08-16 23:41:15'),
(177, 336, 'BeamTEXT', 'sender_id', '2022-08-19 15:37:59', '2022-08-19 15:37:59'),
(178, 337, 'BeamTEXT', 'sender_id', '2022-08-19 15:38:28', '2022-08-19 15:38:28'),
(179, 338, 'BeamTEXT', 'sender_id', '2022-08-19 15:39:30', '2022-08-19 15:39:30'),
(180, 339, 'BeamTEXT', 'sender_id', '2022-08-19 15:44:22', '2022-08-19 15:44:22'),
(181, 340, 'BeamTEXT', 'sender_id', '2022-09-01 14:05:25', '2022-09-01 14:05:25'),
(182, 341, 'BeamTEXT', NULL, '2022-09-01 14:16:28', '2022-09-01 14:16:28'),
(183, 342, 'BeamTEXT', 'sender_id', '2022-09-01 14:17:00', '2022-09-01 14:17:00'),
(184, 343, 'BeamTEXT', 'sender_id', '2022-09-01 14:33:22', '2022-09-01 14:33:22'),
(185, 344, 'BeamTEXT', 'sender_id', '2022-09-01 14:50:53', '2022-09-01 14:50:53'),
(192, 351, 'BeamTEXT', NULL, '2022-09-05 14:05:37', '2022-09-05 14:05:37'),
(198, 357, 'BeamTEXT', NULL, '2022-09-05 14:41:30', '2022-09-05 14:41:30'),
(203, 362, 'BeamTEXT', NULL, '2022-09-05 14:51:37', '2022-09-05 14:51:37'),
(204, 363, 'BeamTEXT', NULL, '2022-09-05 14:56:06', '2022-09-05 14:56:06'),
(205, 364, 'BeamTEXT', NULL, '2022-09-05 14:59:19', '2022-09-05 14:59:19'),
(229, 388, 'BeamTEXT', 'sender_id', '2022-09-05 17:09:52', '2022-09-05 17:09:52'),
(230, 389, 'BeamTEXT', 'sender_id', '2022-09-05 17:12:08', '2022-09-05 17:12:08'),
(231, 390, 'BeamTEXT', 'sender_id', '2022-09-05 17:16:59', '2022-09-05 17:16:59'),
(232, 391, 'BeamTEXT', 'sender_id', '2022-09-05 17:17:50', '2022-09-05 17:17:50'),
(233, 392, 'BeamTEXT', 'sender_id', '2022-09-05 17:18:18', '2022-09-05 17:18:18'),
(234, 393, 'BeamTEXT', 'sender_id', '2022-09-05 17:18:20', '2022-09-05 17:18:20'),
(235, 394, 'BeamTEXT', 'sender_id', '2022-09-05 17:19:26', '2022-09-05 17:19:26'),
(236, 395, 'BeamTEXT', 'sender_id', '2022-09-05 17:21:54', '2022-09-05 17:21:54'),
(237, 396, 'BeamTEXT', 'sender_id', '2022-09-05 17:22:31', '2022-09-05 17:22:31'),
(238, 397, 'BeamTEXT', 'sender_id', '2022-09-05 17:23:22', '2022-09-05 17:23:22'),
(239, 398, 'BeamTEXT', 'sender_id', '2022-09-05 17:24:27', '2022-09-05 17:24:27'),
(240, 399, 'BeamTEXT', 'sender_id', '2022-09-05 17:25:08', '2022-09-05 17:25:08'),
(241, 400, 'BeamTEXT', 'sender_id', '2022-09-05 17:25:20', '2022-09-05 17:25:20'),
(242, 401, 'BeamTEXT', 'sender_id', '2022-09-05 17:26:10', '2022-09-05 17:26:10'),
(243, 402, 'BeamTEXT', 'sender_id', '2022-09-05 17:33:12', '2022-09-05 17:33:12'),
(244, 403, 'BeamTEXT', 'sender_id', '2022-09-08 00:48:51', '2022-09-08 00:48:51'),
(247, 406, 'BeamTEXT', 'sender_id', '2022-09-08 01:07:05', '2022-09-08 01:07:05'),
(248, 407, 'BeamTEXT', 'sender_id', '2022-09-08 01:49:56', '2022-09-08 01:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `cg_campaigns_sending_servers`
--

CREATE TABLE `cg_campaigns_sending_servers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `sending_server_id` bigint(20) UNSIGNED NOT NULL,
  `fitness` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_campaigns_sending_servers`
--

INSERT INTO `cg_campaigns_sending_servers` (`id`, `campaign_id`, `sending_server_id`, `fitness`, `created_at`, `updated_at`) VALUES
(26, 148, 1, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(27, 148, 2, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(28, 148, 3, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(29, 148, 4, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(30, 148, 5, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(31, 148, 6, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(32, 148, 7, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(33, 148, 9, 100, '2022-07-26 10:01:18', '2022-07-26 10:01:18'),
(35, 149, 1, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(36, 149, 2, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(37, 149, 3, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(38, 149, 4, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(39, 149, 5, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(40, 149, 6, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(41, 149, 7, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(42, 149, 9, 100, '2022-07-26 10:25:11', '2022-07-26 10:25:11'),
(44, 150, 1, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(45, 150, 2, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(46, 150, 3, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(47, 150, 4, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(48, 150, 5, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(49, 150, 6, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(50, 150, 7, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(51, 150, 9, 100, '2022-07-26 10:25:40', '2022-07-26 10:25:40'),
(53, 151, 1, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(54, 151, 2, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(55, 151, 3, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(56, 151, 4, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(57, 151, 5, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(58, 151, 6, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(59, 151, 7, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(60, 151, 9, 100, '2022-07-26 10:31:15', '2022-07-26 10:31:15'),
(62, 159, 1, 100, '2022-07-26 10:56:19', '2022-07-26 10:56:19'),
(63, 163, 1, 100, '2022-07-26 11:21:35', '2022-07-26 11:21:35'),
(64, 164, 1, 100, '2022-07-26 11:26:50', '2022-07-26 11:26:50'),
(65, 166, 1, 100, '2022-07-26 11:30:01', '2022-07-26 11:30:01'),
(66, 170, 1, 100, '2022-07-26 11:34:34', '2022-07-26 11:34:34'),
(67, 171, 1, 100, '2022-07-26 11:35:24', '2022-07-26 11:35:24'),
(68, 179, 1, 100, '2022-07-26 11:58:58', '2022-07-26 11:58:58'),
(69, 181, 1, 100, '2022-07-26 12:21:00', '2022-07-26 12:21:00'),
(70, 182, 1, 100, '2022-07-26 12:22:55', '2022-07-26 12:22:55'),
(71, 183, 1, 100, '2022-07-26 12:29:33', '2022-07-26 12:29:33'),
(72, 184, 1, 100, '2022-07-26 12:29:37', '2022-07-26 12:29:37'),
(74, 186, 1, 100, '2022-07-26 12:30:47', '2022-07-26 12:30:47'),
(76, 188, 1, 100, '2022-07-26 12:57:28', '2022-07-26 12:57:28'),
(77, 192, 1, 100, '2022-07-26 13:04:24', '2022-07-26 13:04:24'),
(78, 193, 1, 100, '2022-07-26 13:05:18', '2022-07-26 13:05:18'),
(79, 195, 15, 100, '2022-07-26 14:53:23', '2022-07-26 14:53:23'),
(80, 196, 1, 100, '2022-07-26 14:55:20', '2022-07-26 14:55:20'),
(81, 197, 1, 100, '2022-07-26 14:57:26', '2022-07-26 14:57:26'),
(82, 198, 15, 100, '2022-07-26 14:59:09', '2022-07-26 14:59:09'),
(83, 199, 15, 100, '2022-07-26 15:05:40', '2022-07-26 15:05:40'),
(84, 200, 15, 100, '2022-07-26 15:12:33', '2022-07-26 15:12:33'),
(85, 201, 15, 100, '2022-07-26 15:13:59', '2022-07-26 15:13:59'),
(86, 202, 15, 100, '2022-07-26 15:15:18', '2022-07-26 15:15:18'),
(87, 203, 1, 100, '2022-07-26 16:39:07', '2022-07-26 16:39:07'),
(88, 206, 1, 100, '2022-07-27 01:40:20', '2022-07-27 01:40:20'),
(89, 207, 1, 100, '2022-07-27 01:43:43', '2022-07-27 01:43:43'),
(90, 208, 1, 100, '2022-07-27 08:57:24', '2022-07-27 08:57:24'),
(93, 211, 15, 100, '2022-07-27 10:39:27', '2022-07-27 10:39:27'),
(94, 212, 15, 100, '2022-07-27 10:47:24', '2022-07-27 10:47:24'),
(95, 213, 16, 100, '2022-07-28 15:06:43', '2022-07-28 15:06:43'),
(96, 214, 16, 100, '2022-07-30 12:42:48', '2022-07-30 12:42:48'),
(97, 215, 16, 100, '2022-07-30 12:47:11', '2022-07-30 12:47:11'),
(98, 216, 16, 100, '2022-07-30 13:33:31', '2022-07-30 13:33:31'),
(99, 222, 15, 100, '2022-08-02 15:06:41', '2022-08-02 15:06:41'),
(100, 223, 17, 100, '2022-08-03 00:42:51', '2022-08-03 00:42:51'),
(101, 224, 15, 100, '2022-08-03 08:55:01', '2022-08-03 08:55:01'),
(102, 225, 15, 100, '2022-08-03 08:56:47', '2022-08-03 08:56:47'),
(103, 226, 9, 100, '2022-08-03 09:00:35', '2022-08-03 09:00:35'),
(104, 227, 15, 100, '2022-08-03 10:54:50', '2022-08-03 10:54:50'),
(105, 228, 15, 100, '2022-08-04 09:24:46', '2022-08-04 09:24:46'),
(106, 228, 15, 100, '2022-08-04 09:25:28', '2022-08-04 09:25:28'),
(107, 229, 17, 100, '2022-08-04 12:02:45', '2022-08-04 12:02:45'),
(108, 230, 19, 100, '2022-08-06 14:54:55', '2022-08-06 14:54:55'),
(109, 234, 19, 100, '2022-08-06 15:45:31', '2022-08-06 15:45:31'),
(110, 235, 19, 100, '2022-08-06 15:46:32', '2022-08-06 15:46:32'),
(111, 236, 19, 100, '2022-08-06 15:47:51', '2022-08-06 15:47:51'),
(112, 237, 19, 100, '2022-08-08 14:06:01', '2022-08-08 14:06:01'),
(113, 238, 19, 100, '2022-08-08 14:06:36', '2022-08-08 14:06:36'),
(114, 239, 19, 100, '2022-08-08 15:15:47', '2022-08-08 15:15:47'),
(115, 240, 19, 100, '2022-08-08 16:17:25', '2022-08-08 16:17:25'),
(116, 241, 19, 100, '2022-08-08 17:14:49', '2022-08-08 17:14:49'),
(117, 242, 19, 100, '2022-08-08 17:27:59', '2022-08-08 17:27:59'),
(120, 245, 19, 100, '2022-08-08 17:31:21', '2022-08-08 17:31:21'),
(121, 246, 19, 100, '2022-08-08 17:32:55', '2022-08-08 17:32:55'),
(125, 250, 19, 100, '2022-08-08 17:36:17', '2022-08-08 17:36:17'),
(127, 252, 19, 100, '2022-08-08 17:37:24', '2022-08-08 17:37:24'),
(128, 253, 19, 100, '2022-08-08 17:37:59', '2022-08-08 17:37:59'),
(129, 254, 19, 100, '2022-08-08 17:38:32', '2022-08-08 17:38:32'),
(130, 255, 19, 100, '2022-08-08 17:39:01', '2022-08-08 17:39:01'),
(131, 256, 19, 100, '2022-08-08 17:39:16', '2022-08-08 17:39:16'),
(133, 258, 19, 100, '2022-08-08 17:46:28', '2022-08-08 17:46:28'),
(134, 259, 19, 100, '2022-08-08 17:47:00', '2022-08-08 17:47:00'),
(135, 260, 19, 100, '2022-08-09 11:42:37', '2022-08-09 11:42:37'),
(136, 261, 19, 100, '2022-08-09 11:42:41', '2022-08-09 11:42:41'),
(138, 263, 19, 100, '2022-08-09 11:44:39', '2022-08-09 11:44:39'),
(139, 264, 19, 100, '2022-08-09 11:45:01', '2022-08-09 11:45:01'),
(140, 265, 19, 100, '2022-08-09 11:50:24', '2022-08-09 11:50:24'),
(141, 266, 19, 100, '2022-08-09 11:53:31', '2022-08-09 11:53:31'),
(142, 267, 19, 100, '2022-08-09 11:53:47', '2022-08-09 11:53:47'),
(144, 269, 19, 100, '2022-08-09 12:00:50', '2022-08-09 12:00:50'),
(145, 270, 19, 100, '2022-08-09 12:32:00', '2022-08-09 12:32:00'),
(146, 271, 19, 100, '2022-08-09 12:40:19', '2022-08-09 12:40:19'),
(147, 272, 19, 100, '2022-08-09 12:41:44', '2022-08-09 12:41:44'),
(148, 273, 19, 100, '2022-08-09 12:42:15', '2022-08-09 12:42:15'),
(149, 274, 19, 100, '2022-08-09 12:42:58', '2022-08-09 12:42:58'),
(150, 275, 19, 100, '2022-08-09 12:48:24', '2022-08-09 12:48:24'),
(151, 276, 19, 100, '2022-08-09 12:48:40', '2022-08-09 12:48:40'),
(152, 277, 19, 100, '2022-08-09 12:50:10', '2022-08-09 12:50:10'),
(153, 278, 19, 100, '2022-08-09 12:50:54', '2022-08-09 12:50:54'),
(154, 279, 19, 100, '2022-08-09 12:51:53', '2022-08-09 12:51:53'),
(155, 280, 19, 100, '2022-08-09 12:52:16', '2022-08-09 12:52:16'),
(156, 281, 19, 100, '2022-08-09 13:01:00', '2022-08-09 13:01:00'),
(157, 282, 19, 100, '2022-08-09 13:14:53', '2022-08-09 13:14:53'),
(158, 283, 19, 100, '2022-08-09 13:14:57', '2022-08-09 13:14:57'),
(159, 284, 19, 100, '2022-08-09 13:15:41', '2022-08-09 13:15:41'),
(163, 288, 19, 100, '2022-08-09 13:24:54', '2022-08-09 13:24:54'),
(164, 289, 19, 100, '2022-08-09 13:25:33', '2022-08-09 13:25:33'),
(165, 290, 19, 100, '2022-08-09 13:25:52', '2022-08-09 13:25:52'),
(166, 291, 19, 100, '2022-08-09 13:37:56', '2022-08-09 13:37:56'),
(167, 292, 19, 100, '2022-08-09 13:38:46', '2022-08-09 13:38:46'),
(170, 295, 19, 100, '2022-08-09 14:15:32', '2022-08-09 14:15:32'),
(171, 296, 19, 100, '2022-08-09 14:20:57', '2022-08-09 14:20:57'),
(172, 297, 19, 100, '2022-08-09 14:22:11', '2022-08-09 14:22:11'),
(173, 298, 19, 100, '2022-08-09 14:29:50', '2022-08-09 14:29:50'),
(174, 299, 19, 100, '2022-08-09 14:30:06', '2022-08-09 14:30:06'),
(175, 300, 19, 100, '2022-08-09 14:33:59', '2022-08-09 14:33:59'),
(177, 302, 19, 100, '2022-08-09 14:35:40', '2022-08-09 14:35:40'),
(178, 303, 19, 100, '2022-08-10 11:38:21', '2022-08-10 11:38:21'),
(179, 304, 19, 100, '2022-08-10 16:34:06', '2022-08-10 16:34:06'),
(180, 305, 19, 100, '2022-08-12 14:57:12', '2022-08-12 14:57:12'),
(181, 306, 19, 100, '2022-08-12 14:57:52', '2022-08-12 14:57:52'),
(182, 307, 19, 100, '2022-08-12 14:58:32', '2022-08-12 14:58:32'),
(183, 308, 19, 100, '2022-08-12 14:59:05', '2022-08-12 14:59:05'),
(184, 309, 19, 100, '2022-08-12 14:59:55', '2022-08-12 14:59:55'),
(185, 310, 19, 100, '2022-08-12 15:06:12', '2022-08-12 15:06:12'),
(186, 311, 19, 100, '2022-08-12 15:07:18', '2022-08-12 15:07:18'),
(187, 312, 19, 100, '2022-08-12 15:07:52', '2022-08-12 15:07:52'),
(188, 313, 19, 100, '2022-08-12 15:10:34', '2022-08-12 15:10:34'),
(190, 315, 19, 100, '2022-08-12 15:12:51', '2022-08-12 15:12:51'),
(191, 316, 19, 100, '2022-08-12 15:14:08', '2022-08-12 15:14:08'),
(192, 317, 19, 100, '2022-08-12 15:25:06', '2022-08-12 15:25:06'),
(193, 318, 19, 100, '2022-08-12 15:43:43', '2022-08-12 15:43:43'),
(194, 319, 19, 100, '2022-08-12 15:53:15', '2022-08-12 15:53:15'),
(195, 320, 19, 100, '2022-08-12 15:53:33', '2022-08-12 15:53:33'),
(196, 321, 19, 100, '2022-08-12 15:55:10', '2022-08-12 15:55:10'),
(197, 322, 19, 100, '2022-08-12 15:55:22', '2022-08-12 15:55:22'),
(198, 323, 19, 100, '2022-08-12 15:57:17', '2022-08-12 15:57:17'),
(199, 324, 19, 100, '2022-08-12 16:02:18', '2022-08-12 16:02:18'),
(200, 325, 19, 100, '2022-08-12 16:02:52', '2022-08-12 16:02:52'),
(201, 326, 19, 100, '2022-08-12 16:05:07', '2022-08-12 16:05:07'),
(202, 327, 19, 100, '2022-08-12 16:05:41', '2022-08-12 16:05:41'),
(203, 328, 19, 100, '2022-08-12 16:07:06', '2022-08-12 16:07:06'),
(204, 329, 19, 100, '2022-08-13 12:06:04', '2022-08-13 12:06:04'),
(205, 330, 19, 100, '2022-08-13 12:29:29', '2022-08-13 12:29:29'),
(206, 331, 19, 100, '2022-08-16 11:56:37', '2022-08-16 11:56:37'),
(207, 332, 19, 100, '2022-08-16 13:45:57', '2022-08-16 13:45:57'),
(208, 333, 19, 100, '2022-08-16 14:08:18', '2022-08-16 14:08:18'),
(209, 334, 17, 100, '2022-08-16 23:11:28', '2022-08-16 23:11:28'),
(210, 335, 17, 100, '2022-08-16 23:41:15', '2022-08-16 23:41:15'),
(211, 336, 19, 100, '2022-08-19 15:37:59', '2022-08-19 15:37:59'),
(212, 337, 19, 100, '2022-08-19 15:38:28', '2022-08-19 15:38:28'),
(213, 338, 19, 100, '2022-08-19 15:39:30', '2022-08-19 15:39:30'),
(214, 339, 19, 100, '2022-08-19 15:44:22', '2022-08-19 15:44:22'),
(215, 340, 19, 100, '2022-09-01 14:05:25', '2022-09-01 14:05:25'),
(216, 341, 19, 100, '2022-09-01 14:16:28', '2022-09-01 14:16:28'),
(217, 342, 19, 100, '2022-09-01 14:17:00', '2022-09-01 14:17:00'),
(218, 343, 19, 100, '2022-09-01 14:33:22', '2022-09-01 14:33:22'),
(219, 344, 19, 100, '2022-09-01 14:50:53', '2022-09-01 14:50:53'),
(226, 351, 9, 100, '2022-09-05 14:05:37', '2022-09-05 14:05:37'),
(232, 357, 19, 100, '2022-09-05 14:41:30', '2022-09-05 14:41:30'),
(237, 362, 19, 100, '2022-09-05 14:51:37', '2022-09-05 14:51:37'),
(238, 363, 19, 100, '2022-09-05 14:56:06', '2022-09-05 14:56:06'),
(239, 364, 19, 100, '2022-09-05 14:59:19', '2022-09-05 14:59:19'),
(263, 388, 19, 100, '2022-09-05 17:09:52', '2022-09-05 17:09:52'),
(264, 389, 19, 100, '2022-09-05 17:12:08', '2022-09-05 17:12:08'),
(265, 390, 19, 100, '2022-09-05 17:16:59', '2022-09-05 17:16:59'),
(266, 391, 19, 100, '2022-09-05 17:17:50', '2022-09-05 17:17:50'),
(267, 392, 19, 100, '2022-09-05 17:18:18', '2022-09-05 17:18:18'),
(268, 393, 19, 100, '2022-09-05 17:18:20', '2022-09-05 17:18:20'),
(269, 394, 19, 100, '2022-09-05 17:19:26', '2022-09-05 17:19:26'),
(270, 395, 19, 100, '2022-09-05 17:21:54', '2022-09-05 17:21:54'),
(271, 396, 19, 100, '2022-09-05 17:22:31', '2022-09-05 17:22:31'),
(272, 397, 19, 100, '2022-09-05 17:23:22', '2022-09-05 17:23:22'),
(273, 398, 19, 100, '2022-09-05 17:24:27', '2022-09-05 17:24:27'),
(274, 399, 19, 100, '2022-09-05 17:25:08', '2022-09-05 17:25:08'),
(275, 400, 19, 100, '2022-09-05 17:25:20', '2022-09-05 17:25:20'),
(276, 401, 19, 100, '2022-09-05 17:26:10', '2022-09-05 17:26:10'),
(277, 402, 19, 100, '2022-09-05 17:33:12', '2022-09-05 17:33:12'),
(278, 403, 20, 100, '2022-09-08 00:48:51', '2022-09-08 00:48:51'),
(281, 406, 20, 100, '2022-09-08 01:07:05', '2022-09-08 01:07:05'),
(282, 407, 20, 100, '2022-09-08 01:49:56', '2022-09-08 01:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `cg_chat_boxes`
--

CREATE TABLE `cg_chat_boxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_chat_box_messages`
--

CREATE TABLE `cg_chat_box_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `box_id` bigint(20) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_url` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sms',
  `send_by` enum('from','to') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sending_server_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_contacts`
--

CREATE TABLE `cg_contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `anniversary_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_contacts`
--

INSERT INTO `cg_contacts` (`id`, `uid`, `customer_id`, `group_id`, `phone`, `status`, `first_name`, `last_name`, `email`, `username`, `company`, `address`, `birth_date`, `anniversary_date`, `created_at`, `updated_at`) VALUES
(1, '62077532853d3', 1, 1, '+919923417354', 'subscribe', 'John', '1000$', 'harshshkl@gmail.com', 'harsh_shukla', 'Ebay', 'xyz', NULL, NULL, '2022-02-12 13:52:02', '2022-02-15 21:39:59'),
(2, '6214acdc65e59', 3, 2, '919923417354', 'subscribe', 'John', 'Doe', 'john@gmail.com', 'john_doe', 'EBAY', NULL, NULL, NULL, '2022-02-22 08:29:00', '2022-02-22 08:29:21'),
(3, '62457f3a67d7c', 1, 3, '+2348127139986', 'subscribe', 'Beamtext1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-31 14:15:22', '2022-03-31 14:15:22'),
(4, '62457f4c61deb', 1, 3, '+2347031476524', 'subscribe', 'Beamtext2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-31 14:15:40', '2022-03-31 14:15:40'),
(7, '62d13efd59fdb', 3, 5, '+918837842284', 'subscribe', 'Prateek', 'Asthana', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-15 14:18:37', '2022-07-15 14:18:37'),
(8, '62d566665dcb5', 16, 6, '2347031476524', 'subscribe', 'Kola', 'Joseph', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-18 17:55:50', '2022-07-18 17:55:50'),
(9, '62d6ec2031023', 16, 9, '+2348127138896', 'subscribe', 'Harsh', 'Shukla', NULL, NULL, 'fiverr', NULL, NULL, NULL, '2022-07-19 21:38:40', '2022-07-22 01:03:58'),
(10, '62d6ec20310c7', 16, 9, '+2347031476524', 'subscribe', 'ola', 'Kano', NULL, NULL, 'ACS', NULL, NULL, NULL, '2022-07-19 21:38:40', '2022-07-22 01:05:18'),
(11, '62de58e571a9d', 3, 13, 'harsh_shukla', 'subscribe', 'fiverr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 12:48:37', '2022-07-25 12:48:37'),
(13, '62de59b91c8b9', 3, 13, '+18009009090', 'subscribe', 'Harsh', 'Shukla', 'johndoe@gmail.com', 'harsh_shukla', 'fiverr', 'test message', NULL, NULL, '2022-07-25 12:52:09', '2022-07-25 12:52:09'),
(14, '62de59b91c912', 3, 13, '+18009009091', 'subscribe', '', '', '', '', '', 'test message', NULL, NULL, '2022-07-25 12:52:09', '2022-07-25 12:52:09'),
(15, '62de5a4e29b4e', 3, 13, '+18009009090', 'subscribe', NULL, 'johndoe@gmail.com', NULL, 'harsh_shukla', NULL, NULL, NULL, NULL, '2022-07-25 12:54:38', '2022-07-25 12:54:38'),
(16, '62de5a4e29bb6', 3, 13, '+18009009091', 'subscribe', NULL, '', NULL, '', NULL, NULL, NULL, NULL, '2022-07-25 12:54:38', '2022-07-25 12:54:38'),
(17, '62de5afa153be', 3, 13, '+18009009090', 'subscribe', 'Harsh', 'Shukla', 'johndoe@gmail.com', 'harsh_shukla', 'fiverr', 'test message', NULL, NULL, '2022-07-25 12:57:30', '2022-07-25 12:57:30'),
(18, '62de5afa1540e', 3, 13, '+18009009091', 'subscribe', '', '', '', '', '', 'test message', NULL, NULL, '2022-07-25 12:57:30', '2022-07-25 12:59:01'),
(19, '62de5b421267a', 3, 13, '+18009009090', 'subscribe', NULL, NULL, 'johndoe@gmail.com', 'harsh_shukla', NULL, NULL, NULL, NULL, '2022-07-25 12:58:42', '2022-07-25 12:58:42'),
(20, '62de5b42126d4', 3, 13, '+18009009091', 'subscribe', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2022-07-25 12:58:42', '2022-07-25 12:58:42'),
(21, '62de5b6632edd', 3, 13, '566343434343', 'subscribe', '11', 'CDSV', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 12:59:18', '2022-07-25 12:59:18'),
(22, '62de5caff244a', 3, 13, '+18009009090', 'subscribe', NULL, NULL, 'johndoe@gmail.com', 'harsh_shukla', 'fiverr', NULL, NULL, NULL, '2022-07-25 13:04:47', '2022-07-25 13:04:47'),
(23, '62de5caff24b4', 3, 13, '+18009009091', 'subscribe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-25 13:04:47', '2022-07-25 13:07:17'),
(24, '62e264448ed4b', 19, 14, '+2347031476524', 'subscribe', 'HOADS', 'jOE', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-28 14:26:12', '2022-07-30 12:45:03'),
(25, '62e26b7e2dca1', 19, 14, '+2348127139986', 'subscribe', 'TOla', 'Base', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-28 14:57:02', '2022-07-30 12:45:23'),
(26, '62e4eff3338f0', 19, 14, '+2348056254493', 'subscribe', 'Nike', 'ojo', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-30 12:46:43', '2022-07-30 12:46:43'),
(27, '62e98690d42da', 20, 16, '+2348162054031', 'subscribe', 'Tunde', 'Ajala', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 00:18:24', '2022-08-04 11:57:24'),
(28, '62eb7c365a815', 20, 16, '+2349061432670', 'subscribe', 'tunde', 'ajale nu', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-04 11:58:46', '2022-08-04 11:58:46'),
(29, '62eb7cf626517', 20, 16, '+2347031476524', 'subscribe', 'Kunle', 'Joseph', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-04 12:01:58', '2022-08-04 12:01:58'),
(30, '62edfcc38f52f', 3, 13, '12', 'subscribe', 'dsgv', 'd', NULL, NULL, 'cvb', NULL, NULL, NULL, '2022-08-06 09:31:47', '2022-08-06 09:31:47'),
(38, '62f0ce77681bb', 3, 13, '1234567899', 'subscribe', 'ss', 'ss', NULL, NULL, 'hh', NULL, NULL, NULL, '2022-08-08 13:51:03', '2022-08-08 13:51:22'),
(42, '62fbdd8adcf93', 20, 16, '+2348127139986', 'subscribe', 'kj2', 'jo', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-16 23:10:18', '2022-08-16 23:10:18'),
(43, '6315da4ad077b', 23, 19, '+2348127139986', 'subscribe', 'Test', 'CDSV', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 16:15:22', '2022-09-05 17:29:56'),
(44, '6315da6c7ad36', 23, 19, '+2347031476524', 'subscribe', 'Test', 'test1', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-05 16:15:56', '2022-09-05 17:29:21'),
(45, '6318f1c1507fb', 25, 20, '+2347068840204', 'subscribe', 'Tunde', 'Ajala', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 00:32:17', '2022-09-08 01:48:56'),
(46, '6318f3a2aaaed', 25, 20, '+2347031476524', 'subscribe', 'Kunle', 'Joseph', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 00:40:18', '2022-09-08 01:48:28'),
(47, '6318f3a2aab7c', 25, 20, '+2347068840204', 'subscribe', 'Lanre', 'Ajala', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 00:40:18', '2022-09-08 01:47:59');

-- --------------------------------------------------------

--
-- Table structure for table `cg_contacts_custom_field`
--

CREATE TABLE `cg_contacts_custom_field` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_contact_groups`
--

CREATE TABLE `cg_contact_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_welcome_sms` tinyint(1) NOT NULL DEFAULT 1,
  `unsubscribe_notification` tinyint(1) NOT NULL DEFAULT 1,
  `send_keyword_message` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `signup_sms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `welcome_sms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unsubscribe_sms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cache` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_contact_groups`
--

INSERT INTO `cg_contact_groups` (`id`, `uid`, `customer_id`, `name`, `sender_id`, `send_welcome_sms`, `unsubscribe_notification`, `send_keyword_message`, `status`, `signup_sms`, `welcome_sms`, `unsubscribe_sms`, `cache`, `batch_id`, `created_at`, `updated_at`) VALUES
(1, '620774b31547f', 1, 'Demo', '18339193983', 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":1}', NULL, '2022-02-12 13:49:55', '2022-02-12 13:52:02'),
(2, '6214acc6e3ea5', 3, 'Client', NULL, 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":1}', NULL, '2022-02-22 08:28:38', '2022-07-09 16:51:07'),
(3, '62457eddeead6', 1, 'Clients', NULL, 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":2}', NULL, '2022-03-31 14:13:49', '2022-03-31 14:15:40'),
(5, '62d13ee5340de', 3, 'Prateek Asthana', NULL, 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":1}', NULL, '2022-07-15 14:18:13', '2022-07-15 14:18:37'),
(6, '62d564f3ef4d6', 16, 'Test contact group', NULL, 0, 0, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":1}', NULL, '2022-07-18 17:49:39', '2022-07-18 17:55:50'),
(9, '62d6ebc470da1', 16, 'second cg', NULL, 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":2}', '96d131d5-3c23-4266-b7d2-c9111c03f0db', '2022-07-19 21:37:08', '2022-07-19 21:38:40'),
(10, '62dd5d5d26b71', 16, 'cg test3', NULL, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-24 18:55:25', '2022-07-24 18:55:25'),
(11, '62de4e1d60562', 3, 'Rahul Rawat', NULL, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 12:02:37', '2022-07-25 12:02:37'),
(12, '62de57aaf3d2e', 3, 'ssss', NULL, 1, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 12:43:22', '2022-07-25 12:43:22'),
(13, '62de57e278204', 3, 'aa', '11', 0, 0, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":14}', '96f8b0f8-8ace-401a-9944-ba62fed3feb4', '2022-07-25 12:44:18', '2022-08-08 13:51:03'),
(14, '62e262eb5a55a', 19, 'Test 1 gp', NULL, 0, 0, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":3}', NULL, '2022-07-28 14:20:27', '2022-07-30 12:46:43'),
(15, '62e2652b537da', 19, 'Kunle Joseph', NULL, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-28 14:30:03', '2022-07-28 14:30:03'),
(16, '62e98656f3338', 20, 'Church Members', NULL, 0, 0, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":4}', NULL, '2022-08-03 00:17:26', '2022-08-16 23:10:18'),
(19, '6315d9fc87896', 23, 'Client information', NULL, 1, 1, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":2}', NULL, '2022-09-05 16:14:04', '2022-09-05 16:15:56'),
(20, '6318f0b07880e', 25, 'Cell group Ggbagada', NULL, 0, 0, 0, 1, NULL, NULL, NULL, '{\"SubscribersCount\":3}', '9735f293-3bbd-4e9a-bec5-b78d2ce2bc5c', '2022-09-08 00:27:44', '2022-09-08 00:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `cg_contact_groups_optin_keywords`
--

CREATE TABLE `cg_contact_groups_optin_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_group` bigint(20) UNSIGNED NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_contact_groups_optout_keywords`
--

CREATE TABLE `cg_contact_groups_optout_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_group` bigint(20) UNSIGNED NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_countries`
--

CREATE TABLE `cg_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_countries`
--

INSERT INTO `cg_countries` (`id`, `name`, `iso_code`, `country_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Zimbabwe', 'ZW', '263', 1, '2022-01-27 05:01:42', '2022-01-27 05:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `cg_csv_data`
--

CREATE TABLE `cg_csv_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ref_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `csv_filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `csv_header` tinyint(1) NOT NULL DEFAULT 0,
  `csv_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_csv_data`
--

INSERT INTO `cg_csv_data` (`id`, `user_id`, `ref_id`, `ref_type`, `csv_filename`, `csv_header`, `csv_data`, `created_at`, `updated_at`) VALUES
(3, 3, '62de57e278204', 'contact', 'import_file_demo.csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-07-25 12:50:41', '2022-07-25 12:50:41'),
(6, 3, '62de57e278204', 'contact', 'import_file_demo.csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-07-25 12:54:15', '2022-07-25 12:54:15'),
(10, 3, '62ea1b7aa9fc5', 'campaign', 'import_file_demo (2).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-08-03 10:53:46', '2022-08-03 10:53:46'),
(13, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:31:59', '2022-08-06 09:31:59'),
(14, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:34:37', '2022-08-06 09:34:37'),
(15, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:39:57', '2022-08-06 09:39:57'),
(16, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:46:40', '2022-08-06 09:46:40'),
(17, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:48:32', '2022-08-06 09:48:32'),
(18, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:49:19', '2022-08-06 09:49:19'),
(19, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:49:53', '2022-08-06 09:49:53'),
(20, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:50:29', '2022-08-06 09:50:29'),
(21, 3, '62de57e278204', 'contact', 'ContactGroups (3).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12\",\"cvb\",\"dsgv\",\"d\"]]', '2022-08-06 09:50:45', '2022-08-06 09:50:45'),
(22, 23, '62ee5567830d9', 'contact', 'import_file_demo (5).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-08-06 15:54:31', '2022-08-06 15:54:31'),
(24, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"]]', '2022-08-08 11:40:59', '2022-08-08 11:40:59'),
(26, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899hii\",\"\",\"ss\",\"ss\"]]', '2022-08-08 11:43:59', '2022-08-08 11:43:59'),
(27, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899hii\",\"\",\"ss\",\"ss\"]]', '2022-08-08 11:47:58', '2022-08-08 11:47:58'),
(28, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899hii\",\"\",\"ss\",\"ss\"]]', '2022-08-08 11:53:38', '2022-08-08 11:53:38'),
(29, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899hii\",\"\",\"ss\",\"ss\"]]', '2022-08-08 11:54:08', '2022-08-08 11:54:08'),
(30, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899hii\",\"\",\"ss\",\"ss\"]]', '2022-08-08 11:54:39', '2022-08-08 11:54:39'),
(38, 23, '62ee5567830d9', 'contact', 'ContactGroups.csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"1234567899\",\"hh\",\"ss\",\"ss\"]]', '2022-08-08 17:57:54', '2022-08-08 17:57:54'),
(40, 23, '631079afcdd26', 'campaign', 'import_file_demo (5).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-09-01 14:21:51', '2022-09-01 14:21:51'),
(41, 23, '63108010677fe', 'campaign', 'import_file_demo (6).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-09-01 14:49:04', '2022-09-01 14:49:04'),
(42, 23, '6310833b93f1e', 'campaign', 'import_file_demo (9).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-09-01 15:02:35', '2022-09-01 15:02:35'),
(43, 23, '6310839fd6cf7', 'campaign', 'import_file_demo (9).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-09-01 15:04:15', '2022-09-01 15:04:15'),
(44, 23, '631083bfdb03d', 'campaign', 'import_file_demo (6).csv', 1, '[[\"Phone Number\",\"Email Address\",\"User name\",\"Company\",\"First Name\",\"Last Name\",\"Message\"],[\"+18009009090\",\"johndoe@gmail.com\",\"harsh_shukla\",\"fiverr\",\"Harsh\",\"Shukla\",\"test message\"],[\"+18009009091\",\"\",\"\",\"\",\"\",\"\",\"test message\"]]', '2022-09-01 15:04:47', '2022-09-01 15:04:47'),
(45, 23, '631083e9a89ea', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 15:05:29', '2022-09-01 15:05:29'),
(46, 23, '631083f5aeeed', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 15:05:41', '2022-09-01 15:05:41'),
(47, 23, '6310841710b22', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 15:06:15', '2022-09-01 15:06:15'),
(48, 23, '631085f084a7a', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 15:14:08', '2022-09-01 15:14:08'),
(49, 23, '63108e298c006', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 15:49:13', '2022-09-01 15:49:13'),
(50, 23, '63109223772cd', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 16:06:11', '2022-09-01 16:06:11'),
(51, 23, '6310994e5dc20', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 16:36:46', '2022-09-01 16:36:46'),
(52, 23, '6310998c2ec3c', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 16:37:48', '2022-09-01 16:37:48'),
(53, 23, '631099bfc77c0', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 16:38:39', '2022-09-01 16:38:39'),
(54, 23, '631099f25bd66', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-01 16:39:30', '2022-09-01 16:39:30'),
(55, 23, '6315c2fb00b6d', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:35:55', '2022-09-05 14:35:55'),
(56, 23, '6315c351d4e56', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:37:21', '2022-09-05 14:37:21'),
(57, 23, '6315c3a31f3ed', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"12233\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:38:43', '2022-09-05 14:38:43'),
(58, 23, '6315c42ef21aa', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:41:02', '2022-09-05 14:41:02'),
(59, 23, '6315c4d99c557', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:43:53', '2022-09-05 14:43:53'),
(60, 23, '6315c58c57b5f', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:46:52', '2022-09-05 14:46:52'),
(61, 23, '6315c69eb028f', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:51:26', '2022-09-05 14:51:26'),
(62, 23, '6315c7aa04839', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:55:54', '2022-09-05 14:55:54'),
(63, 23, '6315c86d759fe', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 14:59:09', '2022-09-05 14:59:09'),
(64, 23, '6315c9cfab102', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 15:05:03', '2022-09-05 15:05:03'),
(65, 23, '6315cf07d103a', 'campaign', 'ContactGroups (6).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"8127139986\",\"dvdf\",\"dvgrf\",\"vdfv\"]]', '2022-09-05 15:27:19', '2022-09-05 15:27:19'),
(66, 25, '6318f0b07880e', 'contact', 'ContactGroups (2).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"07031476524\",\"\",\"Kunle\",\"Joseph\"],[\"07068840204\",\"\",\"Lanre\",\"Ajala\"]]', '2022-09-08 00:38:46', '2022-09-08 00:38:46'),
(68, 25, '6318f6827a673', 'campaign', 'ContactGroups (2).csv', 1, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"07031476524\",\"\",\"Kunle\",\"Joseph\"],[\"07068840204\",\"\",\"Lanre\",\"Ajala\"]]', '2022-09-08 00:52:34', '2022-09-08 00:52:34'),
(69, 25, '6318f6c0691eb', 'campaign', 'ContactGroups (2).csv', 0, '[[\"Phone Number\",\"Company\",\"First Name\",\"Last Name\"],[\"07031476524\",\"\",\"Kunle\",\"Joseph\"],[\"07068840204\",\"\",\"Lanre\",\"Ajala\"]]', '2022-09-08 00:53:36', '2022-09-08 00:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `cg_currencies`
--

CREATE TABLE `cg_currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_currencies`
--

INSERT INTO `cg_currencies` (`id`, `uid`, `user_id`, `name`, `code`, `format`, `status`, `created_at`, `updated_at`) VALUES
(1, '61f18c97ed309', 1, 'US Dollar', 'USD', '${PRICE}', 1, '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(2, '61f18c98000e3', 1, 'EURO', 'EUR', '€{PRICE}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(3, '61f18c98076ac', 1, 'British Pound', 'GBP', '£{PRICE}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(4, '61f18c980ecaa', 1, 'Japanese Yen', 'JPY', '¥{PRICE}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(5, '61f18c981d708', 1, 'Russian Ruble', 'RUB', '‎₽{PRICE}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(6, '61f18c9824b18', 1, 'Vietnam Dong', 'VND', '{PRICE}₫', 0, '2022-01-27 05:02:00', '2022-08-03 00:59:09'),
(7, '61f18c982c083', 1, 'Brazilian Real', 'BRL', '‎R${PRICE}', 0, '2022-01-27 05:02:00', '2022-08-03 00:59:09'),
(8, '61f18c983ab27', 1, 'Bangladeshi Taka', 'BDT', '‎৳{PRICE}', 0, '2022-01-27 05:02:00', '2022-08-03 00:59:07'),
(9, '61f18c98495fb', 1, 'Canadian Dollar', 'CAD', '‎C${PRICE}', 0, '2022-01-27 05:02:00', '2022-08-03 00:59:05'),
(10, '61f18c9850b86', 1, 'Indian rupee', 'INR', '‎₹{PRICE}', 0, '2022-01-27 05:02:00', '2022-08-03 00:59:03'),
(11, '61f18c9857f23', 1, 'Nigerian Naira', 'NGN', '‎₦{PRICE}', 1, '2022-01-27 05:02:00', '2022-04-24 18:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `cg_customers`
--

CREATE TABLE `cg_customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `parent` bigint(20) UNSIGNED DEFAULT NULL,
  `company` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `financial_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `financial_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `financial_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notifications` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_customers`
--

INSERT INTO `cg_customers` (`id`, `uid`, `user_id`, `contact_id`, `parent`, `company`, `website`, `address`, `city`, `postcode`, `financial_address`, `financial_city`, `financial_postcode`, `tax_number`, `state`, `country`, `phone`, `notifications`, `permissions`, `created_at`, `updated_at`) VALUES
(1, '61f18c99f3f0c', 1, NULL, NULL, NULL, 'https://www.beamtext.com/smsportal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"voice_campaign_builder\",\"voice_quick_send\",\"voice_bulk_messages\",\"sms_template\",\"developers\",\"access_backend\"]', '2022-01-27 05:02:02', '2022-02-18 16:42:51'),
(2, '620fdb62ec563', 3, NULL, NULL, 'ebay', 'https://www.beamtext.com/smsportal', 'DDFDR', 'DD', '', 'yyy', 'yyy', '90', NULL, 'XYZ', NULL, '91999342738', '{\"login\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"voice_campaign_builder\",\"voice_quick_send\",\"voice_bulk_messages\",\"sms_template\",\"developers\",\"access_backend\"]', '2022-02-18 16:46:10', '2022-08-04 09:16:42'),
(15, '62d3044a4dd96', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '26 olso road', 'Lagos', '101241', NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-07-16 22:32:42', '2022-07-24 19:53:13'),
(16, '62d6ffa75407e', 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '26 olso road', 'Lagos', '101241', NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-07-19 23:01:59', '2022-07-24 19:40:50'),
(17, '62e262a90c668', 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '26 olso road', 'Lagos', '101241', NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-07-28 14:19:21', '2022-07-30 13:06:56'),
(18, '62e985b28ecd2', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '26 olso road', 'Lagos', '101241', NULL, NULL, NULL, '09058728830', '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-08-03 00:14:42', '2022-09-13 02:54:23'),
(20, '62ece5da9b1b6', 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-08-05 13:41:46', '2022-08-05 13:41:46'),
(21, '62ee474413603', 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'address', 'fbgrt', '248011', NULL, NULL, NULL, '566343434343', '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-08-06 14:49:40', '2022-09-05 15:14:59'),
(22, '630d36ac8659e', 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-08-30 02:59:08', '2022-08-30 02:59:08'),
(23, '6318ef0260d18', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3 Alimosho street', 'Lagos', '00343', NULL, NULL, NULL, '08024234323', '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-09-08 00:20:34', '2022-09-08 01:03:49'),
(24, '6318fdb774795', 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '08127139986', '{\"login\":\"no\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-09-08 01:23:19', '2022-09-08 01:23:19'),
(25, '631f6d17c08b1', 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"login\":\"no\",\"tickets\":\"yes\",\"sender_id\":\"yes\",\"keyword\":\"yes\",\"subscription\":\"yes\",\"promotion\":\"yes\",\"profile\":\"yes\"}', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', '2022-09-12 22:32:07', '2022-09-12 22:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `cg_custom_sending_servers`
--

CREATE TABLE `cg_custom_sending_servers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `server_id` bigint(20) UNSIGNED NOT NULL,
  `http_request_method` enum('get','post') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'get',
  `json_encoded_post` tinyint(1) NOT NULL DEFAULT 0,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_type_accept` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `character_encoding` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ssl_certificate_verification` tinyint(1) NOT NULL DEFAULT 0,
  `authorization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multi_sms_delimiter` enum(',',';','array') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username_param` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_status` tinyint(1) NOT NULL DEFAULT 0,
  `action_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_status` tinyint(1) NOT NULL DEFAULT 0,
  `source_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_status` tinyint(1) NOT NULL DEFAULT 0,
  `destination_param` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_param` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unicode_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unicode_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unicode_status` tinyint(1) NOT NULL DEFAULT 0,
  `route_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_status` tinyint(1) NOT NULL DEFAULT 0,
  `language_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_status` tinyint(1) NOT NULL DEFAULT 0,
  `custom_one_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_one_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_one_status` tinyint(1) NOT NULL DEFAULT 0,
  `custom_two_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_two_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_two_status` tinyint(1) NOT NULL DEFAULT 0,
  `custom_three_param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_three_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_three_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_email_templates`
--

CREATE TABLE `cg_email_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_email_templates`
--

INSERT INTO `cg_email_templates` (`id`, `uid`, `name`, `slug`, `subject`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, '61f18c9866c69', 'Customer Registration', 'customer_registration', 'Welcome to {app_name}', 'Hi {first_name} {last_name},\n                                      Welcome to {app_name}! This message is an automated reply to your User Access request. Login to your User panel by using the details below:\n                                      {login_url}\n                                      Email: {email_address}\n                                      Password: {password}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(2, '61f18c986df9f', 'Customer Registration Verification', 'registration_verification', 'Registration Verification From {app_name}', 'Hi {first_name} {last_name},\n                                      Welcome to {app_name}! This message is an automated reply to your account verification request. Click the following url to verify your account:\n                                      {verification_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(3, '61f18c9875516', 'Password Reset', 'password_reset', '{app_name} New Password', 'Hi {first_name} {last_name},\n                                      Password Reset Successfully! This message is an automated reply to your password reset request. Login to your account to set up your all details by using the details below:\n                                      {login_url}\n                                      Email: {email_address}\n                                      Password: {password}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(4, '61f18c987c9af', 'Forgot Password', 'forgot_password', '{app_name} password change request', 'Hi {first_name} {last_name},\n                                      Password Reset Successfully! This message is an automated reply to your password reset request. Click this link to reset your password:\n                                      {forgot_password_link}\n                                      Notes: Until your password has been changed, your current password will remain valid. The Forgot Password Link will be available for a limited time only.', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(5, '61f18c9883f57', 'Login Notification', 'login_notification', 'Your {app_name} Login Information', 'Hi,\n                                      You successfully logged in to {app_name} on {time} from ip {ip_address}.  If you did not login, please contact our support immediately.', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(6, '61f18c988b45a', 'Customer Registration Notification', 'registration_notification', 'New customer registered to {app_name}', 'Hi,\n                                      New customer named {first_name} {last_name} registered. Login to your portal to show details.\n                                      {customer_profile_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(7, '61f18c98929bd', 'Sender ID Notification', 'sender_id_notification', 'New sender id requested to {app_name}', 'Hi,\n                                      New sender id {sender_id} requested. Login to your portal to show details.\n                                      {sender_id_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(8, '61f18c98a1548', 'Subscription Notification', 'subscription_notification', 'New subscription to {app_name}', 'Hi,\n                                      New subscription made on {app_name}. Login to your portal to show details.\n                                      {invoice_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(9, '61f18c98aff36', 'Keyword purchase Notification', 'keyword_purchase_notification', 'New keyword sale on {app_name}', 'Hi,\n                                      New keyword sale made on {app_name}. Login to your portal to show details.\n                                      {keyword_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00'),
(10, '61f18c98b747a', 'Phone number purchase Notification', 'number_purchase_notification', 'New phone number sale on {app_name}', 'Hi,\n                                      New phone number sale made on {app_name}. Login to your portal to show details.\n                                      {number_url}', 1, '2022-01-27 05:02:00', '2022-01-27 05:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `cg_failed_jobs`
--

CREATE TABLE `cg_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_failed_jobs`
--

INSERT INTO `cg_failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(1, 'database', 'default', '{\"uuid\":\"7c4d1863-bcc6-4908-a149-98de25777ca6\",\"displayName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":300,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\StoreCampaignJob\\\":13:{s:23:\\\"deleteWhenMissingModels\\\";b:1;s:14:\\\"\\u0000*\\u0000campaign_id\\\";i:2;s:7:\\\"timeout\\\";i:300;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'TypeError: Argument 2 passed to Vonage\\SMS\\Message\\SMS::__construct() must be of the type string, null given, called in /var/www/html/smsportal/app/Models/SendCampaignSMS.php on line 714 and defined in /var/www/html/smsportal/vendor/vonage/client-core/src/SMS/Message/SMS.php:40\nStack trace:\n#0 /var/www/html/smsportal/app/Models/SendCampaignSMS.php(714): Vonage\\SMS\\Message\\SMS->__construct()\n#1 /var/www/html/smsportal/app/Models/Campaigns.php(783): App\\Models\\SendCampaignSMS->sendPlainSMS()\n#2 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Collections/Traits/EnumeratesValues.php(234): App\\Models\\Campaigns->App\\Models\\{closure}()\n#3 /var/www/html/smsportal/app/Models/Campaigns.php(811): Illuminate\\Support\\Collection->each()\n#4 /var/www/html/smsportal/app/Jobs/StoreCampaignJob.php(46): App\\Models\\Campaigns->singleProcess()\n#5 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): App\\Jobs\\StoreCampaignJob->handle()\n#6 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#7 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure()\n#8 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod()\n#9 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Container.php(651): Illuminate\\Container\\BoundMethod::call()\n#10 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(128): Illuminate\\Container\\Container->call()\n#11 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}()\n#12 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}()\n#13 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(132): Illuminate\\Pipeline\\Pipeline->then()\n#14 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(120): Illuminate\\Bus\\Dispatcher->dispatchNow()\n#15 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}()\n#16 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}()\n#17 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(122): Illuminate\\Pipeline\\Pipeline->then()\n#18 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(70): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware()\n#19 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call()\n#20 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(428): Illuminate\\Queue\\Jobs\\Job->fire()\n#21 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(378): Illuminate\\Queue\\Worker->process()\n#22 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(329): Illuminate\\Queue\\Worker->runJob()\n#23 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(117): Illuminate\\Queue\\Worker->runNextJob()\n#24 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker()\n#25 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#26 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#27 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure()\n#28 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod()\n#29 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Container.php(651): Illuminate\\Container\\BoundMethod::call()\n#30 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(136): Illuminate\\Container\\Container->call()\n#31 /var/www/html/smsportal/vendor/symfony/console/Command/Command.php(299): Illuminate\\Console\\Command->execute()\n#32 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run()\n#33 /var/www/html/smsportal/vendor/symfony/console/Application.php(978): Illuminate\\Console\\Command->run()\n#34 /var/www/html/smsportal/vendor/symfony/console/Application.php(295): Symfony\\Component\\Console\\Application->doRunCommand()\n#35 /var/www/html/smsportal/vendor/symfony/console/Application.php(167): Symfony\\Component\\Console\\Application->doRun()\n#36 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Application.php(92): Symfony\\Component\\Console\\Application->run()\n#37 /var/www/html/smsportal/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(129): Illuminate\\Console\\Application->run()\n#38 /var/www/html/smsportal/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle()\n#39 {main}', '2022-02-15 02:34:02'),
(2, 'database', 'default', '{\"uuid\":\"1e107fb9-d2fc-4d9a-b3dc-1b6b110ac4d4\",\"displayName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":300,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\StoreCampaignJob\\\":13:{s:23:\\\"deleteWhenMissingModels\\\";b:1;s:14:\\\"\\u0000*\\u0000campaign_id\\\";i:131;s:7:\\\"timeout\\\";i:300;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: Undefined offset: -1 in /home/beamobgj/public_html/smsportal/app/Library/RouletteWheel.php:36\nStack trace:\n#0 /home/beamobgj/public_html/smsportal/app/Library/RouletteWheel.php(36): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Undefined offse...\', \'/home/beamobgj/...\', 36, Array)\n#1 /home/beamobgj/public_html/smsportal/app/Models/Campaigns.php(851): App\\Library\\RouletteWheel::generate(Array)\n#2 /home/beamobgj/public_html/smsportal/app/Models/Campaigns.php(727): App\\Models\\Campaigns->pickSendingServer()\n#3 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Collections/Traits/EnumeratesValues.php(234): App\\Models\\Campaigns->App\\Models\\{closure}(Object(Illuminate\\Support\\Collection), 0)\n#4 /home/beamobgj/public_html/smsportal/app/Models/Campaigns.php(759): Illuminate\\Support\\Collection->each(Object(Closure))\n#5 /home/beamobgj/public_html/smsportal/app/Jobs/StoreCampaignJob.php(46): App\\Models\\Campaigns->singleProcess()\n#6 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): App\\Jobs\\StoreCampaignJob->handle()\n#7 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#8 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#9 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#10 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Container.php(651): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#11 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(128): Illuminate\\Container\\Container->call(Array)\n#12 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\StoreCampaignJob))\n#13 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\StoreCampaignJob))\n#14 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(132): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(120): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\StoreCampaignJob), false)\n#16 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\StoreCampaignJob))\n#17 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\StoreCampaignJob))\n#18 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#19 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(70): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\StoreCampaignJob))\n#20 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#21 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(428): Illuminate\\Queue\\Jobs\\Job->fire()\n#22 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(378): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#23 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(329): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#24 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(117): Illuminate\\Queue\\Worker->runNextJob(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#25 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#26 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#27 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#28 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#29 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#30 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Container.php(651): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#31 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(136): Illuminate\\Container\\Container->call(Array)\n#32 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Command/Command.php(299): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#33 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#34 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(978): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(295): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(167): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Application.php(92): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#38 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#39 /home/beamobgj/public_html/smsportal/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#40 {main}', '2022-07-25 12:30:51'),
(3, 'database', 'default', '{\"uuid\":\"f12278b3-b30b-40ae-81e0-f4552b4d93b2\",\"displayName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":300,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\StoreCampaignJob\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\StoreCampaignJob\\\":13:{s:23:\\\"deleteWhenMissingModels\\\";b:1;s:14:\\\"\\u0000*\\u0000campaign_id\\\";i:186;s:7:\\\"timeout\\\";i:300;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'Illuminate\\Queue\\MaxAttemptsExceededException: App\\Jobs\\StoreCampaignJob has been attempted too many times or run too long. The job may have previously timed out. in /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php:750\nStack trace:\n#0 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(504): Illuminate\\Queue\\Worker->maxAttemptsExceededException(Object(Illuminate\\Queue\\Jobs\\DatabaseJob))\n#1 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(418): Illuminate\\Queue\\Worker->markJobAsFailedIfAlreadyExceedsMaxAttempts(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), 1)\n#2 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(378): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#3 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(329): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#4 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(117): Illuminate\\Queue\\Worker->runNextJob(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#5 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#6 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#7 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#8 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#9 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#10 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Container/Container.php(651): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#11 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(136): Illuminate\\Container\\Container->call(Array)\n#12 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Command/Command.php(299): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#13 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#14 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(978): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#15 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(295): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#16 /home/beamobgj/public_html/smsportal/vendor/symfony/console/Application.php(167): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#17 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Console/Application.php(92): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#18 /home/beamobgj/public_html/smsportal/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#19 /home/beamobgj/public_html/smsportal/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#20 {main}', '2022-07-26 12:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `cg_import_job_histories`
--

CREATE TABLE `cg_import_job_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `import_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'processing',
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_import_job_histories`
--

INSERT INTO `cg_import_job_histories` (`id`, `name`, `import_id`, `type`, `status`, `options`, `batch_id`, `created_at`, `updated_at`) VALUES
(1, 'ImportContacts_20220719050703', '62d6ebc470da1', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96d131d5-3c23-4266-b7d2-c9111c03f0db', '2022-07-19 21:38:03', '2022-07-19 21:38:40'),
(2, 'ImportContacts_20220725080749', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc861b-9c5c-4102-a633-1fe9e524209f', '2022-07-25 12:47:49', '2022-07-25 12:48:37'),
(3, 'ImportContacts_20220725080753', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc8790-22b1-4f30-ad7b-e7bc1385ed36', '2022-07-25 12:51:53', '2022-07-25 12:52:09'),
(4, 'ImportContacts_20220725080733', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc8829-09fc-4dd7-a2e1-9468d075ac41', '2022-07-25 12:53:33', '2022-07-25 12:54:38'),
(5, 'ImportContacts_20220725080721', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc8929-e155-4287-8803-5b7895e85ec7', '2022-07-25 12:56:21', '2022-07-25 12:57:30'),
(6, 'ImportContacts_20220725080705', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc89c8-45ff-4896-bd93-b723a74e127e', '2022-07-25 12:58:05', '2022-07-25 12:58:42'),
(7, 'ImportContacts_20220725090733', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96dc8c18-b5ac-4db9-bae4-04d3551534bf', '2022-07-25 13:04:33', '2022-07-25 13:04:48'),
(8, 'ImportCampaigns_20220803060850', '62ea1bbad88aa', 'import_campaign', 'finished', '{\"status\":\"finished\",\"message\":\"Import campaign was successfully imported.\"}', '96ee7890-30ab-455f-bdad-42815901f7b0', '2022-08-03 10:54:50', '2022-08-03 10:55:41'),
(9, 'ImportContacts_20220806050819', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f463a4-b2e2-4ab6-9c89-1d32fdc3238a', '2022-08-06 09:31:19', '2022-08-06 09:31:47'),
(10, 'ImportContacts_20220806110809', '62ee5567830d9', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f4eda1-8c75-4460-95d5-4e4b99ae668e', '2022-08-06 15:57:09', '2022-08-06 15:58:06'),
(11, 'ImportContacts_20220808070822', '62ee5567830d9', 'import_contact', 'processing', '{\"status\":\"processing\",\"message\":\"Import contacts are running\"}', NULL, '2022-08-08 11:41:22', '2022-08-08 11:41:22'),
(12, 'ImportContacts_20220808070800', '62ee5567830d9', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f898b5-59f3-4e39-a7dd-0baa7df25610', '2022-08-08 11:43:00', '2022-08-08 11:43:12'),
(13, 'ImportContacts_20220808090817', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8a0ad-59fa-446c-8be8-e24bc9ad6854', '2022-08-08 13:05:17', '2022-08-08 13:05:21'),
(14, 'ImportContacts_20220808090822', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8acdd-692c-4def-ad3d-41ef21343f8b', '2022-08-08 13:39:22', '2022-08-08 13:39:33'),
(15, 'ImportContacts_20220808090801', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8b051-6b2e-4bdd-955a-9ecd72cdb5af', '2022-08-08 13:49:01', '2022-08-08 13:49:37'),
(16, 'ImportContacts_20220808090845', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8b094-9920-4d96-a5d3-ec6d827f60b0', '2022-08-08 13:49:45', '2022-08-08 13:50:13'),
(17, 'ImportContacts_20220808090851', '62de57e278204', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8b0f8-8ace-401a-9944-ba62fed3feb4', '2022-08-08 13:50:51', '2022-08-08 13:51:03'),
(18, 'ImportContacts_20220808090811', '62ee5567830d9', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8b2e1-568f-4ef8-8478-54b7b46b4228', '2022-08-08 13:56:11', '2022-08-08 13:57:48'),
(19, 'ImportContacts_20220808090813', '62ee5567830d9', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '96f8b33f-15fc-4654-905b-f0ec3c53b3c9', '2022-08-08 13:57:13', '2022-08-08 13:58:15'),
(20, 'ImportCampaigns_20220901100928', '6310786c28ebd', 'import_campaign', 'finished', '{\"status\":\"finished\",\"message\":\"Import campaign was successfully imported.\"}', '972901c0-55fa-49b5-ac49-e42809e3d406', '2022-09-01 14:16:28', '2022-09-01 14:16:52'),
(21, 'ImportCampaigns_20220901120933', '631091856459d', 'import_campaign', 'processing', '{\"status\":\"processing\",\"message\":\"Import campaign are running\"}', NULL, '2022-09-01 16:03:33', '2022-09-01 16:03:33'),
(22, 'ImportContacts_20220907080947', '6318f0b07880e', 'import_contact', 'finished', '{\"status\":\"finished\",\"message\":\"Import contacts was successfully imported.\"}', '9735f293-3bbd-4e9a-bec5-b78d2ce2bc5c', '2022-09-08 00:39:47', '2022-09-08 00:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `cg_invoices`
--

CREATE TABLE `cg_invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `currency_id` bigint(20) UNSIGNED NOT NULL,
  `payment_method` bigint(20) UNSIGNED NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_invoices`
--

INSERT INTO `cg_invoices` (`id`, `uid`, `user_id`, `currency_id`, `payment_method`, `amount`, `type`, `description`, `transaction_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '621339b1f0b2f', 1, 1, 6, '15', 'subscription', 'Payment for sms unit', 'gga7y6cdof', 'paid', '2022-02-21 06:05:21', '2022-02-21 06:05:21'),
(2, '62651b1812537', 1, 11, 6, '500', 'subscription', 'Payment for sms unit', '062uuc9gjl', 'paid', '2022-04-24 13:40:40', '2022-04-24 13:40:40'),
(5, '62c952b7387fa', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', '81966bdfhn', 'paid', '2022-07-09 14:04:39', '2022-07-09 14:04:39'),
(6, '62c975480d778', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', '6cnqjmb63b', 'paid', '2022-07-09 16:32:08', '2022-07-09 16:32:08'),
(7, '62cbf0b321396', 3, 11, 3, '25000.00', 'subscription', 'Payment for plan Standard', 'pi_3LKJ8KJerTkfRDz21l8TdVTS', 'paid', '2022-07-11 13:43:15', '2022-07-11 13:43:15'),
(8, '62d700d970148', 17, 11, 6, '250', 'subscription', 'Payment for sms unit', 'gdrmq6ba1l', 'paid', '2022-07-19 23:07:05', '2022-07-19 23:07:05'),
(9, '62da73c0708cd', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'nmwd5xyuhu', 'paid', '2022-07-22 13:54:08', '2022-07-22 13:54:08'),
(10, '62da7573dd0e9', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', '1g583l2lvm', 'paid', '2022-07-22 14:01:23', '2022-07-22 14:01:23'),
(11, '62da92fb1a265', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', '4pmbvv868y', 'paid', '2022-07-22 16:07:23', '2022-07-22 16:07:23'),
(12, '62dbbbdab6601', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'myjotpdo1k', 'paid', '2022-07-23 13:14:02', '2022-07-23 13:14:02'),
(13, '62dbbc37d3158', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'hlgonwpet9', 'paid', '2022-07-23 13:15:35', '2022-07-23 13:15:35'),
(14, '62dbc3d524851', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'wjahjftovl', 'paid', '2022-07-23 13:48:05', '2022-07-23 13:48:05'),
(15, '62dbc95f3a778', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'lgflq4aykt', 'paid', '2022-07-23 14:11:43', '2022-07-23 14:11:43'),
(16, '62dbc9aab1027', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'w8lh403onv', 'paid', '2022-07-23 14:12:58', '2022-07-23 14:12:58'),
(17, '62dbcd404e123', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'v64ae0c3z1', 'paid', '2022-07-23 14:28:16', '2022-07-23 14:28:16'),
(18, '62dbcd976560d', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'kp2y6516xg', 'paid', '2022-07-23 14:29:43', '2022-07-23 14:29:43'),
(19, '62dbce6ceb102', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', '9nt1ljqdh5', 'paid', '2022-07-23 14:33:16', '2022-07-23 14:33:16'),
(20, '62dbd770e2fe8', 3, 11, 6, '13500.00', 'subscription', 'Payment for plan Basic', 'hbsjzkc10y', 'paid', '2022-07-23 15:11:44', '2022-07-23 15:11:44'),
(21, '62dbdcb8ee524', 3, 11, 6, '1350', 'subscription', 'Payment for plan Basic', 'df7ic9gxhb', 'paid', '2022-07-23 15:34:16', '2022-07-23 15:34:16'),
(22, '62dbdd1140e19', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '95oo8kncl7', 'paid', '2022-07-23 15:35:45', '2022-07-23 15:35:45'),
(23, '62dbe160e242e', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'xfhrkzz2iu', 'paid', '2022-07-23 15:54:08', '2022-07-23 15:54:08'),
(24, '62dbe1ad609db', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '0mfu66ce3g', 'paid', '2022-07-23 15:55:25', '2022-07-23 15:55:25'),
(25, '62dbe1d2d7995', 3, 11, 6, '5670', 'subscription', 'Payment for plan Basic', 'duloo45ymi', 'paid', '2022-07-23 15:56:02', '2022-07-23 15:56:02'),
(26, '62dbe3ae7fbeb', 3, 11, 6, '62.1', 'subscription', 'Payment for plan Basic', '7zoujzzfr8', 'paid', '2022-07-23 16:03:58', '2022-07-23 16:03:58'),
(27, '62dbe72cb9f4a', 3, 11, 6, '1080', 'subscription', 'Payment for plan Basic', '93u1ukv58w', 'paid', '2022-07-23 16:18:52', '2022-07-23 16:18:52'),
(28, '62dbe82a7ef03', 3, 11, 6, '54000', 'subscription', 'Payment for plan Basic', 'bezzmflqm8', 'paid', '2022-07-23 16:23:06', '2022-07-23 16:23:06'),
(29, '62dbe924a158a', 3, 11, 6, '4590', 'subscription', 'Payment for plan Basic', 'pustasozb3', 'paid', '2022-07-23 16:27:16', '2022-07-23 16:27:16'),
(30, '62dbe94de164d', 3, 11, 6, '2970', 'subscription', 'Payment for plan Basic', 'gd6clcmpeh', 'paid', '2022-07-23 16:27:57', '2022-07-23 16:27:57'),
(31, '62de3b6669d56', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', '28zxs3i88x', 'paid', '2022-07-25 10:42:46', '2022-07-25 10:42:46'),
(32, '62de64bad600b', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '4szd1tco92', 'paid', '2022-07-25 13:39:06', '2022-07-25 13:39:06'),
(33, '62de70424390f', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '4rqk87f5q3', 'paid', '2022-07-25 14:28:18', '2022-07-25 14:28:18'),
(34, '62de7146071d9', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'bfxrkat2rl', 'paid', '2022-07-25 14:32:38', '2022-07-25 14:32:38'),
(35, '62de72f9415c2', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', 'pgo4378lfd', 'paid', '2022-07-25 14:39:53', '2022-07-25 14:39:53'),
(36, '62de737b73b5a', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', 'oihwb3xqjd', 'paid', '2022-07-25 14:42:03', '2022-07-25 14:42:03'),
(37, '62de7428f079a', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'eqm0zeqxur', 'paid', '2022-07-25 14:44:56', '2022-07-25 14:44:56'),
(38, '62dfad43a0794', 3, 11, 6, '330480', 'subscription', 'Payment for plan Basic', 'y1rzlnkfbb', 'paid', '2022-07-26 13:00:51', '2022-07-26 13:00:51'),
(39, '62dfc6f0128db', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '0uzucl42yf', 'paid', '2022-07-26 14:50:24', '2022-07-26 14:50:24'),
(40, '62dfd40221789', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', 'vc88tqw350', 'paid', '2022-07-26 15:46:10', '2022-07-26 15:46:10'),
(41, '62dfd43e7bca1', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', 'rp88d6dvds', 'paid', '2022-07-26 15:47:10', '2022-07-26 15:47:10'),
(42, '62dfd4747d0da', 3, 11, 6, '8640', 'subscription', 'Payment for plan Basic', '9lxruaxcte', 'paid', '2022-07-26 15:48:04', '2022-07-26 15:48:04'),
(43, '62dfd7af264d0', 3, 11, 6, '1080', 'subscription', 'Payment for plan Basic', 'v0ntke537c', 'paid', '2022-07-26 16:01:51', '2022-07-26 16:01:51'),
(44, '62dfddc12358e', 3, 11, 6, '2700', 'subscription', 'Payment for plan Basic', 'vcuigjz7dt', 'paid', '2022-07-26 16:27:45', '2022-07-26 16:27:45'),
(45, '62dfddeb1e740', 3, 11, 6, '10800', 'subscription', 'Payment for plan Basic', 'io0y8mxk0f', 'paid', '2022-07-26 16:28:27', '2022-07-26 16:28:27'),
(46, '62dfdea8dd14f', 3, 11, 6, '540', 'subscription', 'Payment for plan Basic', '0dtg42dmxr', 'paid', '2022-07-26 16:31:36', '2022-07-26 16:31:36'),
(47, '62dfded73cbea', 3, 11, 6, '1080', 'subscription', 'Payment for plan Basic', 'ftz6kp2mos', 'paid', '2022-07-26 16:32:23', '2022-07-26 16:32:23'),
(48, '62e0cbf6514d7', 3, 11, 6, '5400', 'subscription', 'Payment for plan Basic', 'n5zuvh4i5k', 'paid', '2022-07-27 09:24:06', '2022-07-27 09:24:06'),
(49, '62e0d6cc6adf7', 3, 11, 6, '1350', 'subscription', 'Payment for plan Basic', 'wbmzifv90n', 'paid', '2022-07-27 10:10:20', '2022-07-27 10:10:20'),
(50, '62e0d77b165c6', 3, 11, 6, '72900', 'subscription', 'Payment for plan Basic', 'zzvgt9bbcu', 'paid', '2022-07-27 10:13:15', '2022-07-27 10:13:15'),
(51, '62e0d8b97b3d9', 3, 11, 6, '1350', 'subscription', 'Payment for plan Basic', 'u4uhthhh9o', 'paid', '2022-07-27 10:18:33', '2022-07-27 10:18:33'),
(52, '62e0d8d97785f', 3, 11, 6, '62100', 'subscription', 'Payment for plan Basic', 'myhlqtsbg6', 'paid', '2022-07-27 10:19:05', '2022-07-27 10:19:05'),
(53, '62e0d99a278a9', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'z6ssftpd5j', 'paid', '2022-07-27 10:22:18', '2022-07-27 10:22:18'),
(54, '62e0d9bc746fd', 3, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'gb9w26i9d3', 'paid', '2022-07-27 10:22:52', '2022-07-27 10:22:52'),
(55, '62e4f45110d88', 19, 11, 6, '2500000', 'subscription', 'Payment for plan Standard', 'mdn7ozm7z0', 'paid', '2022-07-30 13:05:21', '2022-07-30 13:05:21'),
(56, '62e90501023f3', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '9s43f3hykn', 'paid', '2022-08-02 15:05:37', '2022-08-02 15:05:37'),
(57, '62e989519b768', 20, 11, 6, '14400000', 'subscription', 'Payment for plan Pro', 'dej7rlkwu0', 'paid', '2022-08-03 00:30:09', '2022-08-03 00:30:09'),
(58, '62ea02f5da827', 3, 11, 6, '1080', 'subscription', 'Payment for plan Basic', 'xf4hdznmz4', 'paid', '2022-08-03 09:09:09', '2022-08-03 09:09:09'),
(59, '62ea031cd59f3', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 'osgtgsizm0', 'paid', '2022-08-03 09:09:48', '2022-08-03 09:09:48'),
(60, '62ea033f90b38', 3, 11, 6, '135000', 'subscription', 'Payment for plan Basic', 'zm4or9ch5m', 'paid', '2022-08-03 09:10:23', '2022-08-03 09:10:23'),
(61, '62ea03606000e', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', '4e6st2vnxs', 'paid', '2022-08-03 09:10:56', '2022-08-03 09:10:56'),
(62, '62ea0386319ea', 3, 11, 6, '1215000', 'subscription', 'Payment for plan Basic', 'w9nw68xegg', 'paid', '2022-08-03 09:11:34', '2022-08-03 09:11:34'),
(63, '62ea03b102b90', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 't8ztzrbunw', 'paid', '2022-08-03 09:12:17', '2022-08-03 09:12:17'),
(64, '62ea0bf7e7326', 3, 11, 6, '270', 'subscription', 'Payment for plan Basic', '3eqal0ozfh', 'paid', '2022-08-03 09:47:35', '2022-08-03 09:47:35'),
(65, '62ea0c83053d6', 3, 11, 6, '9180', 'subscription', 'Payment for plan Basic', 'vfs442kbu6', 'paid', '2022-08-03 09:49:55', '2022-08-03 09:49:55'),
(66, '62ea0c9ae0d4c', 3, 11, 6, '9180', 'subscription', 'Payment for plan Basic', 'vfs442kbu6', 'paid', '2022-08-03 09:50:18', '2022-08-03 09:50:18'),
(67, '62ea0c9e7b18c', 3, 11, 6, '9180', 'subscription', 'Payment for plan Basic', 'vfs442kbu6', 'paid', '2022-08-03 09:50:22', '2022-08-03 09:50:22'),
(68, '62ea0e29037a0', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', '855fjoallv', 'paid', '2022-08-03 09:56:57', '2022-08-03 09:56:57'),
(69, '62ea0ed21b35a', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 'of2uwu9c1d', 'paid', '2022-08-03 09:59:46', '2022-08-03 09:59:46'),
(70, '62ea131bb4d43', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 'yfll8eddjo', 'paid', '2022-08-03 10:18:03', '2022-08-03 10:18:03'),
(71, '62ea137c9cd25', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'tv30vtwrau', 'paid', '2022-08-03 10:19:40', '2022-08-03 10:19:40'),
(72, '62ea13a11bb13', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 'obreqva9fw', 'paid', '2022-08-03 10:20:17', '2022-08-03 10:20:17'),
(73, '62ea142f88e87', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '9te5yxi67p', 'paid', '2022-08-03 10:22:39', '2022-08-03 10:22:39'),
(74, '62ea147bc614b', 3, 11, 6, '5400', 'subscription', 'Payment for plan Basic', 'ip2vjjcbcu', 'paid', '2022-08-03 10:23:55', '2022-08-03 10:23:55'),
(75, '62ea147fa2dea', 3, 11, 6, '5400', 'subscription', 'Payment for plan Basic', 'ip2vjjcbcu', 'paid', '2022-08-03 10:23:59', '2022-08-03 10:23:59'),
(76, '62ea1491e2c51', 3, 11, 6, '5400', 'subscription', 'Payment for plan Basic', 'ip2vjjcbcu', 'paid', '2022-08-03 10:24:17', '2022-08-03 10:24:17'),
(77, '62ea14ebf3038', 3, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', '2v5i7nhd1b', 'paid', '2022-08-03 10:25:47', '2022-08-03 10:25:47'),
(78, '62ea1a2828460', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', 'c3y6xyd8w8', 'paid', '2022-08-03 10:48:08', '2022-08-03 10:48:08'),
(79, '62ea1a5362c4c', 3, 11, 6, '12000000', 'subscription', 'Payment for plan Pro', '96esvis4yq', 'paid', '2022-08-03 10:48:51', '2022-08-03 10:48:51'),
(80, '62ea1cdd9283e', 3, 11, 6, '13500', 'subscription', 'Payment for plan Basic', 'e6s21gi6qx', 'paid', '2022-08-03 10:59:41', '2022-08-03 10:59:41'),
(88, '62ea6d9a93aea', 3, 11, 6, '810', 'subscription', 'Payment for plan Basic', 'u86ic3qllu', 'paid', '2022-08-03 16:44:10', '2022-08-03 16:44:10'),
(89, '62ea6e47c9bad', 3, 11, 6, '1350', 'subscription', 'Payment for plan Basic', 'qj042q178q', 'paid', '2022-08-03 16:47:03', '2022-08-03 16:47:03'),
(90, '62ea707e3736b', 3, 11, 6, '1350', 'subscription', 'Payment for plan Basic', '5hlyi6c24w', 'paid', '2022-08-03 16:56:30', '2022-08-03 16:56:30'),
(91, '62eb542d9d485', 3, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'ytbpsre966', 'paid', '2022-08-04 09:07:57', '2022-08-04 09:07:57'),
(92, '62eb54a51a8c2', 3, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'ytbpsre966', 'paid', '2022-08-04 09:09:57', '2022-08-04 09:09:57'),
(93, '62eb552518335', 3, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'ytbpsre966', 'paid', '2022-08-04 09:12:05', '2022-08-04 09:12:05'),
(94, '62eb553aebbeb', 3, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'ytbpsre966', 'paid', '2022-08-04 09:12:26', '2022-08-04 09:12:26'),
(95, '62eb5623b2476', 3, 11, 6, '6210', 'subscription', 'Payment for plan Basic', '0u7oz7t8wz', 'paid', '2022-08-04 09:16:19', '2022-08-04 09:16:19'),
(96, '62ee481113f1b', 23, 11, 6, '1350000', 'subscription', 'Payment for plan Basic', 'tw4n7kkb8h', 'paid', '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(97, '62fbea002674a', 20, 11, 6, '1890000', 'subscription', 'Payment for plan Basic', '4bdmmf6t6l', 'paid', '2022-08-17 00:03:28', '2022-08-17 00:03:28'),
(98, '6315cc2ee82cc', 23, 11, 6, '3240', 'subscription', 'Payment for plan Basic', 'bd9vgqvh58', 'paid', '2022-09-05 15:15:10', '2022-09-05 15:15:10'),
(99, '6318f9384ffb0', 25, 11, 6, '270000', 'subscription', 'Payment for plan Basic', '7iewu6cvqn', 'paid', '2022-09-08 01:04:08', '2022-09-08 01:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `cg_jobs`
--

CREATE TABLE `cg_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_job_batches`
--

CREATE TABLE `cg_job_batches` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_jobs` int(11) NOT NULL,
  `pending_jobs` int(11) NOT NULL,
  `failed_jobs` int(11) NOT NULL,
  `failed_job_ids` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `finished_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_job_batches`
--

INSERT INTO `cg_job_batches` (`id`, `name`, `total_jobs`, `pending_jobs`, `failed_jobs`, `failed_job_ids`, `options`, `cancelled_at`, `created_at`, `finished_at`) VALUES
('96d131d5-3c23-4266-b7d2-c9111c03f0db', 'ImportContacts_20220719050703', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":895:{@cU4WVMfNya4SLkSxWGqnNPA6/c3GgtfmXfgZwmFRkDo=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:9;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220719050703\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:1;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000005174936600000000417eaeea\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@tceckpCXno2B7nVb1QCicIJb+uzXkI4LmenQasuZDj4=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000005174936800000000417eaeea\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1287:{@b6VLqeZWBwgEDLVxuHQRrLwaJlZ972JnWdLX9wE/GLM=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:9;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:1;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000005174936a00000000417eaeea\";}}}}', NULL, 1658252283, 1658252320),
('96dc861b-9c5c-4102-a633-1fe9e524209f', 'ImportContacts_20220725080749', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@x9MiMjMEYNiboQzfdia/fvI/HmWn1gHMTAiK8IXHxTQ=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725080749\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:2;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000071533dd2000000006beeb0d6\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@0ghNnS6w2pAjAT5FvIFKH9x7LaBZwLt71R22xWL4kSk=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000071533dd0000000006beeb0d6\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@iWBgzwtV9r60FsuB7NTvx2nfd00H83VqZiUW9rFv2c0=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:2;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000071533dde000000006beeb0d6\";}}}}', NULL, 1658738869, 1658738917),
('96dc8790-22b1-4f30-ad7b-e7bc1385ed36', 'ImportContacts_20220725080753', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@/r6tYczp05i72eycKaK1/5WbhT4O7h+LhYUFD012cT0=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725080753\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:3;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000002b8b09e70000000070239a64\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@m55biL3oU9qq7ozSQyYhE7H05fSkaEdZ/AvhVn1bZyY=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000002b8b09e50000000070239a64\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@Fx1ovE+ssJQBzfCAoiO+XgK2FmR7cBz3ZyJxvnti0dk=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:4;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000002b8b09eb0000000070239a64\";}}}}', NULL, 1658739113, 1658739129),
('96dc8829-09fc-4dd7-a2e1-9468d075ac41', 'ImportContacts_20220725080733', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@rMaB5GARo8Q4+cU5GG378eG9zTDB+8ogWtGCQ6Sef88=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725080733\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:4;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000014973d37000000002cce9425\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@bxTjeAjPa3tdKl7Ej3yjW77xHA5zYPkQIj5ZeWG11Jk=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000014973d35000000002cce9425\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@iLilNbaryDAkMAT10Fq/cuPDKnaS+xZpsuI12Nfb/gQ=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:5;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000014973d3b000000002cce9425\";}}}}', NULL, 1658739213, 1658739278),
('96dc8929-e155-4287-8803-5b7895e85ec7', 'ImportContacts_20220725080721', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@H0JxLZIbni94HHGS2a6eT4lbMo9giIbIcbLblxyJVSo=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725080721\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:5;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001aabf599000000005e10dc70\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@NWF2M4TdoGv7m6bvENRk24khGJmILQ3WGNyMbujhchQ=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001aabf59b000000005e10dc70\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@fnXmPWo0o2jkeJ7vhH24xgvI9dULIitBvmVx5ggaUvA=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:7;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001aabf595000000005e10dc70\";}}}}', NULL, 1658739381, 1658739450),
('96dc89c8-45ff-4896-bd93-b723a74e127e', 'ImportContacts_20220725080705', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@4T6zo/X2FmcVjjNjgpRyzAac5ZCTJse1VmxzxiqYZ6g=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725080705\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:6;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000076045ba0000000006dc2d1d0\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@MyiMpylYbFWq65BaizT7/VXOJvdMs3o4uBA/VTMMWCg=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000076045ba2000000006dc2d1d0\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@HKQpRiF2bPDCQ3u4jrtBmey2tsw0vqch3ZYtNJ2JNC4=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:8;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000076045bac000000006dc2d1d0\";}}}}', NULL, 1658739485, 1658739522),
('96dc8c18-b5ac-4db9-bae4-04d3551534bf', 'ImportContacts_20220725090733', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@YChFrFKEohqpQij6qPv/LbDfxOIDwsFsFNEDoORgQCc=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220725090733\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:7;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000054bf38480000000072a13ccf\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@gF4T6gKLDhGVryLCwJcKiNvRTa44e7V1DQvKRBNpHeo=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000054bf384a0000000072a13ccf\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1288:{@KJDzJ6MV9MFrF9gUp171e0GlisBJfeAYGF1L5yMZpg8=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:9;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000054bf38440000000072a13ccf\";}}}}', NULL, 1658739874, 1658739888),
('96ee7890-30ab-455f-bdad-42815901f7b0', 'ImportCampaigns_20220803060850', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1104:{@xuJlXSigpYrYQp2X1WLFkjEpmvi6KgPitxHBIWLh3lg=.a:5:{s:3:\"use\";a:3:{s:12:\"new_campaign\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:20:\"App\\Models\\Campaigns\";s:2:\"id\";i:227;s:9:\"relations\";a:4:{i:0;s:4:\"user\";i:1;s:13:\"user.customer\";i:2;s:26:\"user.customer.subscription\";i:3;s:31:\"user.customer.subscription.plan\";}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:30:\"ImportCampaigns_20220803060850\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:8;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:327:\"function (\\Illuminate\\Bus\\Batch $batch) use ($new_campaign, $import_name, $import_job) {\n                            $new_campaign->processing();\n                            $new_campaign->update([\'batch_id\' => $batch->id]);\n                            $import_job->update([\'batch_id\' => $batch->id]);\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000027f7d3b200000000523f65a7\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":761:{@dGDWG8D08Z+hh5oS3vZ2f3BJkIXw04UsvOS/So3wUwA=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:533:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                            $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                            if ($import_history) {\n                                $import_history->status  = \'failed\';\n                                $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                                $import_history->save();\n                            }\n\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000027f7d38900000000523f65a7\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1451:{@it+pZjsJvGfi61X9SHZRs6NAP29DJpwjbVu+/8ZGHBA=.a:5:{s:3:\"use\";a:2:{s:12:\"new_campaign\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:20:\"App\\Models\\Campaigns\";s:2:\"id\";i:227;s:9:\"relations\";a:4:{i:0;s:4:\"user\";i:1;s:13:\"user.customer\";i:2;s:26:\"user.customer.subscription\";i:3;s:31:\"user.customer.subscription.plan\";}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:11;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:746:\"function (\\Illuminate\\Bus\\Batch $batch) use ($new_campaign, $data) {\n                            $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                            if ($import_history) {\n                                $import_history->status  = \'finished\';\n                                $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import campaign was successfully imported.\']);\n                                $import_history->save();\n                                $new_campaign->delivered();\n                            }\n\n                            $data->delete();\n                            //send event notification remaining\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000027f7d38400000000523f65a7\";}}}}', NULL, 1659509691, 1659509741),
('96f463a4-b2e2-4ab6-9c89-1d32fdc3238a', 'ImportContacts_20220806050819', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":896:{@dIg8dlviLmaPVF3ezbII3PRZZRrUrjZAe3ZDIdXGDnk=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220806050819\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:9;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001628ab5300000000122ab124\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@DAxmvrg2hhOz9ZS6UHqFlTnl5EUewmCInBKT3BR6QCI=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001628ab5500000000122ab124\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@l8OGnRzqT++q5n197i9pX8VJXnK0ftJJNrGJj34HnjM=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:12;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001628ab5700000000122ab124\";}}}}', NULL, 1659763879, 1659763907),
('96f4eda1-8c75-4460-95d5-4e4b99ae668e', 'ImportContacts_20220806110809', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@fR2kZqa1SBuAkci7KXHgqaM3yLnl2tzVmd+VCF7tb0Q=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220806110809\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:10;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000259e971100000000444e3b2a\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@YBYwDGpKIMar+I6VXJp2//aFz5Keb84qlHu/bD0p3FY=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000259e971f00000000444e3b2a\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@leNFn4cCDHgVZbpOw/skeVJGAdTUjhPf3yobfMc9yBA=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:23;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000259e971d00000000444e3b2a\";}}}}', NULL, 1659787029, 1659787086),
('96f8981f-5f25-45e0-a459-283182d86da7', 'ImportContacts_20220808070822', 0, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@NT9CpaBE/xGq+u1SYWIEnosGoOZEI3QOAJUnT13TS0Y=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808070822\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:11;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001bd566dc000000001dffc1a8\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@vQZdDQtjBE2KElvSi5m8GmmrhtB5TY1GOk99Ih9iwcg=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001bd566de000000001dffc1a8\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@5P7ZUqKO+q4JcOM4VUFolKyIrUJRM6RSRK8j6RcOj1A=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:24;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001bd566d0000000001dffc1a8\";}}}}', NULL, 1659944482, NULL),
('96f898b5-59f3-4e39-a7dd-0baa7df25610', 'ImportContacts_20220808070800', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@nDDjjhANVOEiHQBDjthMYfw/jNo8hglLuN963Fo2YRY=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808070800\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:12;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007081d5ed00000000166702ab\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@Vi6Ynrfu3Tq8JK1uuZnoxcYHtzTUMcI9hWEnT/wVwtM=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007081d5e300000000166702ab\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@yHl5By6De1/PZK7giIIgL8m6Z9sC/dvK9AXlx1AbGpI=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:25;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007081d5e100000000166702ab\";}}}}', NULL, 1659944580, 1659944592),
('96f8a0ad-59fa-446c-8be8-e24bc9ad6854', 'ImportContacts_20220808090817', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@ti5rzPddglN/CIZOjQMU9TNLxC0dlJZ/hgBfWlauDlE=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090817\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000067e4ae70000000065fcdd3b\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@XvqdZEbvN/WvEUN6t/j3WC5CyLVTyIXG/VXzygAGreU=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000067e4ae10000000065fcdd3b\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@mLJM6WuAFsLk+JsZFPXWZiXIyA/3SKRAhMT7XAJN+5A=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:31;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000067e4ae30000000065fcdd3b\";}}}}', NULL, 1659945917, 1659945921),
('96f8acdd-692c-4def-ad3d-41ef21343f8b', 'ImportContacts_20220808090822', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@dvPr2Yxu1omDnSenRD8twUhc6h7lr1gnwQbRNLrc5uk=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090822\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:14;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000066d83f3f000000007f27bf99\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@Zjzx2i86U6S4DRtXc++xnryITd/7i1QkDDMJDJMJGVw=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000066d83f39000000007f27bf99\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@rxC8hBCOy6CITZqefJ2plLHlVEQfvCrwfFr11b+X1d8=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:32;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000066d83f3b000000007f27bf99\";}}}}', NULL, 1659947962, 1659947973);
INSERT INTO `cg_job_batches` (`id`, `name`, `total_jobs`, `pending_jobs`, `failed_jobs`, `failed_job_ids`, `options`, `cancelled_at`, `created_at`, `finished_at`) VALUES
('96f8b051-6b2e-4bdd-955a-9ecd72cdb5af', 'ImportContacts_20220808090801', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@RHTY78quey2/j6uhe9WHiDpxydrjd4ZrJEb59mXX9e0=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090801\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:15;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007f04a61d000000000c484f91\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@BYd9TDFXHUVZJmfyDXKaP4A5+/DFJ5tbjO1UZIs+gFE=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007f04a61b000000000c484f91\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@T0QhKQNNkHdRIaBxQu6uykQaDpeOs0FlWzxfFvkg7W0=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:33;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007f04a619000000000c484f91\";}}}}', NULL, 1659948541, 1659948577),
('96f8b094-9920-4d96-a5d3-ec6d827f60b0', 'ImportContacts_20220808090845', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@xzyW+b/+TGtYHOU40cww1OkEhpVJU6hP1Qi6aG0rp5k=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090845\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:16;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000026d4c10e0000000062e2e54c\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@F7nJewEDTyfzFA6728bFmvZS408Rs7GfDVD8eJphSck=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000026d4c1080000000062e2e54c\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@6+NUdn/jGwx0Vh807ly+4+D5HHhw8neel3AUC2/xHbU=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:34;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000026d4c10a0000000062e2e54c\";}}}}', NULL, 1659948585, 1659948613),
('96f8b0f8-8ace-401a-9944-ba62fed3feb4', 'ImportContacts_20220808090851', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@9Al9cIaCea/kHFxffUuLDxvRVNmeSAxzprp7Y5WccZ0=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090851\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b92d64c000000001e53138e\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@TAkGrPbJjC87CLTygPpdIiG0sgai91eymGqXg/Be8aU=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b92d64a000000001e53138e\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@J/CtA6mb+wovQbh2wi9ox6GO3/21y8Dwoqe0wi/ahXg=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:13;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:35;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b92d648000000001e53138e\";}}}}', NULL, 1659948651, 1659948663),
('96f8b2e1-568f-4ef8-8478-54b7b46b4228', 'ImportContacts_20220808090811', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@Z8iJFnGCpqDpGH+vX45unShtigL6DI8Obgc20MR12mA=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090811\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:18;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b00ae80000000004c474210\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@s12XKrrUbJbUmutstBDGDFlZVucfAsacQxCPVAxqkxs=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b00ae86000000004c474210\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@FlsJoitd5v8jQ44ESmIz+kAEyfEcUm5TE0Cfw33On3k=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:36;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000007b00ae84000000004c474210\";}}}}', NULL, 1659948971, 1659949068),
('96f8b33f-15fc-4654-905b-f0ec3c53b3c9', 'ImportContacts_20220808090813', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@U9V/sT+bltOVXvbmKTMOoAEmxN7EEKvr6upurI7lkcQ=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220808090813\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:19;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001048188e0000000038b7ee38\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@p747jxwWmPHcp/66CU57w6Dg1MOBm/GfklyvWjzAntU=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"00000000104818880000000038b7ee38\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@5VM0AEAUdylkr0ZH0fKWubC4V/QwTlioqUv0PAhETyk=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:17;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:37;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"000000001048188a0000000038b7ee38\";}}}}', NULL, 1659949033, 1659949095),
('972901c0-55fa-49b5-ac49-e42809e3d406', 'ImportCampaigns_20220901100928', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1105:{@VwR4hWHl6neRr1VJEs1Y4ZtbIRWaXuInYnsrqVGaJMA=.a:5:{s:3:\"use\";a:3:{s:12:\"new_campaign\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:20:\"App\\Models\\Campaigns\";s:2:\"id\";i:341;s:9:\"relations\";a:4:{i:0;s:4:\"user\";i:1;s:13:\"user.customer\";i:2;s:26:\"user.customer.subscription\";i:3;s:31:\"user.customer.subscription.plan\";}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:30:\"ImportCampaigns_20220901100928\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:20;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:327:\"function (\\Illuminate\\Bus\\Batch $batch) use ($new_campaign, $import_name, $import_job) {\n                            $new_campaign->processing();\n                            $new_campaign->update([\'batch_id\' => $batch->id]);\n                            $import_job->update([\'batch_id\' => $batch->id]);\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000050b49e6f0000000017ed869b\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":761:{@yzpZZBm1lcUBR2iaZYyh/3KPClSvs3SW2a1v3lIA7pk=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:533:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                            $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                            if ($import_history) {\n                                $import_history->status  = \'failed\';\n                                $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                                $import_history->save();\n                            }\n\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000050b49e540000000017ed869b\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1451:{@OmCzoLZFme2C9lmJ1ZzsXA9NvG4n7R/tc9HPKBb5/Bc=.a:5:{s:3:\"use\";a:2:{s:12:\"new_campaign\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:20:\"App\\Models\\Campaigns\";s:2:\"id\";i:341;s:9:\"relations\";a:4:{i:0;s:4:\"user\";i:1;s:13:\"user.customer\";i:2;s:26:\"user.customer.subscription\";i:3;s:31:\"user.customer.subscription.plan\";}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:39;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:746:\"function (\\Illuminate\\Bus\\Batch $batch) use ($new_campaign, $data) {\n                            $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                            if ($import_history) {\n                                $import_history->status  = \'finished\';\n                                $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import campaign was successfully imported.\']);\n                                $import_history->save();\n                                $new_campaign->delivered();\n                            }\n\n                            $data->delete();\n                            //send event notification remaining\n                        }\";s:5:\"scope\";s:52:\"App\\Repositories\\Eloquent\\EloquentCampaignRepository\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000050b49e570000000017ed869b\";}}}}', NULL, 1662023788, 1662023812),
('9735f293-3bbd-4e9a-bec5-b78d2ce2bc5c', 'ImportContacts_20220907080947', 1, 0, 0, '[]', 'a:3:{s:4:\"then\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":897:{@dJIxS74YVgAxVcTLRbVjBw5N23xKeojAntMnLeTrE0c=.a:5:{s:3:\"use\";a:3:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:20;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:11:\"import_name\";s:29:\"ImportContacts_20220907080947\";s:10:\"import_job\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:27:\"App\\Models\\ImportJobHistory\";s:2:\"id\";i:22;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:248:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $import_name, $import_job) {\n                        $contact->update([\'batch_id\' => $batch->id]);\n                        $import_job->update([\'batch_id\' => $batch->id]);\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000012aee2b300000000065b6c54\";}}}s:5:\"catch\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":729:{@9qDxmyX0jGFX+b6l5xLXKO09dFHHdOPGGLy+0CbCx4E=.a:5:{s:3:\"use\";a:0:{}s:8:\"function\";s:505:\"function (\\Illuminate\\Bus\\Batch $batch, \\Throwable $e) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'failed\';\n                            $import_history->options = json_encode([\'status\' => \'failed\', \'message\' => $e->getMessage()]);\n                            $import_history->save();\n                        }\n\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000012aee2bd00000000065b6c54\";}}}s:7:\"finally\";a:1:{i:0;C:36:\"Illuminate\\Queue\\SerializableClosure\":1289:{@fZk3BTnzMw+IJQQhnw75ihcUjBd7PyAEDzehOLTqufo=.a:5:{s:3:\"use\";a:2:{s:7:\"contact\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:24:\"App\\Models\\ContactGroups\";s:2:\"id\";i:20;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}s:4:\"data\";O:45:\"Illuminate\\Contracts\\Database\\ModelIdentifier\":4:{s:5:\"class\";s:18:\"App\\Models\\CsvData\";s:2:\"id\";i:67;s:9:\"relations\";a:0:{}s:10:\"connection\";s:5:\"mysql\";}}s:8:\"function\";s:712:\"function (\\Illuminate\\Bus\\Batch $batch) use ($contact, $data) {\n                        $import_history = \\App\\Models\\ImportJobHistory::where(\'batch_id\', $batch->id)->first();\n                        if ($import_history) {\n                            $import_history->status  = \'finished\';\n                            $import_history->options = json_encode([\'status\' => \'finished\', \'message\' => \'Import contacts was successfully imported.\']);\n                            $import_history->save();\n                        }\n\n                        $contact->updateCache(\'SubscribersCount\');\n                        $data->delete();\n                        //send event notification remaining\n                    }\";s:5:\"scope\";s:48:\"App\\Http\\Controllers\\Customer\\ContactsController\";s:4:\"this\";N;s:4:\"self\";s:32:\"0000000012aee2bf00000000065b6c54\";}}}}', NULL, 1662579588, 1662579618);

-- --------------------------------------------------------

--
-- Table structure for table `cg_keywords`
--

CREATE TABLE `cg_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `currency_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_voice` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_mms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('available','assigned','expired') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'available',
  `price` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `billing_cycle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_amount` int(11) DEFAULT NULL,
  `frequency_unit` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validity_date` date DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_languages`
--

CREATE TABLE `cg_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_languages`
--

INSERT INTO `cg_languages` (`id`, `name`, `code`, `iso_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 'us', 1, '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(2, 'German', 'de', 'de', 0, '2022-01-27 05:01:55', '2022-02-22 15:53:22'),
(3, 'French', 'fr', 'fr', 0, '2022-01-27 05:01:55', '2022-02-22 15:53:22'),
(6, 'Hindi', 'hi', 'in', 0, '2022-02-17 19:55:50', '2022-02-22 15:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `cg_migrations`
--

CREATE TABLE `cg_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_migrations`
--

INSERT INTO `cg_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_26_134739_create_app_config_table', 1),
(4, '2018_07_27_112022_create_payment_methods_table', 1),
(5, '2018_10_18_201850_create_countries_table', 1),
(6, '2018_12_01_122106_create_languages_table', 1),
(7, '2018_12_01_130207_create_contact_groups_table', 1),
(8, '2018_12_01_130424_create_contacts_table', 1),
(9, '2018_12_01_191808_create_currencies_table', 1),
(10, '2018_12_01_192942_create_customers_table', 1),
(11, '2018_12_01_193935_create_plan_table', 1),
(12, '2018_12_02_190238_setup_permission_table', 1),
(13, '2019_03_09_065029_create_subscriptions_table', 1),
(14, '2019_08_19_000000_create_failed_jobs_table', 1),
(15, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(16, '2020_02_19_092836_create_sending_servers_table', 1),
(17, '2020_02_25_121119_create_jobs_table', 1),
(18, '2020_04_27_174308_create_plans_sending_servers_table', 1),
(19, '2020_04_27_174552_create_plans_coverage_countries_table', 1),
(20, '2020_05_01_184946_create_custom_sending_servers_table', 1),
(21, '2020_05_27_084347_create_keywords_table', 1),
(22, '2020_05_30_123429_create_senderid_table', 1),
(23, '2020_05_31_175740_create_subscription_transactions_table', 1),
(24, '2020_05_31_175813_create_subscription_logs_table', 1),
(25, '2020_06_29_124959_create_email_templates_table', 1),
(26, '2020_11_14_105312_create_phone_numbers_table', 1),
(27, '2020_11_15_122755_create_blacklists_table', 1),
(28, '2020_11_17_134741_create_spam_word_table', 1),
(29, '2020_12_17_120510_create_contact_groups_optin_keywords_table', 1),
(30, '2020_12_17_120525_create_contact_groups_optout_keywords_table', 1),
(31, '2020_12_17_132523_create_contacts_custom_field_table', 1),
(32, '2021_02_07_101007_create_template_tags_table', 1),
(33, '2021_02_10_192026_create_job_batches_table', 1),
(34, '2021_02_12_135406_create_import_job_histories_table', 1),
(35, '2021_02_24_095516_create_senderid_plans_table', 1),
(36, '2021_02_27_094439_create_templates_table', 1),
(37, '2021_03_01_062609_create_campaigns_table', 1),
(38, '2021_03_02_060310_create_reports_table', 1),
(39, '2021_03_07_033941_create_campaigns_lists_table', 1),
(40, '2021_03_07_034338_create_campaigns_senderids_table', 1),
(41, '2021_03_07_035250_create_campaigns_recipients_table', 1),
(42, '2021_03_08_054924_create_campaigns_sending_servers_table', 1),
(43, '2021_03_25_135511_create_invoices_table', 1),
(44, '2021_03_31_081200_create_csv_data_table', 1),
(45, '2021_03_31_125855_create_chat_boxes_table', 1),
(46, '2021_03_31_130224_create_chat_box_messages_table', 1),
(47, '2021_04_08_140645_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cg_notifications`
--

CREATE TABLE `cg_notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `notification_for` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `notification_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark_read` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_notifications`
--

INSERT INTO `cg_notifications` (`id`, `uid`, `user_id`, `notification_for`, `notification_type`, `message`, `mark_read`, `created_at`, `updated_at`) VALUES
(93, '62cf5cb4c6d55', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-14 04:00:52', '2022-07-14 04:00:52'),
(94, '62d0ae2a3a561', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-15 04:00:42', '2022-07-15 04:00:42'),
(95, '62d1ff8bf0221', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-16 04:00:11', '2022-07-16 04:00:11'),
(96, '62d3044a511f9', 1, 'admin', 'user', 'KJ joe Registered', 0, '2022-07-16 22:32:42', '2022-07-16 22:32:42'),
(97, '62d35115577e8', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-17 04:00:21', '2022-07-17 04:00:21'),
(98, '62d5f42a47ddd', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-19 04:00:42', '2022-07-19 04:00:42'),
(99, '62d6ffa75939f', 1, 'admin', 'user', 'kj2 joe Registered', 0, '2022-07-19 23:01:59', '2022-07-19 23:01:59'),
(100, '62d745915a05a', 3, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-20 04:00:17', '2022-07-20 04:00:17'),
(101, '62d7459254fac', 16, 'customer', 'subscription', 'Your sms unit running low!!', 0, '2022-07-20 04:00:18', '2022-07-20 04:00:18'),
(102, '62da73c078af0', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-22 13:54:08', '2022-07-22 13:54:08'),
(103, '62da7573e6dfc', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-22 14:01:23', '2022-07-22 14:01:23'),
(104, '62da92fb227b5', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-22 16:07:23', '2022-07-22 16:07:23'),
(105, '62dbbbdac1bbe', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 13:14:02', '2022-07-23 13:14:02'),
(106, '62dbbc37da9b4', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 13:15:35', '2022-07-23 13:15:35'),
(107, '62dbc3d52d750', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 13:48:05', '2022-07-23 13:48:05'),
(108, '62dbc95f446cf', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 14:11:43', '2022-07-23 14:11:43'),
(109, '62dbc9aab9f7a', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 14:12:58', '2022-07-23 14:12:58'),
(110, '62dbcd4056e33', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 14:28:16', '2022-07-23 14:28:16'),
(111, '62dbcd976cfc7', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 14:29:43', '2022-07-23 14:29:43'),
(112, '62dbce6d0a5a4', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 14:33:17', '2022-07-23 14:33:17'),
(113, '62dbd770ea6d1', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:11:44', '2022-07-23 15:11:44'),
(114, '62dbdcb9014c5', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:34:17', '2022-07-23 15:34:17'),
(115, '62dbdd1149d78', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:35:45', '2022-07-23 15:35:45'),
(116, '62dbe160e949f', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:54:08', '2022-07-23 15:54:08'),
(117, '62dbe1ad65d8b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:55:25', '2022-07-23 15:55:25'),
(118, '62dbe1d2df176', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 15:56:02', '2022-07-23 15:56:02'),
(119, '62dbe3ae898bd', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 16:03:58', '2022-07-23 16:03:58'),
(120, '62dbe72cbf779', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 16:18:52', '2022-07-23 16:18:52'),
(121, '62dbe82a84965', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 16:23:06', '2022-07-23 16:23:06'),
(122, '62dbe924aadba', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 16:27:16', '2022-07-23 16:27:16'),
(123, '62dbe94dea168', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-23 16:27:57', '2022-07-23 16:27:57'),
(124, '62de3b667166e', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 10:42:46', '2022-07-25 10:42:46'),
(125, '62de64baddb0b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 13:39:06', '2022-07-25 13:39:06'),
(126, '62de7042483c1', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 14:28:18', '2022-07-25 14:28:18'),
(127, '62de71460e867', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 14:32:38', '2022-07-25 14:32:38'),
(128, '62de72f947632', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 14:39:53', '2022-07-25 14:39:53'),
(129, '62de737b7b450', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 14:42:03', '2022-07-25 14:42:03'),
(130, '62de7429012b0', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-25 14:44:57', '2022-07-25 14:44:57'),
(131, '62dfad43a831d', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 13:00:51', '2022-07-26 13:00:51'),
(132, '62dfc6f01e9d3', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 14:50:24', '2022-07-26 14:50:24'),
(133, '62dfd4022c46b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 15:46:10', '2022-07-26 15:46:10'),
(134, '62dfd43e84c1d', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 15:47:10', '2022-07-26 15:47:10'),
(135, '62dfd47484e9e', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 15:48:04', '2022-07-26 15:48:04'),
(136, '62dfd7af301ba', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 16:01:51', '2022-07-26 16:01:51'),
(137, '62dfddc134341', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 16:27:45', '2022-07-26 16:27:45'),
(138, '62dfddeb29cf9', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 16:28:27', '2022-07-26 16:28:27'),
(139, '62dfdea8e9d75', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 16:31:36', '2022-07-26 16:31:36'),
(140, '62dfded742f71', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-26 16:32:23', '2022-07-26 16:32:23'),
(141, '62e0cbf65b11b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 09:24:06', '2022-07-27 09:24:06'),
(142, '62e0d6cc75f7b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:10:20', '2022-07-27 10:10:20'),
(143, '62e0d77b1f554', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:13:15', '2022-07-27 10:13:15'),
(144, '62e0d8b9860ca', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:18:33', '2022-07-27 10:18:33'),
(145, '62e0d8d980931', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:19:05', '2022-07-27 10:19:05'),
(146, '62e0d99a2e5b6', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:22:18', '2022-07-27 10:22:18'),
(147, '62e0d9bc7ef1a', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-07-27 10:22:52', '2022-07-27 10:22:52'),
(148, '62e262a914fa5', 1, 'admin', 'user', 'Ola Faith Registered', 0, '2022-07-28 14:19:21', '2022-07-28 14:19:21'),
(149, '62e4f4511d32e', 1, 'admin', 'plan', 'Standard Purchased By Ola Faith', 0, '2022-07-30 13:05:21', '2022-07-30 13:05:21'),
(150, '62e905010d58b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-02 15:05:37', '2022-08-02 15:05:37'),
(151, '62e985b2948b2', 1, 'admin', 'user', 'Tunde Ajala Registered', 0, '2022-08-03 00:14:42', '2022-08-03 00:14:42'),
(152, '62e98951a378d', 1, 'admin', 'plan', 'Pro Purchased By Tunde Ajala', 0, '2022-08-03 00:30:09', '2022-08-03 00:30:09'),
(153, '62ea02f5ed98d', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 09:09:09', '2022-08-03 09:09:09'),
(154, '62ea031ce3c39', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 09:09:48', '2022-08-03 09:09:48'),
(155, '62ea033f9e8f9', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 09:10:23', '2022-08-03 09:10:23'),
(156, '62ea03606ed8e', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 09:10:56', '2022-08-03 09:10:56'),
(157, '62ea03863cc17', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 09:11:34', '2022-08-03 09:11:34'),
(158, '62ea03b111dd6', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 09:12:17', '2022-08-03 09:12:17'),
(159, '62ea0bf7f280f', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 09:47:35', '2022-08-03 09:47:35'),
(160, '62ea0e290f717', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 09:56:57', '2022-08-03 09:56:57'),
(161, '62ea131bbd2b3', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 10:18:03', '2022-08-03 10:18:03'),
(162, '62ea137ca736a', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 10:19:40', '2022-08-03 10:19:40'),
(163, '62ea13a1268d3', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 10:20:17', '2022-08-03 10:20:17'),
(164, '62ea142f94b4d', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 10:22:39', '2022-08-03 10:22:39'),
(165, '62ea1491f0659', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 10:24:17', '2022-08-03 10:24:17'),
(166, '62ea14ec13211', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 10:25:48', '2022-08-03 10:25:48'),
(167, '62ea1a2832a5a', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 10:48:08', '2022-08-03 10:48:08'),
(168, '62ea1a5368470', 1, 'admin', 'plan', 'Pro Purchased By Client Demo', 0, '2022-08-03 10:48:51', '2022-08-03 10:48:51'),
(169, '62ea1cdd9a933', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 10:59:41', '2022-08-03 10:59:41'),
(170, '62ea217030460', 1, 'admin', 'user', 'hii heloo Registered', 0, '2022-08-03 11:19:12', '2022-08-03 11:19:12'),
(171, '62ea21a139d2a', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:20:01', '2022-08-03 11:20:01'),
(172, '62ea2355943bf', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:27:17', '2022-08-03 11:27:17'),
(173, '62ea247977f81', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:32:09', '2022-08-03 11:32:09'),
(174, '62ea27335cfb9', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:43:47', '2022-08-03 11:43:47'),
(175, '62ea29eaa2e1a', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:55:22', '2022-08-03 11:55:22'),
(176, '62ea2a4adb3a2', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 11:56:58', '2022-08-03 11:56:58'),
(177, '62ea3a97609e6', 1, 'admin', 'plan', 'Basic Purchased By hii heloo', 0, '2022-08-03 13:06:31', '2022-08-03 13:06:31'),
(178, '62ea6d9a9ae9f', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 16:44:10', '2022-08-03 16:44:10'),
(179, '62ea6e47ed55c', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 16:47:03', '2022-08-03 16:47:03'),
(180, '62ea707e3d638', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-03 16:56:30', '2022-08-03 16:56:30'),
(181, '62eb542da4740', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-04 09:07:57', '2022-08-04 09:07:57'),
(182, '62eb54a5221ed', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-04 09:09:57', '2022-08-04 09:09:57'),
(183, '62eb55251ea41', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-04 09:12:05', '2022-08-04 09:12:05'),
(184, '62eb553af168b', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-04 09:12:26', '2022-08-04 09:12:26'),
(185, '62eb5623b8899', 1, 'admin', 'plan', 'Basic Purchased By Client Demo', 0, '2022-08-04 09:16:19', '2022-08-04 09:16:19'),
(186, '62ece5daa4495', 1, 'admin', 'user', 'Hassie Doughty Registered', 0, '2022-08-05 13:41:46', '2022-08-05 13:41:46'),
(187, '62ee47442844a', 1, 'admin', 'user', 'Test test1 Registered', 0, '2022-08-06 14:49:40', '2022-08-06 14:49:40'),
(188, '62ee48111a14a', 1, 'admin', 'plan', 'Basic Purchased By Test test1', 0, '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(189, '62fbea00372be', 1, 'admin', 'plan', 'Basic Purchased By Tunde Ajala', 0, '2022-08-17 00:03:28', '2022-08-17 00:03:28'),
(190, '630d36ac8ded7', 1, 'admin', 'user', 'Brandi Hefner Registered', 0, '2022-08-30 02:59:08', '2022-08-30 02:59:08'),
(191, '6315cc2ef1a25', 1, 'admin', 'plan', 'Basic Purchased By Test test1', 0, '2022-09-05 15:15:10', '2022-09-05 15:15:10'),
(192, '6318ef026a2d1', 1, 'admin', 'user', 'Lanre Ajala Registered', 0, '2022-09-08 00:20:34', '2022-09-08 00:20:34'),
(193, '6318f93862f6e', 1, 'admin', 'plan', 'Basic Purchased By Lanre Ajala', 0, '2022-09-08 01:04:08', '2022-09-08 01:04:08'),
(194, '631f6d17c8ae1', 1, 'admin', 'user', 'Ifeanyi greek Registered', 0, '2022-09-12 22:32:07', '2022-09-12 22:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `cg_password_resets`
--

CREATE TABLE `cg_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_password_resets`
--

INSERT INTO `cg_password_resets` (`email`, `token`, `created_at`) VALUES
('kj1ng@yahoo.com', '$2y$10$sigZwey.AFOY2IaxpU2Rvekt6.8jULjmNvNn4GJ4kh/1/9slThtgu', '2022-07-16 22:50:34');

-- --------------------------------------------------------

--
-- Table structure for table `cg_payment_methods`
--

CREATE TABLE `cg_payment_methods` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_payment_methods`
--

INSERT INTO `cg_payment_methods` (`id`, `uid`, `name`, `type`, `options`, `status`, `created_at`, `updated_at`) VALUES
(1, '61f18c98c61bb', 'PayPal', 'paypal', '{\"client_id\":\"AfXkbWwSG5T3dTN2n4HWEJi7AQ54X3CcVHyo0sMPcWHvKBL9ujQvmMOiivrd65PFz1a2pBFlS6xF1QE2\",\"secret\":\"ENLQxtqjzZOVGQFUosjjWUTje7JXqgU6CODBIheqEv7Mj8p7BuSTyBOlf8PW4sWaL5lZ-mitEahCqxAC\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:00', '2022-02-14 19:09:10'),
(2, '61f18c98cd21e', 'Braintree', 'braintree', '{\"merchant_id\":\"s999zxpkvhh6dpm2\",\"public_key\":\"hw4v45rty67jdxc9\",\"private_key\":\"cac14c4d9d950e32c947b400b10a7596\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:00', '2022-02-14 19:09:10'),
(3, '61f18c98dbee0', 'Stripe', 'stripe', '{\"publishable_key\":\"pk_test_AnS4Ov8GS92XmHeVCDRPIZF4\",\"secret_key\":\"sk_test_iS0xwfgzBF6cmPBBkgO13sjd\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:00', '2022-07-20 11:11:34'),
(4, '61f18c98ef73f', 'Authorize.net', 'authorize_net', '{\"login_id\":\"login_id\",\"transaction_key\":\"transaction_key\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:00', '2022-07-09 14:24:56'),
(5, '61f18c990edc1', '2checkout', '2checkout', '{\"merchant_code\":\"merchant_code\",\"private_key\":\"private_key\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-07-11 13:42:01'),
(6, '61f18c992745d', 'Pay Online (Paystack)', 'paystack', '{\"public_key\":\"pk_test_f262dfce3ae086cbb9b732dd94202d7f39c6c4f8\",\"secret_key\":\"sk_test_c7577c83f37de8b69609aa0c9f82efe5caeac69b\",\"merchant_email\":\"payments@beamtext.com\"}', 1, '2022-01-27 05:02:01', '2022-07-20 11:19:41'),
(7, '61f18c993ac71', 'PayU', 'payu', '{\"client_id\":\"client_id\",\"client_secret\":\"client_secret\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:18'),
(8, '61f18c994962c', 'Paynow', 'paynow', '{\"integration_id\":\"integration_id\",\"integration_key\":\"integration_key\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:19'),
(9, '61f18c99559d6', 'CoinPayments', 'coinpayments', '{\"merchant_id\":\"dfdffddfdfdf\"}', 0, '2022-01-27 05:02:01', '2022-02-18 10:20:32'),
(10, '61f18c995ce67', 'Instamojo', 'instamojo', '{\"api_key\":\"api_key\",\"auth_token\":\"auth_token\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:21'),
(11, '61f18c9964331', 'PayUmoney', 'payumoney', '{\"merchant_key\":\"merchant_key\",\"merchant_salt\":\"merchant_salt\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:24'),
(12, '61f18c996ba4c', 'Razorpay', 'razorpay', '{\"key_id\":\"key_id\",\"key_secret\":\"key_secret\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:30'),
(13, '61f18c9972dd6', 'SSLcommerz', 'sslcommerz', '{\"store_id\":\"store_id\",\"store_passwd\":\"store_id@ssl\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:39'),
(14, '61f18c9988d62', 'aamarPay', 'aamarpay', '{\"store_id\":\"store_id\",\"signature_key\":\"signature_key\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:40'),
(15, '61f18c999c74b', 'Flutterwave', 'flutterwave', '{\"public_key\":\"public_key\",\"secret_key\":\"secret_key\",\"environment\":\"sandbox\"}', 0, '2022-01-27 05:02:01', '2022-02-14 19:09:40'),
(16, '61f18c99afe87', 'Pay to the Bank', 'offline_payment', '{\"payment_details\":\"<p>Please make a deposit to our bank account at:<\\/p>\\r\\n<h6>Zenith Bank<\\/h6>\\r\\n<p>Account number: 1215116599<\\/p>\\r\\n<p>Account name: EarnProfit Limited<\\/p>\",\"payment_confirmation\":\"After payment please contact with following email address info@beamtext.com with your transaction id. Normally it may take 1 - 2 business days to process. Should you have any question, feel free contact with us.\"}', 1, '2022-01-27 05:02:01', '2022-07-20 11:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `cg_permissions`
--

CREATE TABLE `cg_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_permissions`
--

INSERT INTO `cg_permissions` (`id`, `uid`, `role_id`, `name`, `created_at`, `updated_at`) VALUES
(1, '61f18c93a1aa2', 1, 'access backend', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(2, '61f18c93a890b', 1, 'view customer', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(3, '61f18c93b735a', 1, 'create customer', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(4, '61f18c93c376b', 1, 'edit customer', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(5, '61f18c93cac4c', 1, 'delete customer', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(6, '61f18c93d9622', 1, 'view subscription', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(7, '61f18c93e80a6', 1, 'new subscription', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(8, '61f18c93ef550', 1, 'manage subscription', '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(9, '61f18c94028bb', 1, 'delete subscription', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(10, '61f18c9411314', 1, 'manage plans', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(11, '61f18c9424bee', 1, 'create plans', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(12, '61f18c943ab37', 1, 'edit plans', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(13, '61f18c9450b45', 1, 'delete plans', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(14, '61f18c94643db', 1, 'manage currencies', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(15, '61f18c9477ba2', 1, 'create currencies', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(16, '61f18c948b471', 1, 'edit currencies', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(17, '61f18c949c61f', 1, 'delete currencies', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(18, '61f18c94ad711', 1, 'view sending_servers', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(19, '61f18c94bc272', 1, 'create sending_servers', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(20, '61f18c94cfa7a', 1, 'edit sending_servers', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(21, '61f18c94de497', 1, 'delete sending_servers', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(22, '61f18c94e59a9', 1, 'view keywords', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(23, '61f18c94ecf22', 1, 'create keywords', '2022-01-27 05:01:56', '2022-01-27 05:01:56'),
(24, '61f18c95001cf', 1, 'edit keywords', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(25, '61f18c9507701', 1, 'delete keywords', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(26, '61f18c950ecde', 1, 'view sender_id', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(27, '61f18c951d7a4', 1, 'create sender_id', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(28, '61f18c952c209', 1, 'edit sender_id', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(29, '61f18c9533651', 1, 'delete sender_id', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(30, '61f18c953ab56', 1, 'view blacklist', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(31, '61f18c9541fc5', 1, 'create blacklist', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(32, '61f18c95495cf', 1, 'edit blacklist', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(33, '61f18c955cf45', 1, 'delete blacklist', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(34, '61f18c956dfdf', 1, 'view spam_word', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(35, '61f18c958186f', 1, 'create spam_word', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(36, '61f18c9590269', 1, 'edit spam_word', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(37, '61f18c95a3c0d', 1, 'delete spam_word', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(38, '61f18c95ab112', 1, 'view administrator', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(39, '61f18c95c0ff8', 1, 'create administrator', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(40, '61f18c95d4936', 1, 'edit administrator', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(41, '61f18c95dbd34', 1, 'delete administrator', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(42, '61f18c95e31a0', 1, 'view roles', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(43, '61f18c95ea6d0', 1, 'create roles', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(44, '61f18c95f1cf1', 1, 'edit roles', '2022-01-27 05:01:57', '2022-01-27 05:01:57'),
(45, '61f18c9604fd7', 1, 'delete roles', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(46, '61f18c960c561', 1, 'general settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(47, '61f18c9613a88', 1, 'system_email settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(48, '61f18c961af74', 1, 'authentication settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(49, '61f18c96225f7', 1, 'notifications settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(50, '61f18c96299b6', 1, 'localization settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(51, '61f18c9631070', 1, 'pusher settings', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(52, '61f18c9638563', 1, 'view languages', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(53, '61f18c9646e61', 1, 'new languages', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(54, '61f18c964e384', 1, 'manage languages', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(55, '61f18c965d0a8', 1, 'delete languages', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(56, '61f18c966bc6c', 1, 'view payment_gateways', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(57, '61f18c969049c', 1, 'update payment_gateways', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(58, '61f18c96b4c8e', 1, 'view email_templates', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(59, '61f18c96d6f5e', 1, 'update email_templates', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(60, '61f18c96e59cb', 1, 'view background_jobs', '2022-01-27 05:01:58', '2022-01-27 05:01:58'),
(61, '61f18c9700208', 1, 'view purchase_code', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(62, '61f18c970ecab', 1, 'manage update_application', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(63, '61f18c9729a0e', 1, 'manage maintenance_mode', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(64, '61f18c974bcb7', 1, 'view invoices', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(65, '61f18c976e0d7', 1, 'create invoices', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(66, '61f18c9775409', 1, 'edit invoices', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(67, '61f18c977c9a5', 1, 'delete invoices', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(68, '61f18c978406a', 1, 'view sms_history', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(69, '61f18c978b44a', 1, 'view block_message', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(70, '61f18c9792aec', 1, 'manage coverage_rates', '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(71, '620e2d9a775dc', 2, 'view customer', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(72, '620e2d9a89d05', 2, 'create customer', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(73, '620e2d9a9aeac', 2, 'edit customer', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(74, '620e2d9aae762', 2, 'delete customer', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(75, '620e2d9ac2046', 2, 'view subscription', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(76, '620e2d9ad31c7', 2, 'new subscription', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(77, '620e2d9ae42d5', 2, 'manage subscription', '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(78, '620e2d9b05f9e', 2, 'delete subscription', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(79, '620e2d9b197f9', 2, 'manage plans', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(80, '620e2d9b2a9e0', 2, 'create plans', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(81, '620e2d9b3bb1e', 2, 'edit plans', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(82, '620e2d9b4cc4f', 2, 'delete plans', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(83, '620e2d9b6049c', 2, 'manage currencies', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(84, '620e2d9b71646', 2, 'create currencies', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(85, '620e2d9b82792', 2, 'edit currencies', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(86, '620e2d9b93903', 2, 'delete currencies', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(87, '620e2d9ba4ac3', 2, 'view sending_servers', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(88, '620e2d9bb5c65', 2, 'create sending_servers', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(89, '620e2d9bc6d4e', 2, 'edit sending_servers', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(90, '620e2d9bd8072', 2, 'delete sending_servers', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(91, '620e2d9be9211', 2, 'view keywords', '2022-02-17 16:12:27', '2022-02-17 16:12:27'),
(92, '620e2d9c0600d', 2, 'create keywords', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(93, '620e2d9c1bf78', 2, 'edit keywords', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(94, '620e2d9c31fcd', 2, 'delete keywords', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(95, '620e2d9c43049', 2, 'view tags', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(96, '620e2d9c543a8', 2, 'create tags', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(97, '620e2d9c6a193', 2, 'edit tags', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(98, '620e2d9c80127', 2, 'delete tags', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(99, '620e2d9c91292', 2, 'view sender_id', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(100, '620e2d9ca720f', 2, 'create sender_id', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(101, '620e2d9cbd172', 2, 'edit sender_id', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(102, '620e2d9cc6dba', 2, 'delete sender_id', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(103, '620e2d9cd5a3e', 2, 'view phone_numbers', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(104, '620e2d9ceb8a7', 2, 'create phone_numbers', '2022-02-17 16:12:28', '2022-02-17 16:12:28'),
(105, '620e2d9d0d59f', 2, 'edit phone_numbers', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(106, '620e2d9d234ba', 2, 'delete phone_numbers', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(107, '620e2d9d2d246', 2, 'view blacklist', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(108, '620e2d9d4312f', 2, 'create blacklist', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(109, '620e2d9d59116', 2, 'edit blacklist', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(110, '620e2d9d6a2f1', 2, 'delete blacklist', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(111, '620e2d9d78d57', 2, 'view spam_word', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(112, '620e2d9d87730', 2, 'create spam_word', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(113, '620e2d9d9d6bf', 2, 'edit spam_word', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(114, '620e2d9da7305', 2, 'delete spam_word', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(115, '620e2d9db5ce0', 2, 'view administrator', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(116, '620e2d9dcbd8e', 2, 'create administrator', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(117, '620e2d9de1cb9', 2, 'edit administrator', '2022-02-17 16:12:29', '2022-02-17 16:12:29'),
(118, '620e2d9e03a55', 2, 'delete administrator', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(119, '620e2d9e14b7a', 2, 'view roles', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(120, '620e2d9e2ab31', 2, 'create roles', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(121, '620e2d9e3bbaf', 2, 'edit roles', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(122, '620e2d9e51d5c', 2, 'delete roles', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(123, '620e2d9e67be2', 2, 'view languages', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(124, '620e2d9e71741', 2, 'new languages', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(125, '620e2d9e801b3', 2, 'manage languages', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(126, '620e2d9e91393', 2, 'delete languages', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(127, '620e2d9e9fdab', 2, 'general settings', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(128, '620e2d9eae7c4', 2, 'system_email settings', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(129, '620e2d9ebd142', 2, 'authentication settings', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(130, '620e2d9ecbca2', 2, 'notifications settings', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(131, '620e2d9eda727', 2, 'localization settings', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(132, '620e2d9ef0749', 2, 'view payment_gateways', '2022-02-17 16:12:30', '2022-02-17 16:12:30'),
(133, '620e2d9f0d685', 2, 'update payment_gateways', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(134, '620e2d9f14b1d', 2, 'view email_templates', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(135, '620e2d9f234a1', 2, 'update email_templates', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(136, '620e2d9f34656', 2, 'view sms_history', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(137, '620e2d9f43107', 2, 'view invoices', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(138, '620e2d9f54422', 2, 'access backend', '2022-02-17 16:12:31', '2022-02-17 16:12:31'),
(139, '6224bbc62f080', 3, 'view customer', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(140, '6224bbc62f9f6', 3, 'edit customer', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(141, '6224bbc6301e4', 3, 'view subscription', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(142, '6224bbc630d2d', 3, 'manage plans', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(143, '6224bbc631844', 3, 'manage currencies', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(144, '6224bbc631f50', 3, 'view keywords', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(145, '6224bbc632776', 3, 'view tags', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(146, '6224bbc63302e', 3, 'view sender_id', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(147, '6224bbc633b90', 3, 'view phone_numbers', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(148, '6224bbc635a91', 3, 'view blacklist', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(149, '6224bbc63678c', 3, 'view spam_word', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(150, '6224bbc637479', 3, 'view administrator', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(151, '6224bbc638960', 3, 'view roles', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(152, '6224bbc639120', 3, 'view languages', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(153, '6224bbc6397f4', 3, 'view payment_gateways', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(154, '6224bbc639ed3', 3, 'view email_templates', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(155, '6224bbc63a7c3', 3, 'view sms_history', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(156, '6224bbc63b4f1', 3, 'view invoices', '2022-03-06 18:48:54', '2022-03-06 18:48:54'),
(157, '6224bbc63bfdc', 3, 'access backend', '2022-03-06 18:48:54', '2022-03-06 18:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `cg_personal_access_tokens`
--

CREATE TABLE `cg_personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_personal_access_tokens`
--

INSERT INTO `cg_personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'user', 1, 'akasham67@gmail.com', '7228e48d523d8d947a88b7308fe99be12ba94c6654da486e559441b7e34cd9a7', '[\"*\"]', NULL, '2022-01-27 05:01:59', '2022-01-27 05:01:59'),
(2, 'user', 3, 'client@gmail.com', '3dc86f69351ed226c9d42e6395558e9ed0d6e3aecd2f1522b75a16d395bc8f37', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-02-18 16:46:11', '2022-02-18 16:46:11'),
(3, 'user', 4, 'karmahanson990@1secmail.org', '102e20afdb89e4edfa8cd7bbc3bb3eb2ac4415efa23b86e485cf92f21148a01f', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-03-11 05:06:02', '2022-03-11 05:06:02'),
(4, 'user', 5, 'ajalaofficial@gmail.com', '9a6c8f764e2fa671c87f4725040f24d57e1b31e878323ec2621e83e13c9cde27', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-03-28 11:39:04', '2022-03-28 11:39:04'),
(5, 'user', 6, 'joeolalekan2@gmail.com', '6f65d716c1403e19edc8d17931dcddbd0c707c5e29c18be1081f003a17eafc65', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-03-28 11:40:38', '2022-03-28 11:40:38'),
(6, 'user', 7, 'rosalyncuper9377@1secmail.org', '61542852e7c697246152e546e178767b3b2803176fa1e3aa9513ceb41078985e', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-03-31 08:34:56', '2022-03-31 08:34:56'),
(7, 'user', 8, 'cindyagnes@melverly.com', '2bc7e8d027bbaff977861eef860ee6e6bd0d0773fba6f701a6a9334c2d1b1cdc', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-03-31 14:41:53', '2022-03-31 14:41:53'),
(8, 'user', 9, 'berrygaffney@hidebox.org', '2a4ae27b9100e0a4f152f6a2f19c717bbe7cb6e4d672964880223be426d3f4dd', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-04-03 03:08:29', '2022-04-03 03:08:29'),
(9, 'user', 10, 'kj1ngg@gmail.com', '579935b50cce006e358ea4b20d9f8a9dbbc419517ed49a25a36ea618937406a5', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-04-12 22:31:48', '2022-04-12 22:31:48'),
(10, 'user', 11, 'kj1ng@yahoo.com', '396b940f98e7d09aed2a59585350836408f366fe73c04d0f2b4dae5b6215a094', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-04-24 19:32:22', '2022-04-24 19:32:22'),
(11, 'user', 12, 'Tundex77@gmail.com', '3e3ac2ab721c9ea3a837297920f7177b54db54e21266d947643165e3de44b472', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-05-05 17:02:24', '2022-05-05 17:02:24'),
(12, 'user', 13, 'adriannafinniss@hidebox.org', '758e526a27d3bdb256ff8a28e9b46cb81b8e302bc65964ff04c8a6e8c77d6eb3', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-05-26 21:40:35', '2022-05-26 21:40:35'),
(13, 'user', 14, 'boydconey@1secmail.org', 'c68721366561bbbdab36e626a958f2a1e8b26d0ab86a405a25abc48e5e97a673', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-06-05 04:35:02', '2022-06-05 04:35:02'),
(14, 'user', 15, 'naturabright2000@gmail.con', '4e6e3b1dcda38ceed13d41ff8a6cac5e09ff3c7eafde10b888b96cc2bad4d3b3', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-06-06 11:09:24', '2022-06-06 11:09:24'),
(15, 'user', 16, 'kj1ng@yahoo.com', 'cc96d4923f071544ed042086beed36d01870946720584ad489a1f906276401fc', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-07-16 22:32:42', '2022-07-16 22:32:42'),
(16, 'user', 17, 'kj1ngg@gmail.com', '11fef5cc87086324f4170d9ed7c89673355dd09ca8aea9025ad3067a6785297a', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-07-19 23:01:59', '2022-07-19 23:01:59'),
(17, 'user', 19, 'comcityads@gmail.com', '682c609840dbfec61b3feb8c0d98f761ca2bc19297f2b6c6fba2842d4d668dd5', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-07-28 14:19:21', '2022-07-28 14:19:21'),
(18, 'user', 20, 'tundex77@gmail.com', 'd586d5e9ca30d9ccc60f4f5ffb6330cabc3b6a793ee273ed813172bd2ce4e595', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-08-03 00:14:42', '2022-08-03 00:14:42'),
(19, 'user', 21, 'testhachi123@gmail.com', 'f960dcdc70578284e2aa766a7e93cb4a24ff9390de5b802d8cf8e0f496c4f986', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-08-03 11:19:12', '2022-08-03 11:19:12'),
(20, 'user', 22, 'juanitamcgoldrick@hidebox.org', '2d760942f20fb0afe30574d8ad6630ace4296d431d272863399f19851085683c', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-08-05 13:41:46', '2022-08-05 13:41:46'),
(21, 'user', 23, 'testhachi123@gmail.com', '1d4b1ddf5821c98ee278bda1413325e8b2651169d9ccffd33473bd2eee711202', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-08-06 14:49:40', '2022-08-06 14:49:40'),
(22, 'user', 24, 'brandihefner@awer.blastzane.com', '50fe3172e1e9088ee3a128a5237cdefbb6a66c25141888f58a7fe7b00b355d81', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-08-30 02:59:08', '2022-08-30 02:59:08'),
(23, 'user', 25, 'ajalaofficial@gmail.com', '36661d7af3a35988a67141077b2581762cdec2fc255bbfa346182c15fbc6c789', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-09-08 00:20:34', '2022-09-08 00:20:34'),
(24, 'user', 27, 'playsafe272@gmail.com', 'a69561bd4f555aa2d60d8bf11a60bc5a4eeeb079a4f3c5af96bdc57c133033ac', '[\"access_backend\",\"view_reports\",\"view_contact_group\",\"create_contact_group\",\"update_contact_group\",\"delete_contact_group\",\"view_contact\",\"create_contact\",\"update_contact\",\"delete_contact\",\"view_sender_id\",\"create_sender_id\",\"view_blacklist\",\"create_blacklist\",\"delete_blacklist\",\"sms_campaign_builder\",\"sms_quick_send\",\"sms_bulk_messages\",\"sms_template\",\"developers\"]', NULL, '2022-09-12 22:32:07', '2022-09-12 22:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `cg_phone_numbers`
--

CREATE TABLE `cg_phone_numbers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `currency_id` bigint(20) UNSIGNED DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('available','assigned','expired') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'available',
  `capabilities` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `billing_cycle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frequency_amount` int(11) NOT NULL,
  `frequency_unit` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validity_date` date DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_phone_numbers`
--

INSERT INTO `cg_phone_numbers` (`id`, `uid`, `user_id`, `currency_id`, `number`, `status`, `capabilities`, `price`, `billing_cycle`, `frequency_amount`, `frequency_unit`, `validity_date`, `transaction_id`, `created_at`, `updated_at`) VALUES
(1, '6205325cb8f59', 1, 1, '18339193983', 'assigned', '\"[\\\"sms\\\"]\"', '0', 'monthly', 1, 'month', NULL, NULL, '2022-02-10 20:42:20', '2022-02-21 19:40:49'),
(3, '62100064ef7e2', 1, 1, '448000119833', 'assigned', '\"[\\\"sms\\\",\\\"voice\\\"]\"', '0', 'monthly', 1, 'month', NULL, NULL, '2022-02-18 19:24:04', '2022-02-18 19:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `cg_plans`
--

CREATE TABLE `cg_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `currency_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  `billing_cycle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_amount` int(11) NOT NULL DEFAULT 0,
  `frequency_unit` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `custom_order` int(11) NOT NULL DEFAULT 0,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `is_popular` tinyint(1) NOT NULL DEFAULT 0,
  `tax_billing_required` tinyint(1) NOT NULL DEFAULT 0,
  `plans_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=>"susbscription", 1="buy now"',
  `initial_unit` decimal(16,2) NOT NULL DEFAULT 0.00,
  `final_unit` decimal(16,2) NOT NULL DEFAULT 0.00,
  `max_amount` decimal(16,2) NOT NULL DEFAULT 0.00,
  `minimum_amount` decimal(16,2) NOT NULL DEFAULT 0.00,
  `unitpersms` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_unit1` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_plans`
--

INSERT INTO `cg_plans` (`id`, `uid`, `user_id`, `currency_id`, `name`, `description`, `price`, `billing_cycle`, `frequency_amount`, `frequency_unit`, `options`, `status`, `custom_order`, `is_default`, `is_popular`, `tax_billing_required`, `plans_type`, `initial_unit`, `final_unit`, `max_amount`, `minimum_amount`, `unitpersms`, `frequency_unit1`, `created_at`, `updated_at`) VALUES
(1, '620531c03ff11', 1, 11, 'Basic', NULL, '13500.00', 'custom', 10, 'year', '{\"sms_max\":\"5000\",\"whatsapp_max\":\"100\",\"list_max\":\"-1\",\"subscriber_max\":\"-1\",\"subscriber_per_list_max\":\"-1\",\"segment_per_list_max\":\"3\",\"billing_cycle\":\"monthly\",\"sending_limit\":\"1000_per_hour\",\"sending_quota\":\"1000\",\"sending_quota_time\":\"1\",\"sending_quota_time_unit\":\"hour\",\"max_process\":\"1\",\"unsubscribe_url_required\":\"no\",\"create_sending_server\":\"yes\",\"sending_servers_max\":\"5\",\"list_import\":\"yes\",\"list_export\":\"yes\",\"api_access\":\"yes\",\"create_sub_account\":\"no\",\"delete_sms_history\":\"no\",\"add_previous_balance\":\"no\",\"sender_id_verification\":\"yes\",\"send_spam_message\":\"yes\",\"cutting_system\":\"no\",\"cutting_value\":\"0\",\"cutting_unit\":\"percentage\",\"cutting_logic\":\"random\",\"plain_sms\":\"1\",\"receive_plain_sms\":\"0\",\"voice_sms\":\"2\",\"receive_voice_sms\":\"0\",\"mms_sms\":\"3\",\"receive_mms_sms\":\"0\",\"whatsapp_sms\":\"1\",\"receive_whatsapp_sms\":\"0\",\"per_unit_price\":\"2.70\"}', 1, 5, 0, 1, 0, 0, '0.00', '0.00', '0.00', '0.00', '2.70', NULL, '2022-02-10 20:39:44', '2022-08-02 16:40:51'),
(2, '620e4d8f56423', 1, 11, 'Standard', NULL, '25000.00', 'custom', 10, 'year', '{\"sms_max\":\"10000\",\"whatsapp_max\":\"100\",\"list_max\":\"-1\",\"subscriber_max\":\"-1\",\"subscriber_per_list_max\":\"-1\",\"segment_per_list_max\":\"3\",\"billing_cycle\":\"monthly\",\"sending_limit\":\"1000_per_hour\",\"sending_quota\":\"1000\",\"sending_quota_time\":\"1\",\"sending_quota_time_unit\":\"hour\",\"max_process\":\"1\",\"unsubscribe_url_required\":\"no\",\"create_sending_server\":\"yes\",\"sending_servers_max\":\"5\",\"list_import\":\"yes\",\"list_export\":\"yes\",\"api_access\":\"yes\",\"create_sub_account\":\"no\",\"delete_sms_history\":\"no\",\"add_previous_balance\":\"no\",\"sender_id_verification\":\"no\",\"send_spam_message\":\"yes\",\"cutting_system\":\"no\",\"cutting_value\":\"0\",\"cutting_unit\":\"percentage\",\"cutting_logic\":\"random\",\"plain_sms\":\"1\",\"receive_plain_sms\":\"0\",\"voice_sms\":\"2\",\"receive_voice_sms\":\"0\",\"mms_sms\":\"3\",\"receive_mms_sms\":\"0\",\"whatsapp_sms\":\"1\",\"receive_whatsapp_sms\":\"0\",\"per_unit_price\":\"2.50\"}', 1, 4, 0, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', '2.50', NULL, '2022-02-17 18:28:47', '2022-08-02 16:40:33'),
(3, '62655c786f364', 1, 11, 'Pro', NULL, '120000.00', 'custom', 10, 'year', '{\"sms_max\":\"50000\",\"whatsapp_max\":\"100\",\"list_max\":\"-1\",\"subscriber_max\":\"-1\",\"subscriber_per_list_max\":\"-1\",\"segment_per_list_max\":\"3\",\"billing_cycle\":\"monthly\",\"sending_limit\":\"1000_per_hour\",\"sending_quota\":\"1000\",\"sending_quota_time\":\"1\",\"sending_quota_time_unit\":\"hour\",\"max_process\":\"1\",\"unsubscribe_url_required\":\"no\",\"create_sending_server\":\"yes\",\"sending_servers_max\":\"5\",\"list_import\":\"yes\",\"list_export\":\"yes\",\"api_access\":\"yes\",\"create_sub_account\":\"no\",\"delete_sms_history\":\"no\",\"add_previous_balance\":\"no\",\"sender_id_verification\":\"no\",\"send_spam_message\":\"yes\",\"cutting_system\":\"no\",\"cutting_value\":\"0\",\"cutting_unit\":\"percentage\",\"cutting_logic\":\"random\",\"plain_sms\":\"1\",\"receive_plain_sms\":\"0\",\"voice_sms\":\"2\",\"receive_voice_sms\":\"0\",\"mms_sms\":\"3\",\"receive_mms_sms\":\"0\",\"whatsapp_sms\":\"1\",\"receive_whatsapp_sms\":\"0\",\"per_unit_price\":\"2.40\"}', 1, 3, 0, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', '2.40', NULL, '2022-04-24 18:19:36', '2022-08-02 16:40:16'),
(10, '62cbe4654acf0', 1, 11, 'Test', NULL, '270.00', 'custom', 10, 'year', '{\"sms_max\":\"100\",\"whatsapp_max\":\"100\",\"list_max\":\"-1\",\"subscriber_max\":\"-1\",\"subscriber_per_list_max\":\"-1\",\"segment_per_list_max\":\"3\",\"billing_cycle\":\"monthly\",\"sending_limit\":\"1000_per_hour\",\"sending_quota\":\"1000\",\"sending_quota_time\":\"1\",\"sending_quota_time_unit\":\"hour\",\"max_process\":\"1\",\"unsubscribe_url_required\":\"no\",\"create_sending_server\":\"no\",\"sending_servers_max\":\"5\",\"list_import\":\"yes\",\"list_export\":\"yes\",\"api_access\":\"no\",\"create_sub_account\":\"yes\",\"delete_sms_history\":\"yes\",\"add_previous_balance\":\"no\",\"sender_id_verification\":\"yes\",\"send_spam_message\":\"no\",\"cutting_system\":\"no\",\"cutting_value\":\"0\",\"cutting_unit\":\"percentage\",\"cutting_logic\":\"random\",\"plain_sms\":\"1\",\"receive_plain_sms\":\"0\",\"voice_sms\":\"2\",\"receive_voice_sms\":\"0\",\"mms_sms\":\"3\",\"receive_mms_sms\":\"0\",\"whatsapp_sms\":\"1\",\"receive_whatsapp_sms\":\"0\",\"per_unit_price\":\"2.70\"}', 1, 1, 0, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', '2.70', NULL, '2022-07-11 12:50:45', '2022-08-02 16:39:54'),
(11, '62e4f90f2b8b3', 1, 11, 'Test 2', NULL, '2.70', 'custom', 10, 'year', '{\"sms_max\":\"100\",\"whatsapp_max\":\"100\",\"list_max\":\"-1\",\"subscriber_max\":\"-1\",\"subscriber_per_list_max\":\"-1\",\"segment_per_list_max\":\"3\",\"billing_cycle\":\"monthly\",\"sending_limit\":\"1000_per_hour\",\"sending_quota\":\"1000\",\"sending_quota_time\":\"1\",\"sending_quota_time_unit\":\"hour\",\"max_process\":\"1\",\"unsubscribe_url_required\":\"no\",\"create_sending_server\":\"no\",\"sending_servers_max\":\"5\",\"list_import\":\"yes\",\"list_export\":\"yes\",\"api_access\":\"no\",\"create_sub_account\":\"yes\",\"delete_sms_history\":\"yes\",\"add_previous_balance\":\"no\",\"sender_id_verification\":\"yes\",\"send_spam_message\":\"no\",\"cutting_system\":\"no\",\"cutting_value\":\"0\",\"cutting_unit\":\"percentage\",\"cutting_logic\":\"random\",\"plain_sms\":\"1\",\"receive_plain_sms\":\"0\",\"voice_sms\":\"2\",\"receive_voice_sms\":\"0\",\"mms_sms\":\"3\",\"receive_mms_sms\":\"0\",\"whatsapp_sms\":\"1\",\"receive_whatsapp_sms\":\"0\",\"per_unit_price\":\".3\"}', 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', NULL, NULL, '2022-07-30 13:25:35', '2022-08-02 16:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `cg_plans_coverage_countries`
--

CREATE TABLE `cg_plans_coverage_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_plans_sending_servers`
--

CREATE TABLE `cg_plans_sending_servers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sending_server_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `fitness` int(11) NOT NULL,
  `is_primary` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_plans_sending_servers`
--

INSERT INTO `cg_plans_sending_servers` (`id`, `sending_server_id`, `plan_id`, `fitness`, `is_primary`, `created_at`, `updated_at`) VALUES
(15, 9, 2, 100, 1, NULL, NULL),
(16, 9, 1, 100, 1, NULL, NULL),
(17, 9, 3, 100, 1, NULL, NULL),
(20, 9, 10, 100, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cg_reports`
--

CREATE TABLE `cg_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_url` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_by` enum('from','to','api') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sending_server_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_reports`
--

INSERT INTO `cg_reports` (`id`, `uid`, `user_id`, `campaign_id`, `from`, `to`, `message`, `media_url`, `sms_type`, `status`, `send_by`, `cost`, `api_key`, `sending_server_id`, `created_at`, `updated_at`) VALUES
(138, '62df833b98499', 3, 148, 'AB-HACWEB', '+2347031476524', 'grth', NULL, 'plain', 'Delivered|b74c1dd3-f38c-4fa1-a18a-0d49d3a90d99', 'from', '1', NULL, 9, '2022-07-26 10:01:31', '2022-07-26 10:01:31'),
(139, '62df833c47fd3', 3, 148, 'AB-HACWEB', '+918837842284', 'grth', NULL, 'plain', 'REJECTED|1658815291', 'from', '1', NULL, 7, '2022-07-26 10:01:32', '2022-07-26 10:01:32'),
(140, '62df893678886', 3, 150, 'AB-HACWEB', 'bgn', 'dgvrf', NULL, 'plain', 'to address is not numeric', 'from', '1', NULL, 2, '2022-07-26 10:27:02', '2022-07-26 10:27:02'),
(141, '62df89377ad8c', 3, 150, 'AB-HACWEB', '+918837842284', 'dgvrf', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 10:27:03', '2022-07-26 10:27:03'),
(142, '62df904250d60', 3, 159, 'AB-HACWEB', 'fdhh', '+2348127139986', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 10:57:06', '2022-07-26 10:57:06'),
(143, '62df904352d77', 3, 159, 'AB-HACWEB', '+918837842284', '+2348127139986', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 10:57:07', '2022-07-26 10:57:07'),
(144, '62df961cadc4f', 1, 163, 'BeamTEXT', '+2348127139986', 'hiii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:22:04', '2022-07-26 11:22:04'),
(145, '62df961daf930', 1, 163, 'BeamTEXT', '+2348127139986', 'hiii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:22:05', '2022-07-26 11:22:05'),
(146, '62df961eb1c79', 1, 163, 'BeamTEXT', '+2347031476524', 'hiii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:22:06', '2022-07-26 11:22:06'),
(147, '62df97729eea8', 1, 164, 'BeamTEXT', '+2348127139986', 'Hi  your available balance is ', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:27:46', '2022-07-26 11:27:46'),
(148, '62df9773a0e8e', 1, 164, 'BeamTEXT', '+2348127139986', 'Hi Beamtext1 your available balance is ', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:27:47', '2022-07-26 11:27:47'),
(149, '62df9774a315b', 1, 164, 'BeamTEXT', '+2347031476524', 'Hi Beamtext2 your available balance is ', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 11:27:48', '2022-07-26 11:27:48'),
(150, '62df980b0f4f4', 3, 166, 'BeamTEXT', '+2348127139986', 'hii yooo', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:30:19', '2022-07-26 11:30:19'),
(151, '62df980c1421a', 3, 166, 'BeamTEXT', '+918837842284', 'hii yooo', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:30:20', '2022-07-26 11:30:20'),
(152, '62df990fe1c16', 3, 170, 'AB-HACWEB', 'hghg', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:34:39', '2022-07-26 11:34:39'),
(153, '62df9910e40ce', 3, 170, 'AB-HACWEB', '+918837842284', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:34:40', '2022-07-26 11:34:40'),
(154, '62df9955d5d3d', 3, 171, 'AB-HACWEB', ':+2348127139986', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:35:49', '2022-07-26 11:35:49'),
(155, '62df9956d7dfe', 3, 171, 'AB-HACWEB', '+918837842284', 'Prateekhiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 11:35:50', '2022-07-26 11:35:50'),
(156, '62dfa41f88e66', 1, 181, 'BeamTEXT', '+2348127139986', 'fgrtf', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 12:21:51', '2022-07-26 12:21:51'),
(157, '62dfa4208beb8', 1, 181, 'BeamTEXT', '+2348127139986', 'fgrtf', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 12:21:52', '2022-07-26 12:21:52'),
(158, '62dfa4218e4b6', 1, 181, 'BeamTEXT', '+2347031476524', 'fgrtf', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 12:21:53', '2022-07-26 12:21:53'),
(159, '62dfac8d3d8a9', 3, 188, 'AB-HACWEB', '+2348127139986', 'fg', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 12:57:49', '2022-07-26 12:57:49'),
(160, '62dfac8e400c9', 3, 188, 'AB-HACWEB', '+918837842284', 'fg', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 12:57:50', '2022-07-26 12:57:50'),
(161, '62dfae2638181', 3, 192, 'AB-HACWEB', 'hiii', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 13:04:38', '2022-07-26 13:04:38'),
(162, '62dfae273b1e9', 3, 192, 'AB-HACWEB', '+918837842284', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 13:04:39', '2022-07-26 13:04:39'),
(163, '62dfae9b41ae4', 3, 193, 'BeamTEXT', '+2348127139986', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 13:06:35', '2022-07-26 13:06:35'),
(164, '62dfae9c43757', 3, 193, 'BeamTEXT', '+918837842284', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 1, '2022-07-26 13:06:36', '2022-07-26 13:06:36'),
(165, '62dfc7ba01bae', 3, 195, 'BeamTEXT', '+2348127139986', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 14:53:46', '2022-07-26 14:53:46'),
(166, '62dfc7bb0754b', 3, 195, 'BeamTEXT', '+918837842284', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 14:53:47', '2022-07-26 14:53:47'),
(167, '62dfc850d5d16', 1, 196, 'BeamTEXT', '+2348127139986', 'hello', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:56:16', '2022-07-26 14:56:16'),
(168, '62dfc851d8591', 1, 196, 'BeamTEXT', '+2348127139986', 'hello', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:56:17', '2022-07-26 14:56:17'),
(169, '62dfc852da195', 1, 196, 'BeamTEXT', '+2347031476524', 'hello', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:56:18', '2022-07-26 14:56:18'),
(170, '62dfc8aef412f', 1, 197, 'BeamTEXT', '+919923417354', 'hiiillo', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:57:51', '2022-07-26 14:57:51'),
(171, '62dfc8b003617', 1, 197, 'BeamTEXT', '+2348127139986', 'hiiillo', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:57:52', '2022-07-26 14:57:52'),
(172, '62dfc8b1065aa', 1, 197, 'BeamTEXT', '+2348127139986', 'hiiillo', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:57:53', '2022-07-26 14:57:53'),
(173, '62dfc8b2088c4', 1, 197, 'BeamTEXT', '+2347031476524', 'hiiillo', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 14:57:54', '2022-07-26 14:57:54'),
(174, '62dfc9493050f', 3, 198, 'BeamTEXT', '+918837842284', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:25', '2022-07-26 15:00:25'),
(175, '62dfc94a35042', 3, 198, 'BeamTEXT', 'harsh_shukla', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:26', '2022-07-26 15:00:26'),
(176, '62dfc94b3f820', 3, 198, 'BeamTEXT', '+18009009090', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:27', '2022-07-26 15:00:27'),
(177, '62dfc94c42aee', 3, 198, 'BeamTEXT', '+2348127139986', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:28', '2022-07-26 15:00:28'),
(178, '62dfc94d45634', 3, 198, 'BeamTEXT', '+18009009091', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:29', '2022-07-26 15:00:29'),
(179, '62dfc94e4bb92', 3, 198, 'BeamTEXT', '+18009009090', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:30', '2022-07-26 15:00:30'),
(180, '62dfc94f4ea6d', 3, 198, 'BeamTEXT', '+18009009091', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:31', '2022-07-26 15:00:31'),
(181, '62dfc95050930', 3, 198, 'BeamTEXT', '+18009009090', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:32', '2022-07-26 15:00:32'),
(182, '62dfc95156771', 3, 198, 'BeamTEXT', '+18009009091', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:33', '2022-07-26 15:00:33'),
(183, '62dfc952591e5', 3, 198, 'BeamTEXT', '+18009009090', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:34', '2022-07-26 15:00:34'),
(184, '62dfc9535ad8c', 3, 198, 'BeamTEXT', '+18009009091', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:35', '2022-07-26 15:00:35'),
(185, '62dfc9545f09a', 3, 198, 'BeamTEXT', '566343434343', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:36', '2022-07-26 15:00:36'),
(186, '62dfc95563678', 3, 198, 'BeamTEXT', '+18009009090', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:37', '2022-07-26 15:00:37'),
(187, '62dfc95665171', 3, 198, 'BeamTEXT', '+18009009091', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:00:38', '2022-07-26 15:00:38'),
(188, '62dfcacad51b1', 3, 199, 'AB-HACWEB', 'hello', 'hii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:06:50', '2022-07-26 15:06:50'),
(189, '62dfcc5f00bdb', 3, 200, 'BeamTEXT', '+2348127139986', 'hhii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:13:35', '2022-07-26 15:13:35'),
(190, '62dfcc6003e3b', 3, 200, 'BeamTEXT', '+918837842284', 'hhii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:13:36', '2022-07-26 15:13:36'),
(191, '62dfcc9021550', 3, 201, 'BeamTEXT', 'harsh_shukla', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:24', '2022-07-26 15:14:24'),
(192, '62dfcc9126d51', 3, 201, 'BeamTEXT', '+2347031476524', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:25', '2022-07-26 15:14:25'),
(193, '62dfcc9229053', 3, 201, 'BeamTEXT', '+18009009090', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:26', '2022-07-26 15:14:26'),
(194, '62dfcc932c7c4', 3, 201, 'BeamTEXT', '+18009009091', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:27', '2022-07-26 15:14:27'),
(195, '62dfcc94304f5', 3, 201, 'BeamTEXT', '+18009009090', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:28', '2022-07-26 15:14:28'),
(196, '62dfcc9532fe3', 3, 201, 'BeamTEXT', '+18009009091', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:29', '2022-07-26 15:14:29'),
(197, '62dfcc96353b5', 3, 201, 'BeamTEXT', '+18009009090', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:30', '2022-07-26 15:14:30'),
(198, '62dfcc97366e0', 3, 201, 'BeamTEXT', '+18009009091', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:31', '2022-07-26 15:14:31'),
(199, '62dfcc983922b', 3, 201, 'BeamTEXT', '+18009009090', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:32', '2022-07-26 15:14:32'),
(200, '62dfcc993bc32', 3, 201, 'BeamTEXT', '+18009009091', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:33', '2022-07-26 15:14:33'),
(201, '62dfcc9a3fca5', 3, 201, 'BeamTEXT', '566343434343', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:34', '2022-07-26 15:14:34'),
(202, '62dfcc9b42394', 3, 201, 'BeamTEXT', '+18009009090', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:35', '2022-07-26 15:14:35'),
(203, '62dfcc9c44e0a', 3, 201, 'BeamTEXT', '+18009009091', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:14:36', '2022-07-26 15:14:36'),
(204, '62dfccdd04d42', 3, 202, 'BeamTEXT', '+2347031476524', 'hello', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-26 15:15:41', '2022-07-26 15:15:41'),
(205, '62dfe074ae12a', 1, 203, 'BeamTEXT', '+2347031476524', 'hfgfgb', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 16:39:16', '2022-07-26 16:39:16'),
(206, '62dfe075b0c86', 1, 203, 'BeamTEXT', '+2348127139986', 'hfgfgb', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 16:39:17', '2022-07-26 16:39:17'),
(207, '62dfe076b2f63', 1, 203, 'BeamTEXT', '+2347031476524', 'hfgfgb', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-26 16:39:18', '2022-07-26 16:39:18'),
(208, '62e05f508e94d', 1, 206, 'BeamTEXT', '+2348127139986', 'testing', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 01:40:32', '2022-07-27 01:40:32'),
(209, '62e05f5191517', 1, 206, 'BeamTEXT', '+2347031476524', 'testing', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 01:40:33', '2022-07-27 01:40:33'),
(210, '62e06013638f7', 1, 207, 'BeamText', '+2347031476524', 'testing again', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 01:43:47', '2022-07-27 01:43:47'),
(211, '62e0c5be77c84', 1, 208, 'BeamTEXT', '+2347031476524', 'hii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 08:57:34', '2022-07-27 08:57:34'),
(212, '62e0c5bf7a907', 1, 208, 'BeamTEXT', '+2348127139986', 'hii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 08:57:35', '2022-07-27 08:57:35'),
(213, '62e0c5c07e368', 1, 208, 'BeamTEXT', '+2347031476524', 'hii', NULL, 'plain', 'Delivered', 'from', '0', NULL, 1, '2022-07-27 08:57:36', '2022-07-27 08:57:36'),
(218, '62e0ddc9c86f6', 3, 211, 'AB-HACWEB', '+2348127139986', 'gdff', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-27 10:40:09', '2022-07-27 10:40:09'),
(219, '62e0ddcaca7ee', 3, 211, 'AB-HACWEB', '+918837842284', 'gdff', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-27 10:40:10', '2022-07-27 10:40:10'),
(220, '62e0df92b6ca0', 3, 212, 'AB-HACWEB', '+2348127139986', 'hh', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-27 10:47:46', '2022-07-27 10:47:46'),
(221, '62e0df93bb3a5', 3, 212, 'AB-HACWEB', '+918837842284', 'hh', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-07-27 10:47:47', '2022-07-27 10:47:47'),
(222, '62e26dd980170', 19, 213, 'BeamTEXT', '2347031476524', 'This a test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-28 15:07:05', '2022-07-28 15:07:05'),
(223, '62e26dda82ede', 19, 213, 'BeamTEXT', '2348127139986', 'This a test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-28 15:07:06', '2022-07-28 15:07:06'),
(224, '62e4ef24a3fb5', 19, 214, 'BeamTEXT', '2347031476524', 'Tet message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 12:43:16', '2022-07-30 12:43:16'),
(225, '62e4ef25a5d1a', 19, 214, 'BeamTEXT', '2348127139986', 'Tet message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 12:43:17', '2022-07-30 12:43:17'),
(226, '62e4f01375592', 19, 215, 'BeamTEXT', '+2347031476524', 'Testing SMS', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 12:47:15', '2022-07-30 12:47:15'),
(227, '62e4f01477381', 19, 215, 'BeamTEXT', '+2348127139986', 'Testing SMS', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 12:47:16', '2022-07-30 12:47:16'),
(228, '62e4f01579684', 19, 215, 'BeamTEXT', '+2348056254493', 'Testing SMS', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 12:47:17', '2022-07-30 12:47:17'),
(229, '62e4fb23aadbd', 19, 216, 'BeamTEXT', '+2347031476524', 'Happy birthday to you first_name\r\n\r\nWe wish you well in all your endeavors', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 13:34:27', '2022-07-30 13:34:27'),
(230, '62e4fb24af903', 19, 216, 'BeamTEXT', '+2348127139986', 'Happy birthday to you first_name\r\n\r\nWe wish you well in all your endeavors', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 13:34:28', '2022-07-30 13:34:28'),
(231, '62e4fb25b2055', 19, 216, 'BeamTEXT', '+2348056254493', 'Happy birthday to you first_name\r\n\r\nWe wish you well in all your endeavors', NULL, 'plain', 'Delivered', 'from', '1', NULL, 16, '2022-07-30 13:34:29', '2022-07-30 13:34:29'),
(232, '62e905867bbdc', 3, 222, 'AB-HACWEB', '1234567891', 'hiii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-02 15:07:50', '2022-08-02 15:07:50'),
(233, '62e98c8179ec7', 20, 223, 'BeamTEXT', '+2349058728830', 'Happy Birthday Tunde  \r\nWe wish you the best as you add one more year to your many years', NULL, 'plain', 'Delivered', 'from', '1', NULL, 17, '2022-08-03 00:43:45', '2022-08-03 00:43:45'),
(234, '62e9ffb4724ce', 3, 224, 'AB-HACWEB', '+918837842284', 'gg', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-03 08:55:16', '2022-08-03 08:55:16'),
(235, '62ea00425a2f8', 3, 225, 'AB-HACWEB', '+2348127139986', 'sdd', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-03 08:57:38', '2022-08-03 08:57:38'),
(236, '62ea00f8ba9a6', 1, 226, 'BeamTEXT', '+2348127139986', 'fgd', NULL, 'plain', 'Delivered|9ea46732-bd44-4d07-9f27-e503359acfd6', 'from', '0', NULL, 9, '2022-08-03 09:00:40', '2022-08-03 09:00:40'),
(237, '62ea00f92bdbf', 1, 226, 'BeamTEXT', '+2347031476524', 'fgd', NULL, 'plain', 'Delivered|3a06e1df-cf99-447d-9cc8-e1a86f47a870', 'from', '0', NULL, 9, '2022-08-03 09:00:41', '2022-08-03 09:00:41'),
(238, '62ea1bec1a638', 3, 227, 'AB-HACWEB', '+18009009090', 'test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-03 10:55:40', '2022-08-03 10:55:40'),
(239, '62ea1bed1ecda', 3, 227, 'AB-HACWEB', '+18009009091', 'test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-03 10:55:41', '2022-08-03 10:55:41'),
(247, '62eb58509fab5', 3, 228, 'BeamText', '+918837842284', 'hii', NULL, 'plain', 'Delivered', 'from', '1', NULL, 15, '2022-08-04 09:25:36', '2022-08-04 09:25:36'),
(248, '62eb7d59dab46', 20, 229, 'BeamTEXT', '+2348162054031', 'Testing message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 17, '2022-08-04 12:03:37', '2022-08-04 12:03:37'),
(249, '62eb7d5adcb75', 20, 229, 'BeamTEXT', '+2349061432670', 'Testing message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 17, '2022-08-04 12:03:38', '2022-08-04 12:03:38'),
(250, '62eb7d5bdeb35', 20, 229, 'BeamTEXT', '+2347031476524', 'Testing message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 17, '2022-08-04 12:03:39', '2022-08-04 12:03:39'),
(255, '62ee54f9381f2', 23, 236, 'BeamTEXT', '+2347031476524', 'hello Test test1', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-08-06 15:48:09', '2022-08-06 15:48:09'),
(256, '62f0d229736cd', 23, 237, 'BeamTEXT', '+2347031476524', 'hello Test test1', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-08-08 14:06:49', '2022-08-08 14:06:49'),
(257, '62f0d2498625a', 23, 238, 'BeamTEXT', '2347031476524', 'hello Test test1', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-08-08 14:07:21', '2022-08-08 14:07:21'),
(258, '62f0e27bd27b4', 23, 239, 'BeamTEXT', '2347031476524', 'hello Test', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-08-08 15:16:27', '2022-08-08 15:16:27'),
(259, '62f0f0c83c9b1', 23, 240, 'BeamTEXT', '+2347031476524', 'message from system', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-08-08 16:17:28', '2022-08-08 16:17:28'),
(260, '631076c7c34fc', 23, NULL, 'BeamTEXT', '+2348127139986', 'vfh', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-09-01 14:09:27', '2022-09-01 14:09:27'),
(261, '63107883af469', 23, 341, 'BeamTEXT', '+18009009090', 'test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-09-01 14:16:51', '2022-09-01 14:16:51'),
(262, '63107884b283e', 23, 341, 'BeamTEXT', '+18009009091', 'test message', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-09-01 14:16:52', '2022-09-01 14:16:52'),
(263, '6315c62a228d4', 23, NULL, 'BeamTEXT', '+2348127139986', 'sdce', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-09-05 14:49:30', '2022-09-05 14:49:30'),
(264, '6315cccb518c5', 23, NULL, 'BeamTEXT', '+2348127139986', 'vdrdg', NULL, 'plain', 'Delivered', 'from', '1', NULL, 19, '2022-09-05 15:17:47', '2022-09-05 15:17:47'),
(265, '6318f63b2fa01', 25, NULL, 'BeamTEXT', '08127139986', 'Testing', NULL, 'plain', 'Delivered', 'from', '1', NULL, 20, '2022-09-08 00:51:23', '2022-09-08 00:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `cg_roles`
--

CREATE TABLE `cg_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_roles`
--

INSERT INTO `cg_roles` (`id`, `uid`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, '61f18c9392b62', 'administrator', 1, '2022-01-27 05:01:55', '2022-01-27 05:01:55'),
(2, '620e2d9a63475', 'Support', 1, '2022-02-17 16:12:26', '2022-02-17 16:12:26'),
(3, '6224bbc62dd4f', 'Sales Manager', 1, '2022-03-06 18:48:54', '2022-03-06 18:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `cg_role_user`
--

CREATE TABLE `cg_role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_role_user`
--

INSERT INTO `cg_role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cg_sender`
--

CREATE TABLE `cg_sender` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `sender_id` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 2 COMMENT '0=>reject, 1=>approved, 2=>pending',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cg_sender`
--

INSERT INTO `cg_sender` (`id`, `user_id`, `sender_id`, `status`, `created_at`, `updated_at`) VALUES
(6, 3, 'AB-HACWEB', 1, '2022-07-15 10:29:11', '2022-07-15 10:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `cg_senderid`
--

CREATE TABLE `cg_senderid` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `currency_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','active','block','payment_required','expired') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `price` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `billing_cycle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frequency_amount` int(11) NOT NULL,
  `frequency_unit` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validity_date` date DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_claimed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_senderid`
--

INSERT INTO `cg_senderid` (`id`, `uid`, `user_id`, `currency_id`, `sender_id`, `status`, `price`, `billing_cycle`, `frequency_amount`, `frequency_unit`, `validity_date`, `transaction_id`, `payment_claimed`, `created_at`, `updated_at`) VALUES
(1, '620a4e84c9ae6', 1, 1, 'SMSPORTAL', 'pending', '0', 'monthly', 1, 'month', '2022-03-14', NULL, 1, '2022-02-14 17:43:48', '2022-08-02 15:22:49'),
(15, '62d145abc3249', 3, NULL, 'AB-HACWEB', 'pending', '0', '', 0, '', NULL, NULL, 0, '2022-07-15 14:47:07', '2022-08-18 18:07:03'),
(16, '62d159cb8993b', 3, NULL, 'BeamText', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-15 16:12:59', '2022-07-15 16:13:15'),
(17, '62d3052c75cbe', 16, NULL, 'Aremici2', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-16 22:36:28', '2022-07-19 21:43:12'),
(18, '62d56be3d8a00', 16, NULL, 'Vocado', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-18 18:19:15', '2022-07-19 21:43:15'),
(20, '62d703f071526', 17, NULL, 'Kedu', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-19 23:20:16', '2022-07-19 23:20:39'),
(21, '62dd628f90cc7', 16, NULL, 'Kjoe', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-24 19:17:35', '2022-07-24 20:05:12'),
(22, '62de49056c7ef', 3, NULL, 'BeamTEXT', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-25 11:40:53', '2022-07-25 11:41:06'),
(23, '62e26cfcd907e', 19, NULL, 'BeamTEXT', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-28 15:03:24', '2022-07-28 15:04:07'),
(24, '62e4f27931956', 19, NULL, 'TestIDsender', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-07-30 12:57:29', '2022-07-30 13:07:31'),
(25, '62e98a0052a32', 20, NULL, 'BeamTEXT', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-08-03 00:33:04', '2022-08-03 00:38:28'),
(28, '62ee4768f24ea', 23, NULL, 'BeamTEXT', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-08-06 14:50:16', '2022-08-06 14:54:00'),
(29, '6318f4a8d3a62', 25, NULL, 'BeamTEXT', 'active', '0', '', 0, '', NULL, NULL, 0, '2022-09-08 00:44:40', '2022-09-08 00:45:09');

-- --------------------------------------------------------

--
-- Table structure for table `cg_senderid_plans`
--

CREATE TABLE `cg_senderid_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price` decimal(16,2) NOT NULL,
  `billing_cycle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frequency_amount` int(11) NOT NULL,
  `frequency_unit` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_sending_servers`
--

CREATE TABLE `cg_sending_servers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_sid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret_access` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_addr_ton` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5',
  `source_addr_npi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `dest_addr_ton` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `dest_addr_npi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `c1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('http','smpp','whatsapp') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'http',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `plain` tinyint(1) NOT NULL DEFAULT 0,
  `schedule` tinyint(1) NOT NULL DEFAULT 0,
  `two_way` tinyint(1) NOT NULL DEFAULT 0,
  `voice` tinyint(1) NOT NULL DEFAULT 0,
  `mms` tinyint(1) NOT NULL DEFAULT 0,
  `whatsapp` tinyint(1) NOT NULL DEFAULT 0,
  `sms_per_request` int(11) NOT NULL DEFAULT 1,
  `quota_value` int(11) NOT NULL DEFAULT 0,
  `quota_base` int(11) NOT NULL DEFAULT 0,
  `quota_unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'minute',
  `custom` tinyint(1) NOT NULL DEFAULT 0,
  `custom_order` int(11) NOT NULL DEFAULT 0,
  `success_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_sending_servers`
--

INSERT INTO `cg_sending_servers` (`id`, `uid`, `user_id`, `name`, `settings`, `api_link`, `port`, `username`, `password`, `route`, `sms_type`, `account_sid`, `auth_id`, `auth_token`, `access_key`, `secret_access`, `access_token`, `api_key`, `api_secret`, `user_token`, `project_id`, `api_token`, `auth_key`, `device_id`, `region`, `application_id`, `source_addr_ton`, `source_addr_npi`, `dest_addr_ton`, `dest_addr_npi`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`, `c7`, `type`, `status`, `plain`, `schedule`, `two_way`, `voice`, `mms`, `whatsapp`, `sms_per_request`, `quota_value`, `quota_base`, `quota_unit`, `custom`, `custom_order`, `success_keyword`, `created_at`, `updated_at`) VALUES
(1, '61f7fb5feb234', 1, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 0, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-02-01 06:38:15', '2022-07-30 13:32:44'),
(2, '6205319fd1982', 1, 'Nexmo is now Vonage', 'Vonage', 'https://rest.nexmo.com/sms/json', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89d84bc5', '0HyYxvgrlrDtIxOt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 'minute', 0, 0, NULL, '2022-02-11 01:39:11', '2022-03-02 21:48:57'),
(3, '620fd9c36e765', 1, 'Twilio', 'Twilio', NULL, NULL, NULL, NULL, NULL, NULL, 'ACe3c03d45f03b76e5090553cde793323c', NULL, '4a10e49bf23c651b47393eb92342f026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 0, 0, 1, 0, 0, 1, 60, 1, 'minute', 0, 0, NULL, '2022-02-18 21:39:15', '2022-03-02 21:48:55'),
(4, '62138c29184e2', 1, 'SMS India Hub', 'SMSIndiaHub', 'http://global.smsindiahub.in/api/mt/SendSMS', NULL, 'beamtext', '62PKE9nfRQ5sgfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 'minute', 1, 0, '000', '2022-02-21 16:57:13', '2022-03-02 20:43:57'),
(5, '621c92106f107', 1, 'mTalkz', 'MTalkz', 'http://msg.mtalkz.com/V2/http- api.php', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 1, 0, 0, 0, 0, 1, 60, 1, 'minute', 1, 0, 'success', '2022-02-28 19:12:48', '2022-02-28 19:12:52'),
(6, '621f4a68790b0', 1, 'SMSIdea', 'SMSIdea', 'https://smsidea.com/api/mt/SendSMS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 'minute', 1, 0, '000', '2022-03-02 20:43:52', '2022-03-02 23:04:09'),
(7, '621f5ee1f01e1', 1, 'Infobip', 'Infobip', 'https://ej2yy2.api.infobip.com/sms/2/text/advanced', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9b69c7a44f5c7f078854a1e7e4c913a8-5671a0cd-0306-4327-9b2d-3134c48441ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 'minute', 0, 0, NULL, '2022-03-02 22:11:13', '2022-03-29 15:19:38'),
(9, '6242ec3e16477', 1, 'Route Mobile', 'RouteMobile', 'http://ngn.rmlconnect.net/bulksms/bulksms', NULL, 'beamng', '9Q_zd6T(', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '0', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http', 1, 1, 1, 0, 0, 0, 0, 1, 60, 1, 'minute', 0, 0, NULL, '2022-03-29 19:23:42', '2022-07-30 13:32:25'),
(15, '62dfc72601740', 3, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 1, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-07-26 14:51:18', '2022-07-26 14:51:18'),
(16, '62e26a3820895', 19, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 1, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-07-28 14:51:36', '2022-07-28 14:51:36'),
(17, '62e98806c5db4', 20, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 1, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-08-03 00:24:38', '2022-08-03 00:24:38'),
(19, '62ee48111c0f1', 23, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 1, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(20, '6318efe207dec', 25, 'SMPP', 'SMPP', '37.183.161.20', '2378', 'id5', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smpp', 1, 1, 1, 0, 0, 0, 0, 1, 5000, 1, 'minute', 0, 0, NULL, '2022-09-08 00:24:18', '2022-09-08 00:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `cg_spam_word`
--

CREATE TABLE `cg_spam_word` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_spam_word`
--

INSERT INTO `cg_spam_word` (`id`, `uid`, `word`, `created_at`, `updated_at`) VALUES
(1, '620ac84a597dc', 'fuck', '2022-02-15 02:23:22', '2022-02-15 02:23:22'),
(3, '62e4fc9c91bbf', 'bombs', '2022-07-30 13:40:44', '2022-07-30 13:40:44'),
(4, '6318ffc302adf', 'terror', '2022-09-08 01:32:03', '2022-09-08 01:32:03');

-- --------------------------------------------------------

--
-- Table structure for table `cg_subscriptions`
--

CREATE TABLE `cg_subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `payment_method_id` bigint(20) UNSIGNED DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('new','pending','active','ended','renew') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `paid` tinyint(1) NOT NULL DEFAULT 0,
  `payment_claimed` tinyint(1) NOT NULL DEFAULT 0,
  `current_period_ends_at` timestamp NULL DEFAULT NULL,
  `start_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `end_by` bigint(20) UNSIGNED DEFAULT NULL,
  `end_period_last_days` int(11) NOT NULL DEFAULT 10,
  `total_unit_by_topup` decimal(10,0) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_subscriptions`
--

INSERT INTO `cg_subscriptions` (`id`, `uid`, `user_id`, `plan_id`, `payment_method_id`, `options`, `status`, `paid`, `payment_claimed`, `current_period_ends_at`, `start_at`, `end_at`, `end_by`, `end_period_last_days`, `total_unit_by_topup`, `created_at`, `updated_at`) VALUES
(78, '62dfad43a2e7f', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:25:48', '2022-07-26 09:00:51', NULL, NULL, 10, '5000', '2022-07-26 13:00:51', '2022-08-03 10:25:48'),
(79, '62dfc725f0f74', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 10:51:17', '2022-07-27 06:20:09', 1, 10, '63', '2022-07-26 14:51:17', '2022-07-27 10:20:09'),
(80, '62dfc740c00b2', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 10:51:44', '2022-07-27 06:20:09', 1, 10, '63', '2022-07-26 14:51:44', '2022-07-27 10:20:09'),
(81, '62dfc74618a52', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 10:51:50', '2022-07-27 06:20:09', 1, 10, '63', '2022-07-26 14:51:50', '2022-07-27 10:20:09'),
(82, '62dfd40224632', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 11:46:10', '2022-07-27 06:20:09', 1, 10, '1', '2022-07-26 15:46:10', '2022-07-27 10:20:09'),
(83, '62dfd43e7f02a', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 11:47:10', '2022-07-27 06:20:09', 1, 10, '1', '2022-07-26 15:47:10', '2022-07-27 10:20:09'),
(84, '62dfd4747f29f', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 11:48:04', '2022-07-27 06:20:09', 1, 10, '1', '2022-07-26 15:48:04', '2022-07-27 10:20:09'),
(85, '62dfd7af2a257', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:20:09', '2022-07-26 12:01:51', '2022-07-27 06:20:09', 1, 10, '1', '2022-07-26 16:01:51', '2022-07-27 10:20:09'),
(86, '62dfdbb529e43', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-26 12:19:01', '2022-07-27 06:19:48', 1, 10, '23', '2022-07-26 16:19:01', '2022-07-27 10:19:48'),
(87, '62dfddc126501', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-26 12:27:45', '2022-07-27 06:19:48', 1, 10, '1', '2022-07-26 16:27:45', '2022-07-27 10:19:48'),
(88, '62dfddeb22755', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-26 12:28:27', '2022-07-27 06:19:48', 1, 10, '1', '2022-07-26 16:28:27', '2022-07-27 10:19:48'),
(89, '62dfdea8e2113', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-26 12:31:36', '2022-07-27 06:19:48', 1, 10, '1', '2022-07-26 16:31:36', '2022-07-27 10:19:48'),
(90, '62dfded73ecb2', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-26 12:32:23', '2022-07-27 06:19:48', 1, 10, '1', '2022-07-26 16:32:23', '2022-07-27 10:19:48'),
(91, '62e0cbf653fd8', 3, 3, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:20:17', '2022-07-27 05:24:06', NULL, NULL, 10, '50000', '2022-07-27 09:24:06', '2022-08-03 10:20:17'),
(92, '62e0d6cc6f0eb', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-27 06:10:20', '2022-07-27 06:19:48', 1, 10, '5', '2022-07-27 10:10:20', '2022-07-27 10:19:48'),
(93, '62e0d77b1914e', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-27 06:13:15', '2022-07-27 06:19:48', 1, 10, '270', '2022-07-27 10:13:15', '2022-07-27 10:19:48'),
(94, '62e0d8b97da86', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-07-27 10:19:48', '2022-07-27 06:18:33', '2022-07-27 06:19:48', 1, 10, '5', '2022-07-27 10:18:33', '2022-07-27 10:19:48'),
(95, '62e0d8d97b910', 3, 3, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:18:03', '2022-07-27 06:19:05', NULL, NULL, 10, '50000', '2022-07-27 10:19:05', '2022-08-03 10:18:03'),
(96, '62e0d9bc76f07', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-08-01 15:15:45', '2022-07-27 06:22:52', '2022-08-01 11:15:45', 1, 10, '12', '2022-07-27 10:22:52', '2022-08-01 15:15:45'),
(97, '62e0d9da0f5dc', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-08-01 15:15:31', '2022-07-27 06:23:22', '2022-08-01 11:15:31', 1, 10, '45', '2022-07-27 10:23:22', '2022-08-01 15:15:31'),
(98, '62e0da1df1bd7', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-08-01 15:15:27', '2022-07-27 06:24:29', '2022-08-01 11:15:27', 1, 10, '20', '2022-07-27 10:24:29', '2022-08-01 15:15:27'),
(99, '62e0dd3881df0', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'ended', 0, 0, '2022-08-01 15:15:07', '2022-07-27 06:37:44', '2022-08-01 11:15:07', 1, 10, '12', '2022-07-27 10:37:44', '2022-08-01 15:15:07'),
(100, '62e26a381afac', 19, 2, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2023-07-30 13:05:21', '2022-07-28 10:51:36', NULL, NULL, 10, '0', '2022-07-28 14:51:36', '2022-07-30 13:05:21'),
(101, '62e4f4b025c4d', 19, 3, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2023-07-30 13:06:56', '2022-07-30 09:06:56', NULL, NULL, 10, '10000', '2022-07-30 13:06:56', '2022-07-30 13:07:42'),
(102, '62e98806c214b', 20, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 00:24:38', '2022-08-02 20:24:38', NULL, NULL, 10, '0', '2022-08-03 00:24:38', '2022-08-03 00:26:03'),
(103, '62e989519ddea', 20, 3, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 00:30:09', '2022-08-02 20:30:09', NULL, NULL, 10, '60000', '2022-08-03 00:30:09', '2022-08-03 00:30:09'),
(104, '62ea02f5e2f09', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:09:09', '2022-08-03 05:09:09', NULL, NULL, 10, '4', '2022-08-03 09:09:09', '2022-08-03 09:09:09'),
(105, '62ea033f9566e', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:10:23', '2022-08-03 05:10:23', NULL, NULL, 10, '500', '2022-08-03 09:10:23', '2022-08-03 09:10:23'),
(106, '62ea038634e06', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:11:34', '2022-08-03 05:11:34', NULL, NULL, 10, '4500', '2022-08-03 09:11:34', '2022-08-03 09:11:34'),
(107, '62ea0bf7ec964', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:47:35', '2022-08-03 05:47:35', NULL, NULL, 10, '1', '2022-08-03 09:47:35', '2022-08-03 09:47:35'),
(108, '62ea0c8308b4a', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:49:55', '2022-08-03 05:49:55', NULL, NULL, 10, '34', '2022-08-03 09:49:55', '2022-08-03 09:49:55'),
(109, '62ea0c9ae3292', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:50:18', '2022-08-03 05:50:18', NULL, NULL, 10, '34', '2022-08-03 09:50:18', '2022-08-03 09:50:18'),
(110, '62ea0c9e80e3e', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 09:50:22', '2022-08-03 05:50:22', NULL, NULL, 10, '34', '2022-08-03 09:50:22', '2022-08-03 09:50:22'),
(111, '62ea147bc81e3', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:23:55', '2022-08-03 06:23:55', NULL, NULL, 10, '20', '2022-08-03 10:23:55', '2022-08-03 10:23:55'),
(112, '62ea147fa545c', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:23:59', '2022-08-03 06:23:59', NULL, NULL, 10, '20', '2022-08-03 10:23:59', '2022-08-03 10:23:59'),
(113, '62ea1491e5823', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:24:17', '2022-08-03 06:24:17', NULL, NULL, 10, '20', '2022-08-03 10:24:17', '2022-08-03 10:24:17'),
(114, '62ea19f07f63b', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-04 09:12:26', '2022-08-03 06:47:12', NULL, NULL, 10, '0', '2022-08-03 10:47:12', '2022-08-04 09:12:26'),
(115, '62ea1a282c23c', 3, 3, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:48:08', '2022-08-03 06:48:08', NULL, NULL, 10, '50000', '2022-08-03 10:48:08', '2022-08-03 10:48:08'),
(116, '62ea1a53648e6', 3, 3, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:48:51', '2022-08-03 06:48:51', NULL, NULL, 10, '50000', '2022-08-03 10:48:51', '2022-08-03 10:48:51'),
(117, '62ea1cdd94780', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 10:59:41', '2022-08-03 06:59:41', NULL, NULL, 10, '50', '2022-08-03 10:59:41', '2022-08-03 10:59:41'),
(124, '62ea6d9a958ff', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 16:44:10', '2022-08-03 12:44:10', NULL, NULL, 10, '3', '2022-08-03 16:44:10', '2022-08-03 16:44:10'),
(125, '62ea6e47ce716', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 16:47:03', '2022-08-03 12:47:03', NULL, NULL, 10, '5', '2022-08-03 16:47:03', '2022-08-03 16:47:03'),
(126, '62ea707e39bd1', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-03 16:56:30', '2022-08-03 12:56:30', NULL, NULL, 10, '5', '2022-08-03 16:56:30', '2022-08-03 16:56:30'),
(127, '62eb542d9fedc', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-04 09:07:57', '2022-08-04 05:07:57', NULL, NULL, 10, '12', '2022-08-04 09:07:57', '2022-08-04 09:07:57'),
(128, '62eb5623b473c', 3, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-04 09:16:19', '2022-08-04 05:16:19', NULL, NULL, 10, '23', '2022-08-04 09:16:19', '2022-08-04 09:16:19'),
(129, '62eb563a048c5', 3, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'pending', 0, 0, '2032-08-04 09:16:42', '2022-08-04 05:16:42', NULL, NULL, 10, '34', '2022-08-04 09:16:42', '2022-08-04 09:16:42'),
(130, '62ee48111577b', 23, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-06 14:53:05', '2022-08-06 10:53:05', NULL, NULL, 10, '0', '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(131, '62fbea002c234', 20, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-08-17 00:03:28', '2022-08-16 20:03:28', NULL, NULL, 10, '7000', '2022-08-17 00:03:28', '2022-08-17 00:03:28'),
(132, '6315cc2eeafdf', 23, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-09-05 15:15:10', '2022-09-05 11:15:10', NULL, NULL, 10, '12', '2022-09-05 15:15:10', '2022-09-05 15:15:10'),
(133, '6318efe20370f', 25, 1, 16, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-09-08 00:24:18', '2022-09-07 20:24:18', NULL, NULL, 10, '0', '2022-09-08 00:24:18', '2022-09-08 00:24:55'),
(134, '6318f93852de7', 25, 1, 6, '{\"credit_warning\":true,\"credit\":\"100\",\"credit_notify\":\"both\",\"subscription_warning\":true,\"subscription_notify\":\"both\"}', 'active', 0, 0, '2032-09-08 01:04:08', '2022-09-07 21:04:08', NULL, NULL, 10, '1000', '2022-09-08 01:04:08', '2022-09-08 01:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `cg_subscription_logs`
--

CREATE TABLE `cg_subscription_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_subscription_logs`
--

INSERT INTO `cg_subscription_logs` (`id`, `uid`, `subscription_id`, `transaction_id`, `type`, `data`, `created_at`, `updated_at`) VALUES
(65, '62dfad43a5afb', 78, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 13:00:51', '2022-07-26 13:00:51'),
(66, '62dfc6f01bde3', 78, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 14:50:24', '2022-07-26 14:50:24'),
(67, '62dfd40227b38', 82, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 15:46:10', '2022-07-26 15:46:10'),
(68, '62dfd43e82a6d', 83, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 15:47:10', '2022-07-26 15:47:10'),
(69, '62dfd47481f91', 84, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 15:48:04', '2022-07-26 15:48:04'),
(70, '62dfd7af2e601', 85, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 16:01:51', '2022-07-26 16:01:51'),
(71, '62dfddc12e76d', 87, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 16:27:45', '2022-07-26 16:27:45'),
(72, '62dfddeb267aa', 88, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 16:28:27', '2022-07-26 16:28:27'),
(73, '62dfdea8e5d4a', 89, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 16:31:36', '2022-07-26 16:31:36'),
(74, '62dfded7405bd', 90, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-26 16:32:23', '2022-07-26 16:32:23'),
(75, '62e0cbf6576b8', 91, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 09:24:06', '2022-07-27 09:24:06'),
(76, '62e0d6cc73814', 92, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:10:20', '2022-07-27 10:10:20'),
(77, '62e0d77b1c804', 93, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:13:15', '2022-07-27 10:13:15'),
(78, '62e0d8b9803e5', 94, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:18:33', '2022-07-27 10:18:33'),
(79, '62e0d8d97dd0c', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:19:05', '2022-07-27 10:19:05'),
(80, '62e0d99a2b892', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:22:18', '2022-07-27 10:22:18'),
(81, '62e0d9bc7a076', 96, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-07-27 10:22:52', '2022-07-27 10:22:52'),
(82, '62e4f45119f97', 100, NULL, 'admin_plan_assigned', '{\"plan\":\"Standard\",\"price\":\"\\u200e\\u20a625,000\"}', '2022-07-30 13:05:21', '2022-07-30 13:05:21'),
(83, '62e9050108bb6', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-02 15:05:37', '2022-08-02 15:05:37'),
(84, '62e98951a01c0', 103, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 00:30:09', '2022-08-03 00:30:09'),
(85, '62ea02f5e7b64', 104, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:09:09', '2022-08-03 09:09:09'),
(86, '62ea031cde0b9', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 09:09:48', '2022-08-03 09:09:48'),
(87, '62ea033f9a15a', 105, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:10:23', '2022-08-03 09:10:23'),
(88, '62ea03606a18e', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 09:10:56', '2022-08-03 09:10:56'),
(89, '62ea03863808f', 106, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:11:34', '2022-08-03 09:11:34'),
(90, '62ea03b10dd36', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 09:12:17', '2022-08-03 09:12:17'),
(91, '62ea0bf7efbcd', 107, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:47:35', '2022-08-03 09:47:35'),
(92, '62ea0c830b40a', 108, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:49:55', '2022-08-03 09:49:55'),
(93, '62ea0c9ae54b6', 109, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:50:18', '2022-08-03 09:50:18'),
(94, '62ea0c9e82ba4', 110, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 09:50:22', '2022-08-03 09:50:22'),
(95, '62ea0e290a33c', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 09:56:57', '2022-08-03 09:56:57'),
(96, '62ea0ed2229df', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 09:59:46', '2022-08-03 09:59:46'),
(97, '62ea131bbb0ec', 95, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 10:18:03', '2022-08-03 10:18:03'),
(98, '62ea137ca372a', 91, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:19:40', '2022-08-03 10:19:40'),
(99, '62ea13a122f74', 91, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 10:20:17', '2022-08-03 10:20:17'),
(100, '62ea142f90ad8', 78, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:22:39', '2022-08-03 10:22:39'),
(101, '62ea147bc9da7', 111, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:23:55', '2022-08-03 10:23:55'),
(102, '62ea147fa7166', 112, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:23:59', '2022-08-03 10:23:59'),
(103, '62ea1491e9320', 113, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:24:17', '2022-08-03 10:24:17'),
(104, '62ea14ec0ee4f', 78, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:25:48', '2022-08-03 10:25:48'),
(105, '62ea1a282ebb3', 115, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 10:48:08', '2022-08-03 10:48:08'),
(106, '62ea1a536652a', 116, NULL, 'admin_plan_assigned', '{\"plan\":\"Pro\",\"price\":\"\\u200e\\u20a6120,000\"}', '2022-08-03 10:48:51', '2022-08-03 10:48:51'),
(107, '62ea1cdd9763d', 117, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 10:59:41', '2022-08-03 10:59:41'),
(115, '62ea6d9a977e4', 124, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 16:44:10', '2022-08-03 16:44:10'),
(116, '62ea6e47e91eb', 125, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 16:47:03', '2022-08-03 16:47:03'),
(117, '62ea707e3b772', 126, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-03 16:56:30', '2022-08-03 16:56:30'),
(118, '62eb542da236e', 127, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-04 09:07:57', '2022-08-04 09:07:57'),
(119, '62eb54a520282', 114, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-04 09:09:57', '2022-08-04 09:09:57'),
(120, '62eb55251d0bc', 114, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-04 09:12:05', '2022-08-04 09:12:05'),
(121, '62eb553af042e', 114, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-04 09:12:26', '2022-08-04 09:12:26'),
(122, '62eb5623b67bd', 128, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-04 09:16:19', '2022-08-04 09:16:19'),
(123, '62ee481117790', 130, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(124, '62fbea0033fe1', 131, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-08-17 00:03:28', '2022-08-17 00:03:28'),
(125, '6315cc2eee7f6', 132, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-09-05 15:15:10', '2022-09-05 15:15:10'),
(126, '6318f93859c4f', 134, NULL, 'admin_plan_assigned', '{\"plan\":\"Basic\",\"price\":\"\\u200e\\u20a613,500\"}', '2022-09-08 01:04:08', '2022-09-08 01:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `cg_subscription_transactions`
--

CREATE TABLE `cg_subscription_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_subscription_transactions`
--

INSERT INTO `cg_subscription_transactions` (`id`, `uid`, `subscription_id`, `title`, `type`, `status`, `amount`, `created_at`, `updated_at`) VALUES
(119, '62dfad43a3c40', 78, 'Subscribed to Basic plan', 'subscribe', 'success', '330480', '2022-07-26 13:00:51', '2022-07-26 13:00:51'),
(120, '62dfc6f01ab03', 78, 'Subscribed to Basic plan', 'subscribe', 'success', '1350000', '2022-07-26 14:50:24', '2022-07-26 14:50:24'),
(121, '62dfc725f2950', 79, 'Subscribed to Basic plan', 'subscribe', 'success', '₦170.10000000000002', '2022-07-26 14:51:17', '2022-07-26 14:51:39'),
(122, '62dfc740c152b', 80, 'Subscribed to Basic plan', 'subscribe', 'success', '₦170.10000000000002', '2022-07-26 14:51:44', '2022-07-26 14:52:17'),
(123, '62dfc7461a116', 81, 'Subscribed to Basic plan', 'subscribe', 'success', '₦170.90000000000002', '2022-07-26 14:51:50', '2022-07-26 14:52:13'),
(124, '62dfd40225d3c', 82, 'Subscribed to Basic plan', 'subscribe', 'success', '6210', '2022-07-26 15:46:10', '2022-07-26 15:46:10'),
(125, '62dfd43e8027d', 83, 'Subscribed to Basic plan', 'subscribe', 'success', '6210', '2022-07-26 15:47:10', '2022-07-26 15:47:10'),
(126, '62dfd4748059b', 84, 'Subscribed to Basic plan', 'subscribe', 'success', '8640', '2022-07-26 15:48:04', '2022-07-26 15:48:04'),
(127, '62dfd7af2bd57', 85, 'Subscribed to Basic plan', 'subscribe', 'success', '108', '2022-07-26 16:01:51', '2022-07-26 16:01:51'),
(128, '62dfdbb52b926', 86, 'Subscribed to Basic plan', 'subscribe', 'success', '₦62.1', '2022-07-26 16:19:01', '2022-07-26 16:26:50'),
(129, '62dfddc127578', 87, 'Subscribed to Basic plan', 'subscribe', 'success', '270', '2022-07-26 16:27:45', '2022-07-26 16:27:45'),
(130, '62dfddeb2373e', 88, 'Subscribed to Basic plan', 'subscribe', 'success', '1080', '2022-07-26 16:28:27', '2022-07-26 16:28:27'),
(131, '62dfdea8e3aae', 89, 'Subscribed to Basic plan', 'subscribe', 'success', '54', '2022-07-26 16:31:36', '2022-07-26 16:31:36'),
(132, '62dfded73f7c4', 90, 'Subscribed to Basic plan', 'subscribe', 'success', '108', '2022-07-26 16:32:23', '2022-07-26 16:32:23'),
(133, '62e0cbf6555eb', 91, 'Subscribed to Basic plan', 'subscribe', 'success', '540', '2022-07-27 09:24:06', '2022-07-27 09:24:06'),
(134, '62e0d6cc71784', 92, 'Subscribed to Basic plan', 'subscribe', 'success', '135', '2022-07-27 10:10:20', '2022-07-27 10:10:20'),
(135, '62e0d77b1a9db', 93, 'Subscribed to Basic plan', 'subscribe', 'success', '7290', '2022-07-27 10:13:15', '2022-07-27 10:13:15'),
(136, '62e0d8b97ef14', 94, 'Subscribed to Basic plan', 'subscribe', 'success', '135', '2022-07-27 10:18:33', '2022-07-27 10:18:33'),
(137, '62e0d8d97c9a9', 95, 'Subscribed to Basic plan', 'subscribe', 'success', '6210', '2022-07-27 10:19:05', '2022-07-27 10:21:10'),
(138, '62e0d99a29dca', 95, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-07-27 10:22:18', '2022-07-27 10:22:18'),
(139, '62e0d9bc77aeb', 96, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-07-27 10:22:52', '2022-07-27 10:22:52'),
(140, '62e0d9da1103a', 97, 'Subscribed to Basic plan', 'subscribe', 'success', '₦121.50000000000001', '2022-07-27 10:23:22', '2022-07-27 10:23:47'),
(141, '62e0da1df351e', 98, 'Subscribed to Basic plan', 'subscribe', 'success', '₦54', '2022-07-27 10:24:29', '2022-07-27 10:24:41'),
(142, '62e0dd38843b1', 99, 'Subscribed to Basic plan', 'subscribe', 'success', '₦32.400000000000006', '2022-07-27 10:37:44', '2022-07-27 10:38:38'),
(143, '62e26a381dc20', 100, 'Subscribed to Basic plan', 'subscribe', 'success', '‎₦13,500', '2022-07-28 14:51:36', '2022-07-28 14:55:43'),
(144, '62e4f45117b07', 100, 'Subscribed to Standard plan', 'subscribe', 'success', '250000', '2022-07-30 13:05:21', '2022-07-30 13:05:21'),
(145, '62e4f4b02764e', 101, 'Subscribed to Pro plan', 'subscribe', 'success', '₦240000', '2022-07-30 13:06:56', '2022-07-30 13:07:42'),
(146, '62e9050105cc1', 95, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-08-02 15:05:37', '2022-08-02 15:05:37'),
(147, '62e98806c3d49', 102, 'Subscribed to Basic plan', 'subscribe', 'success', '‎₦13,500', '2022-08-03 00:24:38', '2022-08-03 00:26:03'),
(148, '62e989519ead2', 103, 'Subscribed to Pro plan', 'subscribe', 'success', '1440000', '2022-08-03 00:30:09', '2022-08-03 00:30:09'),
(149, '62ea02f5e4da4', 104, 'Subscribed to Basic plan', 'subscribe', 'success', '108', '2022-08-03 09:09:09', '2022-08-03 09:09:09'),
(150, '62ea031cdb007', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 09:09:48', '2022-08-03 09:09:48'),
(151, '62ea033f976a0', 105, 'Subscribed to Basic plan', 'subscribe', 'success', '13500', '2022-08-03 09:10:23', '2022-08-03 09:10:23'),
(152, '62ea036066d1f', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 09:10:56', '2022-08-03 09:10:56'),
(153, '62ea0386367b0', 106, 'Subscribed to Basic plan', 'subscribe', 'success', '121500', '2022-08-03 09:11:34', '2022-08-03 09:11:34'),
(154, '62ea03b10bb8f', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 09:12:17', '2022-08-03 09:12:17'),
(155, '62ea0bf7ee54c', 107, 'Subscribed to Basic plan', 'subscribe', 'success', '27', '2022-08-03 09:47:35', '2022-08-03 09:47:35'),
(156, '62ea0c8309c16', 108, 'Subscribed to Basic plan', 'subscribe', 'success', '918', '2022-08-03 09:49:55', '2022-08-03 09:49:55'),
(157, '62ea0c9ae3e9f', 109, 'Subscribed to Basic plan', 'subscribe', 'success', '918', '2022-08-03 09:50:18', '2022-08-03 09:50:18'),
(158, '62ea0c9e81c38', 110, 'Subscribed to Basic plan', 'subscribe', 'success', '918', '2022-08-03 09:50:22', '2022-08-03 09:50:22'),
(159, '62ea0e2908318', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 09:56:57', '2022-08-03 09:56:57'),
(160, '62ea0ed221877', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 09:59:46', '2022-08-03 09:59:46'),
(161, '62ea131bb9fed', 95, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 10:18:03', '2022-08-03 10:18:03'),
(162, '62ea137ca170d', 91, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-08-03 10:19:40', '2022-08-03 10:19:40'),
(163, '62ea13a121194', 91, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 10:20:17', '2022-08-03 10:20:17'),
(164, '62ea142f8ebbf', 78, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-08-03 10:22:39', '2022-08-03 10:22:39'),
(165, '62ea147bc8edf', 111, 'Subscribed to Basic plan', 'subscribe', 'success', '540', '2022-08-03 10:23:55', '2022-08-03 10:23:55'),
(166, '62ea147fa620f', 112, 'Subscribed to Basic plan', 'subscribe', 'success', '540', '2022-08-03 10:23:59', '2022-08-03 10:23:59'),
(167, '62ea1491e7612', 113, 'Subscribed to Basic plan', 'subscribe', 'success', '540', '2022-08-03 10:24:17', '2022-08-03 10:24:17'),
(168, '62ea14ec07774', 78, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-08-03 10:25:48', '2022-08-03 10:25:48'),
(169, '62ea19f08128f', 114, 'Subscribed to Basic plan', 'subscribe', 'success', '‎₦13,500', '2022-08-03 10:47:12', '2022-08-03 10:47:26'),
(170, '62ea1a282d66e', 115, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 10:48:08', '2022-08-03 10:48:08'),
(171, '62ea1a53655a2', 116, 'Subscribed to Pro plan', 'subscribe', 'success', '1200000', '2022-08-03 10:48:51', '2022-08-03 10:48:51'),
(172, '62ea1cdd9657b', 117, 'Subscribed to Basic plan', 'subscribe', 'success', '1350', '2022-08-03 10:59:41', '2022-08-03 10:59:41'),
(180, '62ea6d9a9659b', 124, 'Subscribed to Basic plan', 'subscribe', 'success', '81', '2022-08-03 16:44:10', '2022-08-03 16:44:10'),
(181, '62ea6e47da63e', 125, 'Subscribed to Basic plan', 'subscribe', 'success', '135', '2022-08-03 16:47:03', '2022-08-03 16:47:03'),
(182, '62ea707e3a789', 126, 'Subscribed to Basic plan', 'subscribe', 'success', '135', '2022-08-03 16:56:30', '2022-08-03 16:56:30'),
(183, '62eb542da0d8c', 127, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-08-04 09:07:57', '2022-08-04 09:07:57'),
(184, '62eb54a51f0bc', 114, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-08-04 09:09:57', '2022-08-04 09:09:57'),
(185, '62eb55251bf99', 114, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-08-04 09:12:05', '2022-08-04 09:12:05'),
(186, '62eb553aef801', 114, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-08-04 09:12:26', '2022-08-04 09:12:26'),
(187, '62eb5623b54c0', 128, 'Subscribed to Basic plan', 'subscribe', 'success', '621', '2022-08-04 09:16:19', '2022-08-04 09:16:19'),
(188, '62eb563a05bde', 129, 'Subscribed to Basic plan', 'subscribe', 'pending', '₦91.80000000000001', '2022-08-04 09:16:42', '2022-08-04 09:16:42'),
(189, '62ee4811167b8', 130, 'Subscribed to Basic plan', 'subscribe', 'success', '135000', '2022-08-06 14:53:05', '2022-08-06 14:53:05'),
(190, '62fbea002e5d5', 131, 'Subscribed to Basic plan', 'subscribe', 'success', '189000', '2022-08-17 00:03:28', '2022-08-17 00:03:28'),
(191, '6315cc2eec1b1', 132, 'Subscribed to Basic plan', 'subscribe', 'success', '324', '2022-09-05 15:15:10', '2022-09-05 15:15:10'),
(192, '6318efe20585f', 133, 'Subscribed to Basic plan', 'subscribe', 'success', '‎₦13,500', '2022-09-08 00:24:18', '2022-09-08 00:24:55'),
(193, '6318f93857dc5', 134, 'Subscribed to Basic plan', 'subscribe', 'success', '27000', '2022-09-08 01:04:08', '2022-09-08 01:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `cg_templates`
--

CREATE TABLE `cg_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_templates`
--

INSERT INTO `cg_templates` (`id`, `uid`, `user_id`, `name`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, '6203cba3a4fed', 1, 'Demo', 'Hi {first_name} your available balance is {username}', 1, '2022-02-09 19:11:47', '2022-02-09 19:11:47'),
(2, '620fd89d5b0eb', 1, 'Standard', 'Hi {first_name} your available balance is  {company}  {last_name}  {address}', 1, '2022-02-18 16:34:21', '2022-02-18 16:34:21'),
(3, '62d6edb592fe3', 16, 'Birthday', 'Happy Birthday {first_name},\r\n\r\nMay you celebrate many more years IJN\r\n\r\nBest wishes', 1, '2022-07-19 21:45:25', '2022-07-19 21:45:25'),
(4, '62e4f2a9a1676', 19, 'Birthday', 'Happy birthday to you {first_name\r\n\r\nWe wish you well in all your endeavors', 1, '2022-07-30 12:58:17', '2022-07-30 12:58:17'),
(5, '62e98b184591d', 20, 'Birthday Wishes', 'Happy Birthday {first_name}  \r\nWe wish you the best as you add one more year to your many years', 1, '2022-08-03 00:37:44', '2022-08-03 00:37:44'),
(6, '6318f56b2ccc3', 25, 'Independence Day', 'Dear {first_name},\r\nYour phone number {phone} has been on our system for over a year. So you are welcome', 1, '2022-09-08 00:47:55', '2022-09-08 00:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `cg_template_tags`
--

CREATE TABLE `cg_template_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cg_top_transactions`
--

CREATE TABLE `cg_top_transactions` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(125) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `payment_method` bigint(20) NOT NULL DEFAULT 0,
  `status` varchar(12) DEFAULT NULL,
  `total_unit` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cg_users`
--

CREATE TABLE `cg_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `is_customer` tinyint(1) NOT NULL DEFAULT 0,
  `active_portal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor` tinyint(1) NOT NULL DEFAULT 0,
  `two_factor_code` int(11) DEFAULT NULL,
  `two_factor_expires_at` datetime DEFAULT NULL,
  `two_factor_backup_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Africa/Lagos',
  `last_access_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cg_users`
--

INSERT INTO `cg_users` (`id`, `uid`, `api_token`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `status`, `image`, `sms_unit`, `is_admin`, `is_customer`, `active_portal`, `two_factor`, `two_factor_code`, `two_factor_expires_at`, `two_factor_backup_code`, `locale`, `timezone`, `last_access_at`, `provider`, `provider_id`, `remember_token`, `address`, `postcode`, `city`, `created_at`, `updated_at`) VALUES
(1, '61f18c97b37eb', '1|2kvcM8YYnFRwH9BzyfvMaKHPUOJcnnLHI7J2PRNx', 'Beamtext', 'Marketing', 'info@beamtext.com', '2022-01-27 05:01:59', '$2y$10$5U1VvAIXBb.9HI4GFlXwuOqSbxFu75CGVJojgHKP5pCRWiFzWFYDG', 1, NULL, '68767', 1, 0, 'admin', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-09-08 00:24:05', NULL, NULL, 'WfH9pF8lnbtgF6190BbW0XuxntEf5r52mQjPmGUnPoC7BYuoHyefhYrtnACU', NULL, NULL, NULL, '2022-01-27 05:01:59', '2022-09-08 00:24:05'),
(3, '620fdb62e4f84', '2|jxLPQqdkEnOSluz1D5VZDYdnBFCkHhmrJqRsv1Pe', 'Client', 'Demo', 'client@gmail.com', '2022-02-17 16:13:18', '$2y$10$h3tTMmC1w6IkJiCSx6zk9eTVRNQq75e3/RJPsxzMpNNGoPVgzwMeG', 1, NULL, '5021', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-08-08 13:04:03', NULL, NULL, 'PHWITwj59Xwtb8iFL77vKx7cQRFAglns7hnaSAOEY533vxU6cXNmiQWs1TOH', 'address', '24433', 'city', '2022-02-18 16:46:10', '2022-08-08 13:04:03'),
(16, '62d3044a4b18d', '15|KEtw69bzajjylyMldhWJwzjsTXrI5qBfRIkHeLRg', 'KJ', 'joe', 'kj1ng@yahoo.com', '2022-07-16 22:32:42', '$2y$10$fjHLiuMorm5Y..O71.gZOOdW4BgVJ97F2uGojVXeNzzb0Gms3W0qy', 1, NULL, '70300', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-08-16 21:54:59', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-16 22:32:42', '2022-08-16 21:54:59'),
(17, '62d6ffa75105b', '16|avBE6rWQlv81sMulKu6gbUZV8HKYRA62xluFTyAd', 'kj2', 'joe', 'kj1ngg@gmail.com', '2022-07-19 23:01:59', '$2y$10$.F4H8ahV4a8S3AiCKaVkyetcalv90LoshQyrkaVqxIsSRqxq4MhjS', 1, NULL, '120100', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-08-16 21:54:35', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 23:01:59', '2022-08-16 21:54:35'),
(18, '62dd6f379bc47', NULL, 'oLA', 'JONAS', 'audienceconnectservices@gmail.com', '2022-07-24 20:11:35', '$2y$10$py.w..rKJf0CCdyurTeo3.6p2KxYC/Z.IfEDJ28eKFf71bUBD4UkS', 1, NULL, NULL, 1, 0, 'admin', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-07-24 20:12:40', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-24 20:11:35', '2022-07-24 20:12:40'),
(19, '62e262a9069d3', '17|EQoJHHwZeOtgB4HrecJc76leKFE7hNtAOw8GfLNA', 'Ola', 'Faith', 'comcityads@gmail.com', '2022-07-28 14:19:20', '$2y$10$Nvay5urv2GLunozF8IhqM..HMUoppquE/QG7auYl5ZUxmSS.XSuFW', 1, NULL, '19997', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-07-30 13:15:20', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-28 14:19:21', '2022-07-30 13:34:26'),
(20, '62e985b28b0e6', '18|Nj3M4mwe0OvF5zKlsARv5MUyUrzNJlS057kb2IUS', 'Tunde', 'Ajala', 'tundex77@gmail.com', '2022-08-03 00:14:42', '$2y$10$06V2yVI35qIimpaMfzV0Xe/u8SMyi/HzIJth.o2WSPCZw3hr82USy', 1, NULL, '71995', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-09-13 02:53:49', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-03 00:14:42', '2022-09-13 02:53:49'),
(22, '62ece5da967b4', '20|q2pUror6zDnoTbcWDupzVcHhBZnwXG2QZC52O3NO', 'Hassie', 'Doughty', 'juanitamcgoldrick@hidebox.org', '2022-08-05 13:41:46', '$2y$10$F2b8CyV3eiShNMJ38Sp9weCgo6S.i74o2YvSggySJB5K.EMAWMMSa', 1, NULL, NULL, 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-08-05 15:01:26', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-05 13:41:46', '2022-08-05 15:01:26'),
(23, '62ee47440ecc9', '21|WbyLHbw06DejjFbJgs2tHxvbrxdgH79xmkGNgWnc', 'Test', 'test1', 'testhachi123@gmail.com', '2022-08-06 14:49:39', '$2y$10$ns9Err3RVG5ucPd8PuMYDO4n1G8W94FNwm.eqwNoolRBPWuSeRQza', 1, NULL, '4923', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Accra', '2022-08-08 13:55:13', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-06 14:49:40', '2022-09-05 17:33:13'),
(24, '630d36ac82d67', '22|Ydy8qu4kBccMy1ADS72IMIMP3RNX8namxYhuyuka', 'Brandi', 'Hefner', 'brandihefner@awer.blastzane.com', '2022-08-30 02:59:08', '$2y$10$44YeKnjZ8rgPhpFlwfQfoehyb2YE.NZuQ7tsY6KWG5kh8znjCB.QW', 1, NULL, NULL, 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Lagos', '2022-08-30 04:19:28', NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-30 02:59:08', '2022-08-30 04:19:28'),
(25, '6318ef025cd9d', '23|r2BZhCkq0PyBpF1x9DgRBKlUZDgN6Eh5MCygjzzN', 'Lanre', 'Ajala', 'ajalaofficial@gmail.com', '2022-09-08 00:20:34', '$2y$10$J/ms41Y.PRAWi..SEcj/geANrniGmV/iN8Nsfff3NqqKZyCHc0fuW', 1, NULL, '5985', 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Lagos', '2022-09-08 01:01:27', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 00:20:34', '2022-09-08 01:49:57'),
(26, '6318fdb77350a', NULL, 'Tunde 3', 'Ajala', 'iamajalatunde@gmail.com', '2022-09-08 01:23:19', '$2y$10$X3S6BVTrhpIPm3iqSdO4M.EwR1gK1jajFIojoT0h2LY94pcBqyct6', 1, NULL, NULL, 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Lagos', '2022-09-08 01:26:03', NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-08 01:23:19', '2022-09-08 01:26:03'),
(27, '631f6d17bc898', '24|arwrPD2CJEwEYlN2yS3ZIYBuKRDg504h40c3vk05', 'Ifeanyi', 'greek', 'playsafe272@gmail.com', '2022-09-12 22:32:07', '$2y$10$EwJ0U3w5qvpRGKGNciZ9Pux7YuwuYYeBiuKn8Lg9/6KQBnyS1qbGi', 1, NULL, NULL, 0, 1, 'customer', 0, NULL, NULL, NULL, 'en', 'Africa/Lagos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-09-12 22:32:07', '2022-09-12 22:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `top_transactions`
--

CREATE TABLE `top_transactions` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(125) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `payment_method` bigint(20) NOT NULL DEFAULT 0,
  `status` varchar(12) DEFAULT NULL,
  `total_unit` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cg_app_config`
--
ALTER TABLE `cg_app_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_blacklists`
--
ALTER TABLE `cg_blacklists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_blacklists_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_campaigns`
--
ALTER TABLE `cg_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_campaigns_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_campaigns_lists`
--
ALTER TABLE `cg_campaigns_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_campaigns_lists_campaign_id_foreign` (`campaign_id`),
  ADD KEY `cg_campaigns_lists_contact_list_id_foreign` (`contact_list_id`);

--
-- Indexes for table `cg_campaigns_recipients`
--
ALTER TABLE `cg_campaigns_recipients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_campaigns_recipients_campaign_id_foreign` (`campaign_id`);

--
-- Indexes for table `cg_campaigns_senderids`
--
ALTER TABLE `cg_campaigns_senderids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_campaigns_senderids_campaign_id_foreign` (`campaign_id`);

--
-- Indexes for table `cg_campaigns_sending_servers`
--
ALTER TABLE `cg_campaigns_sending_servers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_campaigns_sending_servers_campaign_id_foreign` (`campaign_id`),
  ADD KEY `cg_campaigns_sending_servers_sending_server_id_foreign` (`sending_server_id`);

--
-- Indexes for table `cg_chat_boxes`
--
ALTER TABLE `cg_chat_boxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_chat_boxes_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_chat_box_messages`
--
ALTER TABLE `cg_chat_box_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_chat_box_messages_box_id_foreign` (`box_id`),
  ADD KEY `cg_chat_box_messages_sending_server_id_foreign` (`sending_server_id`);

--
-- Indexes for table `cg_contacts`
--
ALTER TABLE `cg_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_contacts_customer_id_foreign` (`customer_id`),
  ADD KEY `cg_contacts_group_id_foreign` (`group_id`);

--
-- Indexes for table `cg_contacts_custom_field`
--
ALTER TABLE `cg_contacts_custom_field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_contacts_custom_field_contact_id_foreign` (`contact_id`);

--
-- Indexes for table `cg_contact_groups`
--
ALTER TABLE `cg_contact_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_contact_groups_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `cg_contact_groups_optin_keywords`
--
ALTER TABLE `cg_contact_groups_optin_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_contact_groups_optin_keywords_contact_group_foreign` (`contact_group`);

--
-- Indexes for table `cg_contact_groups_optout_keywords`
--
ALTER TABLE `cg_contact_groups_optout_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_contact_groups_optout_keywords_contact_group_foreign` (`contact_group`);

--
-- Indexes for table `cg_countries`
--
ALTER TABLE `cg_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_csv_data`
--
ALTER TABLE `cg_csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_currencies`
--
ALTER TABLE `cg_currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_currencies_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_customers`
--
ALTER TABLE `cg_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_customers_user_id_foreign` (`user_id`),
  ADD KEY `cg_customers_contact_id_foreign` (`contact_id`);

--
-- Indexes for table `cg_custom_sending_servers`
--
ALTER TABLE `cg_custom_sending_servers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_custom_sending_servers_server_id_foreign` (`server_id`);

--
-- Indexes for table `cg_email_templates`
--
ALTER TABLE `cg_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_failed_jobs`
--
ALTER TABLE `cg_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_import_job_histories`
--
ALTER TABLE `cg_import_job_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_invoices`
--
ALTER TABLE `cg_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_invoices_user_id_foreign` (`user_id`),
  ADD KEY `cg_invoices_currency_id_foreign` (`currency_id`),
  ADD KEY `cg_invoices_payment_method_foreign` (`payment_method`);

--
-- Indexes for table `cg_jobs`
--
ALTER TABLE `cg_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_jobs_queue_index` (`queue`);

--
-- Indexes for table `cg_job_batches`
--
ALTER TABLE `cg_job_batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_keywords`
--
ALTER TABLE `cg_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_keywords_user_id_foreign` (`user_id`),
  ADD KEY `cg_keywords_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cg_languages`
--
ALTER TABLE `cg_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_migrations`
--
ALTER TABLE `cg_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_notifications`
--
ALTER TABLE `cg_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_notifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_password_resets`
--
ALTER TABLE `cg_password_resets`
  ADD KEY `cg_password_resets_email_index` (`email`);

--
-- Indexes for table `cg_payment_methods`
--
ALTER TABLE `cg_payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_permissions`
--
ALTER TABLE `cg_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `cg_personal_access_tokens`
--
ALTER TABLE `cg_personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cg_personal_access_tokens_token_unique` (`token`),
  ADD KEY `cg_personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `cg_phone_numbers`
--
ALTER TABLE `cg_phone_numbers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_phone_numbers_user_id_foreign` (`user_id`),
  ADD KEY `cg_phone_numbers_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cg_plans`
--
ALTER TABLE `cg_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_plans_user_id_foreign` (`user_id`),
  ADD KEY `cg_plans_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cg_plans_coverage_countries`
--
ALTER TABLE `cg_plans_coverage_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_plans_coverage_countries_country_id_foreign` (`country_id`),
  ADD KEY `cg_plans_coverage_countries_plan_id_foreign` (`plan_id`);

--
-- Indexes for table `cg_plans_sending_servers`
--
ALTER TABLE `cg_plans_sending_servers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_plans_sending_servers_sending_server_id_foreign` (`sending_server_id`),
  ADD KEY `cg_plans_sending_servers_plan_id_foreign` (`plan_id`);

--
-- Indexes for table `cg_reports`
--
ALTER TABLE `cg_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_reports_user_id_foreign` (`user_id`),
  ADD KEY `cg_reports_campaign_id_foreign` (`campaign_id`),
  ADD KEY `cg_reports_sending_server_id_foreign` (`sending_server_id`);

--
-- Indexes for table `cg_roles`
--
ALTER TABLE `cg_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cg_roles_name_unique` (`name`);

--
-- Indexes for table `cg_role_user`
--
ALTER TABLE `cg_role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `cg_role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `cg_sender`
--
ALTER TABLE `cg_sender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_senderid`
--
ALTER TABLE `cg_senderid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_senderid_user_id_foreign` (`user_id`),
  ADD KEY `cg_senderid_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cg_senderid_plans`
--
ALTER TABLE `cg_senderid_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_senderid_plans_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cg_sending_servers`
--
ALTER TABLE `cg_sending_servers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_sending_servers_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_spam_word`
--
ALTER TABLE `cg_spam_word`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_subscriptions`
--
ALTER TABLE `cg_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_subscriptions_user_id_foreign` (`user_id`),
  ADD KEY `cg_subscriptions_end_by_foreign` (`end_by`),
  ADD KEY `cg_subscriptions_payment_method_id_foreign` (`payment_method_id`);

--
-- Indexes for table `cg_subscription_logs`
--
ALTER TABLE `cg_subscription_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_subscription_logs_subscription_id_foreign` (`subscription_id`),
  ADD KEY `cg_subscription_logs_transaction_id_foreign` (`transaction_id`);

--
-- Indexes for table `cg_subscription_transactions`
--
ALTER TABLE `cg_subscription_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_subscription_transactions_subscription_id_foreign` (`subscription_id`);

--
-- Indexes for table `cg_templates`
--
ALTER TABLE `cg_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cg_templates_user_id_foreign` (`user_id`);

--
-- Indexes for table `cg_template_tags`
--
ALTER TABLE `cg_template_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_top_transactions`
--
ALTER TABLE `cg_top_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cg_users`
--
ALTER TABLE `cg_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cg_users_email_unique` (`email`);

--
-- Indexes for table `top_transactions`
--
ALTER TABLE `top_transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cg_app_config`
--
ALTER TABLE `cg_app_config`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `cg_blacklists`
--
ALTER TABLE `cg_blacklists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cg_campaigns`
--
ALTER TABLE `cg_campaigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- AUTO_INCREMENT for table `cg_campaigns_lists`
--
ALTER TABLE `cg_campaigns_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `cg_campaigns_recipients`
--
ALTER TABLE `cg_campaigns_recipients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `cg_campaigns_senderids`
--
ALTER TABLE `cg_campaigns_senderids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `cg_campaigns_sending_servers`
--
ALTER TABLE `cg_campaigns_sending_servers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;

--
-- AUTO_INCREMENT for table `cg_chat_boxes`
--
ALTER TABLE `cg_chat_boxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_chat_box_messages`
--
ALTER TABLE `cg_chat_box_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_contacts`
--
ALTER TABLE `cg_contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `cg_contacts_custom_field`
--
ALTER TABLE `cg_contacts_custom_field`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_contact_groups`
--
ALTER TABLE `cg_contact_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cg_contact_groups_optin_keywords`
--
ALTER TABLE `cg_contact_groups_optin_keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_contact_groups_optout_keywords`
--
ALTER TABLE `cg_contact_groups_optout_keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_countries`
--
ALTER TABLE `cg_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cg_csv_data`
--
ALTER TABLE `cg_csv_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `cg_currencies`
--
ALTER TABLE `cg_currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cg_customers`
--
ALTER TABLE `cg_customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `cg_custom_sending_servers`
--
ALTER TABLE `cg_custom_sending_servers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cg_email_templates`
--
ALTER TABLE `cg_email_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cg_failed_jobs`
--
ALTER TABLE `cg_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cg_import_job_histories`
--
ALTER TABLE `cg_import_job_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `cg_invoices`
--
ALTER TABLE `cg_invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `cg_jobs`
--
ALTER TABLE `cg_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `cg_keywords`
--
ALTER TABLE `cg_keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_languages`
--
ALTER TABLE `cg_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cg_migrations`
--
ALTER TABLE `cg_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `cg_notifications`
--
ALTER TABLE `cg_notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `cg_payment_methods`
--
ALTER TABLE `cg_payment_methods`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cg_permissions`
--
ALTER TABLE `cg_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `cg_personal_access_tokens`
--
ALTER TABLE `cg_personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cg_phone_numbers`
--
ALTER TABLE `cg_phone_numbers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cg_plans`
--
ALTER TABLE `cg_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cg_plans_coverage_countries`
--
ALTER TABLE `cg_plans_coverage_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_plans_sending_servers`
--
ALTER TABLE `cg_plans_sending_servers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cg_reports`
--
ALTER TABLE `cg_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `cg_roles`
--
ALTER TABLE `cg_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cg_sender`
--
ALTER TABLE `cg_sender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cg_senderid`
--
ALTER TABLE `cg_senderid`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `cg_senderid_plans`
--
ALTER TABLE `cg_senderid_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cg_sending_servers`
--
ALTER TABLE `cg_sending_servers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cg_spam_word`
--
ALTER TABLE `cg_spam_word`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cg_subscriptions`
--
ALTER TABLE `cg_subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `cg_subscription_logs`
--
ALTER TABLE `cg_subscription_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `cg_subscription_transactions`
--
ALTER TABLE `cg_subscription_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `cg_templates`
--
ALTER TABLE `cg_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cg_template_tags`
--
ALTER TABLE `cg_template_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cg_top_transactions`
--
ALTER TABLE `cg_top_transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cg_users`
--
ALTER TABLE `cg_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `top_transactions`
--
ALTER TABLE `top_transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cg_blacklists`
--
ALTER TABLE `cg_blacklists`
  ADD CONSTRAINT `cg_blacklists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_campaigns`
--
ALTER TABLE `cg_campaigns`
  ADD CONSTRAINT `cg_campaigns_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_campaigns_lists`
--
ALTER TABLE `cg_campaigns_lists`
  ADD CONSTRAINT `cg_campaigns_lists_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `cg_campaigns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_campaigns_lists_contact_list_id_foreign` FOREIGN KEY (`contact_list_id`) REFERENCES `cg_contact_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_campaigns_recipients`
--
ALTER TABLE `cg_campaigns_recipients`
  ADD CONSTRAINT `cg_campaigns_recipients_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `cg_campaigns` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_campaigns_senderids`
--
ALTER TABLE `cg_campaigns_senderids`
  ADD CONSTRAINT `cg_campaigns_senderids_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `cg_campaigns` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_campaigns_sending_servers`
--
ALTER TABLE `cg_campaigns_sending_servers`
  ADD CONSTRAINT `cg_campaigns_sending_servers_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `cg_campaigns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_campaigns_sending_servers_sending_server_id_foreign` FOREIGN KEY (`sending_server_id`) REFERENCES `cg_sending_servers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_chat_boxes`
--
ALTER TABLE `cg_chat_boxes`
  ADD CONSTRAINT `cg_chat_boxes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_chat_box_messages`
--
ALTER TABLE `cg_chat_box_messages`
  ADD CONSTRAINT `cg_chat_box_messages_box_id_foreign` FOREIGN KEY (`box_id`) REFERENCES `cg_chat_boxes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_chat_box_messages_sending_server_id_foreign` FOREIGN KEY (`sending_server_id`) REFERENCES `cg_sending_servers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_contacts`
--
ALTER TABLE `cg_contacts`
  ADD CONSTRAINT `cg_contacts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_contacts_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `cg_contact_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_contacts_custom_field`
--
ALTER TABLE `cg_contacts_custom_field`
  ADD CONSTRAINT `cg_contacts_custom_field_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `cg_contacts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_contact_groups`
--
ALTER TABLE `cg_contact_groups`
  ADD CONSTRAINT `cg_contact_groups_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_contact_groups_optin_keywords`
--
ALTER TABLE `cg_contact_groups_optin_keywords`
  ADD CONSTRAINT `cg_contact_groups_optin_keywords_contact_group_foreign` FOREIGN KEY (`contact_group`) REFERENCES `cg_contact_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_contact_groups_optout_keywords`
--
ALTER TABLE `cg_contact_groups_optout_keywords`
  ADD CONSTRAINT `cg_contact_groups_optout_keywords_contact_group_foreign` FOREIGN KEY (`contact_group`) REFERENCES `cg_contact_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_currencies`
--
ALTER TABLE `cg_currencies`
  ADD CONSTRAINT `cg_currencies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_customers`
--
ALTER TABLE `cg_customers`
  ADD CONSTRAINT `cg_customers_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `cg_contacts` (`id`),
  ADD CONSTRAINT `cg_customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_custom_sending_servers`
--
ALTER TABLE `cg_custom_sending_servers`
  ADD CONSTRAINT `cg_custom_sending_servers_server_id_foreign` FOREIGN KEY (`server_id`) REFERENCES `cg_sending_servers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_invoices`
--
ALTER TABLE `cg_invoices`
  ADD CONSTRAINT `cg_invoices_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_invoices_payment_method_foreign` FOREIGN KEY (`payment_method`) REFERENCES `cg_payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_keywords`
--
ALTER TABLE `cg_keywords`
  ADD CONSTRAINT `cg_keywords_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_keywords_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_notifications`
--
ALTER TABLE `cg_notifications`
  ADD CONSTRAINT `cg_notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_permissions`
--
ALTER TABLE `cg_permissions`
  ADD CONSTRAINT `cg_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `cg_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_phone_numbers`
--
ALTER TABLE `cg_phone_numbers`
  ADD CONSTRAINT `cg_phone_numbers_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_phone_numbers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_plans`
--
ALTER TABLE `cg_plans`
  ADD CONSTRAINT `cg_plans_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_plans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_plans_coverage_countries`
--
ALTER TABLE `cg_plans_coverage_countries`
  ADD CONSTRAINT `cg_plans_coverage_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `cg_countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_plans_coverage_countries_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `cg_plans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_plans_sending_servers`
--
ALTER TABLE `cg_plans_sending_servers`
  ADD CONSTRAINT `cg_plans_sending_servers_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `cg_plans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_plans_sending_servers_sending_server_id_foreign` FOREIGN KEY (`sending_server_id`) REFERENCES `cg_sending_servers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_reports`
--
ALTER TABLE `cg_reports`
  ADD CONSTRAINT `cg_reports_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `cg_campaigns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_reports_sending_server_id_foreign` FOREIGN KEY (`sending_server_id`) REFERENCES `cg_sending_servers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_role_user`
--
ALTER TABLE `cg_role_user`
  ADD CONSTRAINT `cg_role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `cg_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_senderid`
--
ALTER TABLE `cg_senderid`
  ADD CONSTRAINT `cg_senderid_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_senderid_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_senderid_plans`
--
ALTER TABLE `cg_senderid_plans`
  ADD CONSTRAINT `cg_senderid_plans_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `cg_currencies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_sending_servers`
--
ALTER TABLE `cg_sending_servers`
  ADD CONSTRAINT `cg_sending_servers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_subscriptions`
--
ALTER TABLE `cg_subscriptions`
  ADD CONSTRAINT `cg_subscriptions_end_by_foreign` FOREIGN KEY (`end_by`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_subscriptions_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `cg_payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_subscriptions_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `cg_plans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_subscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_subscription_logs`
--
ALTER TABLE `cg_subscription_logs`
  ADD CONSTRAINT `cg_subscription_logs_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `cg_subscriptions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cg_subscription_logs_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `cg_subscription_transactions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_subscription_transactions`
--
ALTER TABLE `cg_subscription_transactions`
  ADD CONSTRAINT `cg_subscription_transactions_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `cg_subscriptions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cg_templates`
--
ALTER TABLE `cg_templates`
  ADD CONSTRAINT `cg_templates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cg_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
