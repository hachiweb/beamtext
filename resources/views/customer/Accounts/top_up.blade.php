@extends('layouts/contentLayoutMaster')

@section('title', __('locale.labels.top_up'))
@php
$planUnit = 0;
if(!empty($plan)){
$planUnit = $plan->getOption('per_unit_price');
}
@endphp
@section('content')
    <!-- Basic Vertical form layout section start -->
    <section id="basic-vertical-layouts">
        <div class="row match-height">
            <div class="col-md-6 col-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> {{__('locale.customer.add_unit_to_your_account')}} </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="form-body">
                                <form class="form form-vertical" action="{{ route('user.account.top_up') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">

                                            <!--<p>{!! __('locale.description.add_unit') !!}</p>-->
                                            <p>Please enter desired number of units you want to top up</p>
                                            <p>Please use only numeric number not decimal number. Example: Accepted: 1, 2, 3 Not Accepted: 0.025, 1.025, 1.00, 1.5 </p>

                                            <div class="form-group">
                                                <label for="add_unit" class="required">{{__('locale.labels.per_unit_price')}} = <span class="per_unit_price">{{(isset($planUnit)?$planUnit:0)  }}</span> {{ Auth::user()->customer->subscription->plan->currency->code }}</label>

                                                <div class="input-group">
                                                    <input type="text" id="add_unit" class="form-control @error('add_unit') is-invalid @enderror" name="add_unit" required>
                                                    @error('add_unit')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                    <div class="input-group-append">
                                                        <input type="hidden" value="0" name="price_amount">
                                                        <input type="hidden" value="{{(isset($planUnit)?$planUnit:0)}}" name="unit_of_browser">
                                                        <span class="input-group-text text-primary font-weight-bold update-price">0 {{ Auth::user()->customer->subscription->plan->currency->code }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1">
                                                <i class="feather icon-shopping-cart"></i> {{__('locale.labels.checkout')}}
                                            </button>
                                        </div>


                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- // Basic Vertical form layout section end -->


@endsection


@section('page-script')

    <script>
        let firstInvalid = $('form').find('.is-invalid').eq(0),
            price = 0,
            $get_price = $("#add_unit");

        if (firstInvalid.length) {
            $('body, html').stop(true, true).animate({
                'scrollTop': firstInvalid.offset().top - 200 + 'px'
            }, 200);
        }
        function get_price() {
            let total_unit = $get_price[0].value;
            let total_price=0;
            if(total_unit >=0 && total_unit <=9999){
                total_price = total_unit * 2.70;
            $('.per_unit_price').text(2.70)
                
             $("input[name='unit_of_browser']").val('2.70');
            }
            else if(total_unit >=10000  && total_unit <50000 ){
                total_price = total_unit * 2.50;
            $('.per_unit_price').text(2.50)
             $("input[name='unit_of_browser']").val('2.50');
            }
            else if(total_unit >=50000  ){
                total_price = total_unit * 2.40;
            $('.per_unit_price').text(2.40)
             $("input[name='unit_of_browser']").val('2.40');
            }
            $('.update-price').text(Math.ceil(total_price) + " {{ Auth::user()->customer->subscription->plan->currency->code}}")
            $("input[name='price_amount']").val(total_price);
        }

        $get_price.keyup(get_price);

    </script>
@endsection
